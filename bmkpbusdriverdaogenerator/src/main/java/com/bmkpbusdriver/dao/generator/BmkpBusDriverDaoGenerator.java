package com.bmkpbusdriver.dao.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * author:zhangn
 *
 * description: GreenDao 用于构建bus司机端数据库
 */
public class BmkpBusDriverDaoGenerator {

    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1, "cn.bmkp.bus.app.driver");

        addBusInfo(schema);

        new DaoGenerator().generateAll(schema, "bmkpbusdriver/src-gen");
    }


    private static void addBusInfo(Schema schema) {

        Entity busInfo = schema.addEntity("BusInfo");

        busInfo.addIdProperty().autoincrement();
        busInfo.addStringProperty("line");
        busInfo.addStringProperty("busnumber");
        busInfo.addStringProperty("beginStation");  // 起点站
        busInfo.addStringProperty("endStation");    // 终点站
        busInfo.addStringProperty("beginTime");    // 发车时间
        busInfo.addStringProperty("day");          // 日期
        busInfo.addStringProperty("taskKey");    // task key


        Entity station = schema.addEntity("BusStation");

        station.addIdProperty().autoincrement();
        station.addStringProperty("key");
        station.addStringProperty("name");
        station.addStringProperty("detail");
        station.addStringProperty("time");
        station.addBooleanProperty("arrive");
        station.addStringProperty("downNum");


        // 添加多目地址一对多关系
        Property orderId = station.addLongProperty("busInfoId").getProperty();
        busInfo.addToMany(station,orderId).setName("stations");
    }
}
