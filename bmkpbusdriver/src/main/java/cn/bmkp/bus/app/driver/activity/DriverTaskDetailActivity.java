package cn.bmkp.bus.app.driver.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.constant.AppStringConstant;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.models.BusRun;
import cn.bmkp.bus.app.driver.models.Task;
import cn.bmkp.bus.app.driver.utils.ActivityStack;
import cn.bmkp.bus.app.driver.utils.ParseUtil;
import cn.bmkp.bus.app.driver.wilddog.WilddogApi;

/**
 * Created by chenliang on 2016/3/19.
 *
 * description:司机的任务详情页
 */
public class DriverTaskDetailActivity extends BaseActivity {

    @Bind(R.id.tv_my_task_address)
    TextView tvMyTaskAddress;
    @Bind(R.id.tv_my_task_time)
    TextView tvMyTaskTime;
    @Bind(R.id.tv_my_task_start_address)
    TextView tvMyTaskStartAddress;
    @Bind(R.id.tv_my_task_end_address)
    TextView tvMyTaskEndAddress;
    @Bind(R.id.tv_my_task_car_num)
    TextView tvMyTaskCarNum;
    @Bind(R.id.tv_tast_start_time)
    TextView tvTastStartTime;

    private Task mTask;

    private BusRun mBusRun;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        this.finish();
    }

    /**
     * 始发站发车
     * @param view
     */
    @OnClick(R.id.tv_start_task)
    public void departStation(View view) {

        //计算发车时间
        Calendar carTime = Calendar.getInstance();
        String time = mTask.getBeginTime();
        int hour = Integer.parseInt(time.substring(0, time.indexOf(":")));
        int minute = Integer.parseInt(time.substring(time.indexOf(":") + 1, time.length()));
        carTime.set(Calendar.HOUR_OF_DAY, hour);
        carTime.set(Calendar.MINUTE, minute);

        Calendar currentTime = Calendar.getInstance();

        long  c_time = carTime.getTimeInMillis() - currentTime.getTimeInMillis();
        long  min = (c_time/1000)/60;

        if (min>=0) {
            showAlertDialog("", getString(R.string.text_task_details_dialog_text1)+min+getString(R.string.text_task_details_dialog_text2), getString(R.string.text_task_details_dialog_text3), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    requestArrivePoint();

                }
            }, getString(R.string.text_task_details_dialog_text4), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }
                    , DriverTaskDetailActivity.this);
        } else {
            Toast.makeText(DriverTaskDetailActivity.this, R.string.text_task_details_dialog_text5,Toast.LENGTH_SHORT).show();
            requestArrivePoint();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_task_detail);
        ButterKnife.bind(this);

        loadIntentParam();
        initView();
        initData();
    }

    private void loadIntentParam(){

        mTask = (Task) getIntent().getSerializableExtra(AppStringConstant.Param.KEY_TASK);
    }

    private void initView() {

        tvMyTaskAddress.setText(mTask.getBeginStation() + "——" + mTask.getEndStation());
        tvMyTaskTime.setText(mTask.getBeginTime());
        tvMyTaskStartAddress.setText(mTask.getBeginStation());
        tvMyTaskEndAddress.setText(mTask.getEndStation());
    }


    private void initData() {

        // 查询具体的信息 包括各站点 （查询BusRun）
        WilddogApi.getInstance().getDriverTaskDetail(mTask.getLine(), mTask.getDay(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mBusRun = ParseUtil.parseBusRun(dataSnapshot);

                tvMyTaskCarNum.setText(mBusRun.getLicenseNumber());
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                Toast.makeText(DriverTaskDetailActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
            }
        });
    }




    /**
     * 获取当前是哪一站
     */
    private BusStation getCurrentStation(){

        List<BusStation> stations = DBManager.getInstance().getBusInfo().getStations();

        for(BusStation station : stations) {

            if(!station.getArrive()) {
                return  station;
            }
        }
        return null;
    }

    private void updateLocalDB(BusStation busStation){

        DBManager.getInstance().updateBusStation(busStation);
    }


    /**
     * 到站(起点站)
     */
    private void requestArrivePoint(){

//        saveBusInfo(mBusRun, mTask);

        BusStation busStation = getCurrentStation();

        busStation.setArrive(true);

        updateLocalDB(busStation);

        WilddogApi.getInstance().requestArrivePoint(mTask.getLine(), mTask.getDay(), busStation.getKey(), new Wilddog.CompletionListener() {
            @Override
            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                if (wilddogError == null) {
                    Intent intent = new Intent(DriverTaskDetailActivity.this, TaskStationListActivity.class);
                    startActivity(intent);
                    DriverTaskDetailActivity.this.finish();
                    ActivityStack.getInstance().finishActivity(DriverTaskListActivity.class);
                }else{
                    Toast.makeText(DriverTaskDetailActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
