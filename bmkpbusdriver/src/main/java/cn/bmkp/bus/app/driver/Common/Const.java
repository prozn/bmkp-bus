package cn.bmkp.bus.app.driver.common;

/**
 * Created by Micheal on 2016/3/22.
 */
public class Const {

    public static final String PREF_NAME = "BMKP-BUS";
    public static final String DEVICE_TYPE_ANDROID = "android";


    //ServerApi
    public static final String PHONE = "phone";
    public static final String VERIFY_ID = "verify_id";
    public static final String VERIFY_CODE = "verify_code";
    public static final String CLIENT_TYPE = "client_type";
    public static final String CLIENT_TOKEN = "client_token";
}
