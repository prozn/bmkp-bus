package cn.bmkp.bus.app.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.R;

/**
 * Created by fm on 2015/11/18.
 */
public class BusLineStationAdapter extends BaseAdapter {

    private Context mContext;
    private List<BusStation> list;
    private int i;

    public BusLineStationAdapter(Context context, List<BusStation> list,int i) {
        mContext = context;
        this.list = list;
        this.i = i;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final BusStation station = list.get(position);
        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_bus_line_station, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.tvNextStation.setVisibility(View.GONE);
        holder.ivBusSmallImg.setImageResource(R.drawable.ic_bus_small_img_pass);
        if (position==(i-1)) {
            holder.tvNextStation.setVisibility(View.VISIBLE);
            holder.ivBusSmallImg.setImageResource(R.drawable.ic_bus_small_img_next);
        }
        holder.tvDepartStationName.setText(station.getName());
        holder.tvStationDetail.setText(station.getDetail());
        holder.tvDepartTime.setText("(" + station.getTime() + ")");
        return convertView;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_bus_line_station.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @Bind(R.id.tv_depart_station_name)
        TextView tvDepartStationName;
        @Bind(R.id.tv_depart_time)
        TextView tvDepartTime;
        @Bind(R.id.tv_station_detail)
        TextView tvStationDetail;
        @Bind(R.id.tv_next_station)
        TextView tvNextStation;
        @Bind(R.id.iv_bus_small_img)
        ImageView ivBusSmallImg;



        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
