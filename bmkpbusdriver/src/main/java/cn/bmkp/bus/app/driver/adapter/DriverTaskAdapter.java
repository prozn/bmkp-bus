package cn.bmkp.bus.app.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.models.Task;
import cn.bmkp.bus.app.driver.utils.DateUtil;


/**
 * Created by lw on 16/3/19.
 */
public class DriverTaskAdapter extends ArrayAdapter<Task> {
    Context context;

    public DriverTaskAdapter(Context context, int resource, List<Task> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Task task = getItem(position);

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_my_task, parent,
                    false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvItemMyTicketFrom.setText(task.getBeginStation());
        holder.tvItemMyTicketTo.setText(task.getEndStation());
        holder.tvItemMyTicketTime.setText(task.getBeginTime());
        holder.tvItemMyTicketDate.setText(DateUtil.getMonthDay(task.getDay()));

        return convertView;
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_my_task.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @Bind(R.id.tv_item_my_ticket_dot1)
        TextView tvItemMyTicketDot1;
        @Bind(R.id.tv_item_my_ticket_date)
        TextView tvItemMyTicketDate;
        @Bind(R.id.tv_item_my_ticket_time)
        TextView tvItemMyTicketTime;
        @Bind(R.id.tv_item_my_ticket_dot2)
        TextView tvItemMyTicketDot2;
        @Bind(R.id.tv_item_my_ticket_from)
        TextView tvItemMyTicketFrom;
        @Bind(R.id.tv_item_my_ticket_line)
        TextView tvItemMyTicketLine;
        @Bind(R.id.tv_item_my_ticket_to)
        TextView tvItemMyTicketTo;
        @Bind(R.id.ll_item_my_ticket)
        LinearLayout llItemMyTicket;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
