package cn.bmkp.bus.app.driver.constant;

public class AndyConstants {

    public static final String RMB = "¥";
    public static final String PREF_NAME = "sharedPref";
    public static final String LOGIN_FRAGMENT_TAG = "loginFragment";
    public static final String REGISTER_FRAGMENT_TAG = "registerFragment";
}
