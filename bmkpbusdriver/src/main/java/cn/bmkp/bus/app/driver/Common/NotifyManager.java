package cn.bmkp.bus.app.driver.common;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.UUID;

import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.activity.DriverTaskListActivity;

/**
 * Created by LW on 2016/3/18.
 */
public class NotifyManager {

    private static NotifyManager notification = new NotifyManager();
    private Context context;
    private NotificationManager manager;

    private NotifyManager(){
    }

    public void init(Context context){
        this.context = context;
    }

    public static NotifyManager getInstance(){
        return notification;
    }

    public void showNotification(String msg) {
        if (context != null) {
            manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intent = new Intent(context, DriverTaskListActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, UUID.randomUUID().hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Notification notification = new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)//图标
                    .setTicker("")//状态栏显示文本
                    .setContentTitle(context.getString(R.string.text_notification_title))//推送标题
                    .setContentText(msg)//推送内容
                    .setContentIntent(pendingIntent)//点击推送跳转
                    .getNotification();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.flags |= Notification.FLAG_SHOW_LIGHTS;
            notification.sound = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, "6");

            manager.notify(1, notification);
        }
    }

}
