package cn.bmkp.bus.app.driver;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.helper.ParseHelper;
import cn.bmkp.bus.app.driver.helper.PreferenceHelper;
import cn.bmkp.bus.app.driver.restful.RestfulApi;
import cn.bmkp.bus.app.driver.restful.ServerApi;
import cn.bmkp.bus.app.driver.wilddog.WilddogApi;
import retrofit.RestAdapter;

/**
 * Created by zhangn on 2016/3/18.
 */
public class BMKPBusDriverApplication extends Application {

    PreferenceHelper preferenceHelper;
    ParseHelper parseHelper;
    RestfulApi mApi;
    private SQLiteDatabase db;
    private DaoMaster daoMaster;

    ServerApi serverApi;

    public ServerApi getApi() {
        return serverApi;
    }

    public ParseHelper getParseHelper() {
        return parseHelper;
    }

    public PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initWilddog();

        initDB();

        initRest();

        preferenceHelper = new PreferenceHelper(this);

    }

    @Override
    public void onTerminate() {

        DBManager.getInstance().cleanAll();
        super.onTerminate();
    }

    /**
     * 初始化数据库
     */
    private void initDB() {

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "bmkpbus-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        DBManager.getInstance().init(daoMaster.newSession());

        parseHelper = new ParseHelper(this);
    }


    private void initWilddog() {

        WilddogApi.getInstance().init(this);
    }

    private void initRest() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(RestfulApi.SERVER_IP)
                .build();
        mApi = restAdapter.create(RestfulApi.class);
        serverApi = new ServerApi(mApi);
    }
}
