package cn.bmkp.bus.app.driver.base;


import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.IOException;
import java.net.SocketTimeoutException;

import cn.bmkp.bus.app.driver.BMKPBusDriverApplication;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.activity.AppStartActivity;
import cn.bmkp.bus.app.driver.activity.LoginActivity;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.dialog.AnnouncementDialog2Fragment;
import cn.bmkp.bus.app.driver.dialog.ProgressDialogFragment;
import cn.bmkp.bus.app.driver.event.NetworkNotAvailableEvent;
import cn.bmkp.bus.app.driver.event.StopCoreServiceEvent;
import cn.bmkp.bus.app.driver.helper.ParseHelper;
import cn.bmkp.bus.app.driver.helper.PreferenceHelper;
import cn.bmkp.bus.app.driver.restful.CallbackHandler;
import cn.bmkp.bus.app.driver.restful.ServerApi;
import cn.bmkp.bus.app.driver.service.LocationService;
import cn.bmkp.bus.app.driver.utils.ActivityStack;
import cn.bmkp.bus.app.driver.utils.AndyUtils;
import de.greenrobot.event.EventBus;


/**
 * Created by fm on 16/3/18.
 */
public class BaseActivity extends FragmentActivity {

    boolean isRunning;
    BaseDialogFragment baseDialog;
    AlertDialog.Builder alertDialog;
    ProgressDialogFragment progressDialog;
    AnnouncementDialog2Fragment announceDialog;

    public BaseDialogFragment getBaseDialog() {
        return baseDialog;
    }


    public ParseHelper getParseHelper() {
        return ((BMKPBusDriverApplication) getApplication()).getParseHelper();
    }

    public PreferenceHelper getPreferenceHelper() {
        return ((BMKPBusDriverApplication) getApplication()).getPreferenceHelper();
    }

    public ServerApi getApi() {
        return ((BMKPBusDriverApplication) getApplication()).getApi();
    }

    public boolean isRunning() {
        return isRunning;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().registerSticky(this);

        ActivityStack.getInstance().pushActivity(this);

        if (!AndyUtils.isNetworkAvailable(this))
            showNetworkDialog();

        if (!AndyUtils.isGpsEnabled(this))
            showGPSDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);

        ActivityStack.getInstance().popActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;

        MobclickAgent.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;

        MobclickAgent.onResume(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onEventMainThread(NetworkNotAvailableEvent event) {
        EventBus.getDefault().removeStickyEvent(NetworkNotAvailableEvent.class);
        showNetworkDialog();
    }

    public void logout() {
        DBManager.getInstance().cleanAll();
        getPreferenceHelper().clear();
        stopService(new Intent(this, LocationService.class));
//        EventBus.getDefault().postSticky(new StopCoreServiceEvent());
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    public void setImage(ImageView view, String url) {
        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(url.trim()))
            ImageLoader.getInstance().displayImage(url, view);
    }

    public void showException(String error) {
        AndyUtils.showToast(error, this);
    }

    public void showException(Throwable throwable) {
        if (throwable.getClass().equals(ConnectTimeoutException.class)) {
            AndyUtils.showToast(getString(R.string.connect_time_out), this);
        } else if (throwable.getClass().equals(SocketTimeoutException.class)) {
            AndyUtils.showToast(getString(R.string.connect_response_time_out), this);
        } else if (throwable.getClass().equals(java.net.ConnectException.class)) {
            AndyUtils.showToast(getString(R.string.network_error), this);
        } else if (throwable.getClass().equals(IOException.class)) {
            AndyUtils.showToast(getString(R.string.network_error), this);
        } else if (throwable.getClass().equals(OutOfMemoryError.class)) {
            AndyUtils.showToast(getString(R.string.out_of_memory), this);
        } else {
            AndyUtils.showToast(throwable.getMessage(), this);
        }
    }

    public void dismissDialog() {
        if (baseDialog != null) {
            baseDialog.dismissAllowingStateLoss();
            baseDialog = null;
        }

        if (progressDialog != null) {
            progressDialog.dismissAllowingStateLoss();
            progressDialog = null;
        }

        if (announceDialog != null) {
            announceDialog.dismissAllowingStateLoss();
            announceDialog = null;
        }
    }

    public void showAnnounceDialog(String id, String url) {
        if (!isRunning)
            return;

        dismissDialog();
        announceDialog = new AnnouncementDialog2Fragment();
        announceDialog.setAnnounceId(id).setUrl(url);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(announceDialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showProgressDialog(String detail) {
        if (isFinishing())
            return;

        dismissDialog();
        progressDialog = new ProgressDialogFragment();
        progressDialog.setDetail(detail);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(progressDialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showInfoDialog(String title, String content) {
        showInfoDialog(title, content, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
            }
        });
    }

    public void showOKCancelDialog(String title, String content,
                                   String positiveText, View.OnClickListener positiveListener) {
        show2BtnDialog(title, content, positiveText, positiveListener, "取消",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismissDialog();
                    }
                }, -1);
    }

    public void showTwoChoiceDialog(String title, String content,
                                    String positiveText, View.OnClickListener positiveListener,
                                    String negativeText, View.OnClickListener negativeListener) {
        show2BtnDialog(title, content, positiveText, positiveListener, negativeText,
                negativeListener, R.drawable.bg_dialog_button_positive);
    }

    public void show2BtnDialog(String title, String content,
                               String positiveText, View.OnClickListener positiveListener,
                               String negativeText, View.OnClickListener negativeListener,
                               int negativeStyle) {
        if (isFinishing())
            return;

        dismissDialog();
        baseDialog = new BaseDialogFragment();
        baseDialog.setTitle(title);
        if (!TextUtils.isEmpty(content))
            baseDialog.setMessage(content);
        baseDialog.setPositiveButton(positiveText, positiveListener);
        baseDialog.setNegativeButton(negativeText, negativeListener);
        baseDialog.setButtonView2(R.dimen.big_margin);
        if (negativeStyle > 0)
            baseDialog.setNegativeButtonStyle(negativeStyle);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(baseDialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showInfoDialog(String title, String content, View.OnClickListener positiveListener) {
        if (isFinishing())
            return;

        dismissDialog();
        baseDialog = new BaseDialogFragment();
        baseDialog.setTitle(title);
        baseDialog.setMessage(content);
        baseDialog.setPositiveButton(getString(R.string.text_confirm), positiveListener);
        baseDialog.setCancelable(false);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(baseDialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showEditDialog(String title, View.OnClickListener positiveListener) {
        if (isFinishing())
            return;

        dismissDialog();
        baseDialog = new BaseDialogFragment();
        baseDialog.setTitle(title);
        baseDialog.setEditTextView(true);
        baseDialog.setPositiveButton(getString(R.string.text_confirm), positiveListener);
        baseDialog.setNegativeButton(getString(R.string.text_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
            }
        });
        baseDialog.setButtonView2(R.dimen.big_margin);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(baseDialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showNetworkDialog() {
        showOKCancelDialog(getString(R.string.text_network_setup_tips), getString(R.string.text_no_network), getString(R.string.text_setting), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                try {
                    Intent intent = null;
                    if (android.os.Build.VERSION.SDK_INT > 10) {
                        intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    } else {
                        intent = new Intent();
                        ComponentName component = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
                        intent.setComponent(component);
                        intent.setAction("android.intent.action.VIEW");
                    }
                    BaseActivity.this.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showGPSDialog() {
        showOKCancelDialog(getString(R.string.text_open_gps), "", getString(R.string.text_confirm), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                try {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showAlertDialog(String title, String msg,
                                String positive, DialogInterface.OnClickListener positiveListenter,
                                String negative, DialogInterface.OnClickListener negativeListenter,
                                Context context) {
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(positive, positiveListenter);
        alertDialog.setNegativeButton(negative, negativeListenter);
        alertDialog.show();
    }

    protected class SimpleCallbackHandler extends CallbackHandler {

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            dismissDialog();
            showException(throwable);
        }
    }

    
}
