package cn.bmkp.bus.app.driver.helper;

import android.app.NotificationManager;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


public class ParseHelper {

    PreferenceHelper preferenceHelper;
    Context context;
    NotificationManager notificationManager;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DecimalFormat df = new DecimalFormat("0.0");

    public ParseHelper(Context context) {
        this.context = context;

        preferenceHelper = new PreferenceHelper(context);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

//    public DBManager getDbHelper() {
//        return ((BMKPBusDriverApplication) context.getApplicationContext()).getDbManager();
//    }

    //解析验证码
    public String getVerifyId(JSONObject data) throws JSONException {
        if (data == null) {
            return "";
        }
        return data.getString("verify_id");
    }

    //解析登录后的数据
    public String parseToken(JSONObject data) throws JSONException {
        if (data == null) {
            return "";
        }
        return data.getString("token");
    }
}
