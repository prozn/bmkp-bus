package cn.bmkp.bus.app.driver.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.bmkp.bus.app.driver.R;


@SuppressLint("NewApi")
public class AndyUtils {
    static float density = 1;
    private static ProgressDialog mProgressDialog;
    private static Dialog mDialog;

    public static void showSimpleProgressDialog(Context context, String title,
                                                String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showSimpleProgressDialog(Context context) {
        showSimpleProgressDialog(context, null, "Loading...", false);
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showCustomProgressDialog(Context context, String title,
                                                String details, boolean iscancelable) {
        removeCustomProgressDialog();
        mDialog = new Dialog(context, R.style.MyDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_progress);
        ImageView imageView = (ImageView) mDialog
                .findViewById(R.id.iv_dialog_progress);
        ((TextView) mDialog.findViewById(R.id.tv_dialog_title)).setText(title);
        ((TextView) mDialog.findViewById(R.id.tv_dialog_detail))
                .setText(details);
//		imageView.setBackgroundResource(R.drawable.new_loading);
        final AnimationDrawable frameAnimation = (AnimationDrawable) imageView
                .getBackground();
        mDialog.setCancelable(iscancelable);
        imageView.post(new Runnable() {

            @Override
            public void run() {
                frameAnimation.start();
                frameAnimation.setOneShot(false);
            }
        });

        mDialog.show();
    }

    public static void removeCustomProgressDialog() {
        try {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isGpsEnabled(Context context) {
        boolean enable = false;

        try {
            LocationManager locationManager = ((LocationManager) context.
                    getSystemService(Context.LOCATION_SERVICE));
            enable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return enable;
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }



    public static void showToast(String msg, Context ctx) {
        if (ctx != null)
            Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(String msg, Context ctx) {
        if (ctx != null)
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static void showErrorToast(int id, Context ctx) {
        String msg = "未知错误";
        switch (id) {
            case 401:
                msg = ctx.getResources().getString(R.string.error_401);
                break;
            case 402:
                msg = ctx.getResources().getString(R.string.error_402);
                break;
            case 403:
                msg = ctx.getResources().getString(R.string.error_403);
                break;
            case 404:
                msg = ctx.getResources().getString(R.string.error_404);
                break;
            case 405:
                msg = ctx.getResources().getString(R.string.error_405);
                break;
            case 406:
                msg = ctx.getResources().getString(R.string.error_406);
                break;
            case 407:
                msg = ctx.getResources().getString(R.string.error_407);
                break;
            case 408:
                msg = ctx.getResources().getString(R.string.error_408);
                break;
            case 409:
                msg = ctx.getResources().getString(R.string.error_409);
                break;
            case 410:
                msg = ctx.getResources().getString(R.string.error_410);
                break;
            case 411:
                msg = ctx.getResources().getString(R.string.error_411);
                break;
            case 412:
                msg = ctx.getResources().getString(R.string.error_412);
                break;
            case 413:
                msg = ctx.getResources().getString(R.string.error_413);
                break;
            case 414:
                msg = ctx.getResources().getString(R.string.error_414);
                break;
            case 415:
                msg = ctx.getResources().getString(R.string.error_415);
                break;
            case 416:
                msg = ctx.getResources().getString(R.string.error_416);
                break;
            case 417:
                msg = ctx.getResources().getString(R.string.error_417);
                break;

            case 903:
                msg = ctx.getResources().getString(R.string.error_903);
                break;

            default:
                break;
        }
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public static String getVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return context.getResources().getString(R.string.can_not_find_version_name);
        }
    }

    /**
     * @param context
     * @return 是否前台运行
     */
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * 判断当前应用程序处于前台还是后台
     *
     * @param context
     * @return
     */
    public static boolean isApplicationBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }
}
