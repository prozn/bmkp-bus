package cn.bmkp.bus.app.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.models.Station;
import cn.bmkp.bus.app.driver.models.Task;
import cn.bmkp.bus.app.driver.service.LocationService;

public class FinishTaskActivity extends BaseActivity {

    @Bind(R.id.tv_finish_station_start_start_name)
    TextView tvFinishStationStartStartName;
    @Bind(R.id.tv_finish_station_start_end_name)
    TextView tvFinishStationStartEndName;
    @Bind(R.id.tv_finish_station_start_current_station)
    TextView tvFinishStationStartCurrentStation;

    private Task task;
    private Station station;

    @OnClick(R.id.btn_finish_task)
    public void confirm(View v) {
        startActivity(new Intent(FinishTaskActivity.this, DriverTaskListActivity.class));
        FinishTaskActivity.this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_finish_task);
        ButterKnife.bind(this);

        setData();

    }

    private void setData() {

        BusInfo busInfo = DBManager.getInstance().getBusInfo();

        tvFinishStationStartStartName.setText(busInfo.getBeginStation());
        tvFinishStationStartEndName.setText(busInfo.getEndStation());
        tvFinishStationStartCurrentStation.setText(busInfo.getEndStation());

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DriverTaskListActivity.class));
        stopService(new Intent(FinishTaskActivity.this, LocationService.class));
        finish();
    }


}
