package cn.bmkp.bus.app.driver.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import cn.bmkp.bus.app.driver.BMKPBusDriverApplication;
import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.helper.PreferenceHelper;
import cn.bmkp.bus.app.driver.restful.CallbackHandler;
import cn.bmkp.bus.app.driver.utils.LogHelper;
import cn.bmkp.bus.app.driver.wilddog.WilddogApi;

public class LocationService extends Service implements AMapLocationListener {

    private AMapLocationClient mLocationClient;
    private AMapLocationClientOption mLocationOption;
    private PowerManager.WakeLock wakeLock = null;

    ScheduledExecutorService scheduler;
    ScheduledFuture reportLocationHandle;

    private String lat;

    private String lon;

    public PreferenceHelper getPreferenceHelper() {
        return ((BMKPBusDriverApplication) getApplication()).getPreferenceHelper();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initAMap();  // 启动高德
        initScheduleReport();
        aquireWakeLock(); // 请求WalkLock
    }

    private void initScheduleReport(){

        scheduler = Executors.newScheduledThreadPool(2);
        //上报位置
        reportLocationHandle = scheduler.scheduleAtFixedRate(reportLocationWork, 0, 10, TimeUnit.SECONDS);
    }

    private void stopScheduleReport(){

        if(reportLocationHandle != null) {
            reportLocationHandle.cancel(true);
        }
    }

    /**
     * 初始化高德地图，启动定位
     */
    private void initAMap(){
        mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        mLocationOption.setInterval(3 * 1000);
        mLocationClient = new AMapLocationClient(this);
        mLocationClient.setLocationListener(this);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.startLocation();
    }

    /**
     * 停止高德地图
     */
    private void stopAMap(){

        // 在Service销毁的时候销毁定位资源
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
            mLocationClient.onDestroy();
        }
        mLocationClient = null;
    }

    private void aquireWakeLock(){

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                LocationService.class.getName());
        wakeLock.acquire();
    }

    private void releaseWakeLock(){

        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopAMap();
        releaseWakeLock();
        stopScheduleReport();

    }


    /**
     * 上报司机位置
     */
    Runnable reportLocationWork = new Runnable() {
        @Override
        public void run() {
            try {

                BusInfo busInfo = DBManager.getInstance().getBusInfo();

                Log.d("zhangn", "number===>"+busInfo.getBusnumber());

                WilddogApi.getInstance().reportBusLocation(busInfo.getBusnumber(), lat, lon, new Wilddog.CompletionListener() {
                    @Override
                    public void onComplete(WilddogError wilddogError, Wilddog wilddog) {

                    }
                });

            } catch (Throwable t) {
                LogHelper.e(LocationService.class, "doWorkError" + t.getMessage());
            }
        }
    };

    class ReportHandler extends CallbackHandler {

        @Override
        public void onSuccess(JSONObject data) {

        }

        @Override
        public void onError(int code, JSONObject data) {

            LogHelper.e(LocationService.class, "位置上报失败");
        }

        @Override
        public void onFailure(Throwable throwable) {
            LogHelper.e(LocationService.class, throwable != null ? throwable.getMessage() : "位置上报异常");
        }
    }


    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation == null || aMapLocation.getErrorCode() != 0) {
            Log.e("LocationService", "[errorcode: " + aMapLocation.getErrorCode() + "] [errormessage: " + aMapLocation.getErrorInfo() + "]");
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Provider: ").append(aMapLocation.getProvider().toString())
                .append(" 类型：").append(aMapLocation.getLocationType())
                .append(" 精度:").append(aMapLocation.getAccuracy())
                .append(" 纬度:").append(aMapLocation.getLatitude())
                .append(" 经度:").append(aMapLocation.getLongitude())
                .append(" 地址:").append(aMapLocation.getAddress())
                .append(" 速度:").append(aMapLocation.getSpeed())
                .append(" time : ").append(aMapLocation.getTime());

        Log.i("LocationService", sb.toString());

        if (aMapLocation.getLocationType() == 1) {
            lat = aMapLocation.getLatitude() + "";
            lon = aMapLocation.getLongitude() + "";
            getPreferenceHelper().putWalkerLatitude(aMapLocation.getLatitude() + "");
            getPreferenceHelper().putWalkerLongitude(aMapLocation.getLongitude() + "");
            getPreferenceHelper().putWalkerLocationAddress(aMapLocation.getAddress());
            getPreferenceHelper().putWalkerSpeed(aMapLocation.getSpeed());
            getPreferenceHelper().putWalkerLType(aMapLocation.getLocationType());
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
