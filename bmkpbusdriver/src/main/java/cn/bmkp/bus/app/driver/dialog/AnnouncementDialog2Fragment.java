package cn.bmkp.bus.app.driver.dialog;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.utils.DeviceUtil;


/**
 * Created by pandeng on 15/10/29.
 */
public class AnnouncementDialog2Fragment extends DialogFragment {

    private Dialog dialog;
    TextView tvGotoDetail;
    ImageView ivImg;
    ImageButton ibClose;
    String announceId;
    String url;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_announcement2, null);
        tvGotoDetail = ((TextView) view.findViewById(R.id.tv_go_to_detail));
        ivImg = ((ImageView) view.findViewById(R.id.iv_img));
        ibClose = (ImageButton) view.findViewById(R.id.ib_close);

        tvGotoDetail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();

//                Intent intent = new Intent();
//                intent.setClass(getActivity(), AnnouncementInfoActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("id", announceId);
//                intent.putExtras(bundle);
//                startActivity(intent);
            }
        });

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissAllowingStateLoss();
            }
        });

        LinearLayout.LayoutParams lpImg = new LinearLayout
                .LayoutParams((int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8)
                , (int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8));
        ivImg.setLayoutParams(lpImg);

        ImageLoader.getInstance().displayImage(url, ivImg, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {

            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });

        ViewGroup.LayoutParams lp = new ViewGroup
                .LayoutParams((int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8)
                , ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(view, lp);
        dialog.setCancelable(true);

        return dialog;
    }

    public AnnouncementDialog2Fragment setAnnounceId(String id) {
        announceId = id;
        return this;
    }

    public AnnouncementDialog2Fragment setUrl(String u) {
        url = u;
        return this;
    }
}
