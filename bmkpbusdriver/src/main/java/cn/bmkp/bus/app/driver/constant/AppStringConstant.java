package cn.bmkp.bus.app.driver.constant;

/**
 * Created by zhangn on 2016/3/24.
 *
 * description:app中使用的字符串常量
 */
public class AppStringConstant {

    /**
     * 参数常量
     */
    public static class Param{

        public static String KEY_TASK = "task";
    }

}
