package cn.bmkp.bus.app.driver.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DecimalFormat;

import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.constant.AndyConstants;
import cn.bmkp.bus.app.driver.utils.DeviceUtil;


/**
 * Created by pandeng on 15/11/13.
 */
public class OrderInfoDialogFragment extends DialogFragment {

    private Dialog dialog;
    TextView tvOwnerName;
    TextView tvStartAddr;
    TextView tvEndAddr;
    TextView tvOtherFee;
    TextView tvSendFee;
    LinearLayout layoutOrderRemark;
    TextView tvRemark;
    ImageView ivDialogUser;
    ImageButton ibClose;

    String ownerName;
    String portrait;
    String startAddress;
    String destinationAddress;
    double otherFee;
    double transportFee;
    String remark;
    private DecimalFormat df;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_order_user_info, null);

        tvOwnerName = (TextView) view.findViewById(R.id.tv_owner_name);
        tvStartAddr = (TextView) view.findViewById(R.id.tv_start_address);
        tvEndAddr = (TextView) view.findViewById(R.id.tv_end_address);
        tvOtherFee = (TextView) view.findViewById(R.id.tv_other_fee);
        tvSendFee = (TextView) view.findViewById(R.id.tv_send_fee);
        layoutOrderRemark = (LinearLayout) view.findViewById(R.id.ll_order_remark);
        tvRemark = (TextView) view.findViewById(R.id.tv_remark);
        ivDialogUser = (ImageView) view.findViewById(R.id.iv_dialog_user);
        ibClose = (ImageButton) view.findViewById(R.id.ib_close);

        df = new DecimalFormat("0.00");
        tvOwnerName.setText(ownerName);
        tvStartAddr.setText(startAddress);
        tvEndAddr.setText(destinationAddress);
        tvOtherFee.setText(AndyConstants.RMB + df.format(otherFee));
        tvSendFee.setText(AndyConstants.RMB + df.format(transportFee));

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissAllowingStateLoss();
            }
        });

        if (TextUtils.isEmpty(remark)) {
            layoutOrderRemark.setVisibility(View.GONE);
        } else {
            tvRemark.setText(remark);
        }

        if (!TextUtils.isEmpty(portrait) && !TextUtils.isEmpty(portrait.trim()))
            ImageLoader.getInstance().displayImage(portrait, ivDialogUser);

        ViewGroup.LayoutParams lp = new ViewGroup
                .LayoutParams((int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8)
                , ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(view, lp);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    public OrderInfoDialogFragment setOwnerName(String name) {
        ownerName = name;
        return this;
    }

    public OrderInfoDialogFragment setPortrait(String p) {
        portrait = p;
        return this;
    }

    public OrderInfoDialogFragment setStartAddress(String a) {
        startAddress = a;
        return this;
    }

    public OrderInfoDialogFragment setDestinationAddress(String a) {
        destinationAddress = a;
        return this;
    }

    public OrderInfoDialogFragment setOtherFee(double f) {
        otherFee = f;
        return this;
    }

    public OrderInfoDialogFragment setTransportFee(double f) {
        transportFee = f;
        return this;
    }

    public OrderInfoDialogFragment setRemark(String r) {
        remark = r;
        return this;
    }
}
