package cn.bmkp.bus.app.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.utils.ActivityStack;

/**
 * Created by lw on 2016/3/23
 * <p/>
 * 途经站发车界面
 */

public class StationStartActivity extends BaseActivity {

    @Bind(R.id.tv_station_start_start_name)
    TextView tvStationStartStartName;
    @Bind(R.id.tv_station_start_end_name)
    TextView tvStationStartEndName;
    @Bind(R.id.tv_station_start_current_station)
    TextView tvStationStartCurrentStation;
    @Bind(R.id.tv_station_start_driver_tip_time)
    TextView tvStationStartDriverTipTime;
    private int position;

    @OnClick(R.id.btn_finish)
    public void start(View v){
        Intent intent = new Intent(StationStartActivity.this, TaskStationListActivity.class);
        ActivityStack.getInstance().finishActivity(ArriveStationActivity.class);
        startActivity(intent);
        StationStartActivity.this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_station_start);
        ButterKnife.bind(this);

        setData();
    }

    private void setData() {


        BusInfo busInfo = DBManager.getInstance().getBusInfo();

        int nextStation = Integer.valueOf(getCurrentStation().getKey());
        int currentStation = nextStation-2;
        BusStation station = busInfo.getStations().get(currentStation);

        tvStationStartStartName.setText(busInfo.getBeginStation());
        tvStationStartEndName.setText(busInfo.getEndStation());
        tvStationStartCurrentStation.setText(station.getName());
        tvStationStartDriverTipTime.setText(station.getTime());
    }

    /**
     * 获取当前是哪一站
     */
    private BusStation getCurrentStation(){

        List<BusStation> stations = DBManager.getInstance().getBusInfo().getStations();

        for(BusStation station : stations) {

            if(!station.getArrive()) {
                return  station;
            }
        }

        return null;
    }


}
