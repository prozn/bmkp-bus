package cn.bmkp.bus.app.driver.restful;


import java.util.Map;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by pandeng on 15/9/8.
 */
public interface RestfulApi {

    String SERVER_IP = "http://106.75.132.41:89";
    String SERVER_ROUTE = "/owner";
    String SERVER_TOKEN_REFRESH = "/renew";


    @POST(SERVER_ROUTE + "/login")
    void login(@Body Map<String, String> map, Callback<Response> response);

    @POST("/verifycode")
    void getSms(@Body Map<String, String> map, Callback<Response> response);

    @POST(SERVER_ROUTE + "/verifylogin")
    void verifyLogin(@Body Map<String, String> map, Callback<Response> response);

    @POST(SERVER_TOKEN_REFRESH)
    void refreshToken(@Header("Authorization") String token, @Body Map<String, String> map, Callback<Response> response);
}
