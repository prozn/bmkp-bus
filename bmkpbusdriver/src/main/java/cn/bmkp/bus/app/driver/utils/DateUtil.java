package cn.bmkp.bus.app.driver.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by zhangn on 2016/3/28.
 */
public class DateUtil {

    private static String KEY_FORMAT = "yyyyMMdd";


    /**
     * 解析月日
     * @param dateStr
     * @return
     */
    public static String getMonthDay(String dateStr){

        SimpleDateFormat df = new SimpleDateFormat(KEY_FORMAT);
        String monthDay;
        //班次时间
        try {
            Date date = df.parse(dateStr);
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);
            monthDay =  String.valueOf(ca.get(Calendar.MONTH)+1) + "月"
                    + String.valueOf(ca.get(Calendar.DATE)) + "日";
        } catch (ParseException e) {
            monthDay = dateStr;
        }

        return monthDay;
    }
}
