package cn.bmkp.bus.app.driver.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.update.UmengUpdateAgent;
import com.wilddog.client.AuthData;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.adapter.DriverTaskAdapter;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.constant.AppStringConstant;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.models.BusRun;
import cn.bmkp.bus.app.driver.models.DriveTask;
import cn.bmkp.bus.app.driver.models.Task;
import cn.bmkp.bus.app.driver.restful.CallbackHandler;
import cn.bmkp.bus.app.driver.service.LocationService;
import cn.bmkp.bus.app.driver.utils.ActivityStack;
import cn.bmkp.bus.app.driver.utils.ParseUtil;
import cn.bmkp.bus.app.driver.wilddog.WilddogApi;


/**
 * description:司机的任务列表
 */
public class DriverTaskListActivity extends BaseActivity {

    @Bind(R.id.lv_my_tasks)
    ListView lvMyTasks;
    @Bind(R.id.tv_my_tasks_empty)
    TextView tvMyTasksEmpty;

    private DriverTaskAdapter taskAdapter;

    private List<Task> tasks;
    private long exitTime = 0;
    private BusRun busRun;

    //退出登录
    @OnClick(R.id.iv_toolbar_exit)
    void exit(View view) {
        showAlertDialog("", getString(R.string.text_exit_or_not), getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        }, getString(R.string.text_cancel), null, this);
    }

    @OnItemClick(R.id.lv_my_tasks)
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

        startLocationService();
        
        WilddogApi.getInstance().getDriverTaskDetail(tasks.get(position).getLine(), tasks.get(position).getDay(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                busRun = ParseUtil.parseBusRun(dataSnapshot);
                saveBusInfo(busRun, tasks.get(position));

                if (busRun.getStations().get(0).isArrive()){
                    Intent intent = new Intent(DriverTaskListActivity.this, TaskStationListActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(DriverTaskListActivity.this, DriverTaskDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppStringConstant.Param.KEY_TASK, tasks.get(position));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                Toast.makeText(DriverTaskListActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_task);

        ButterKnife.bind(this);
        showProgressDialog("加载中…");
        loadData();

        checkAndUpdateToken();

        checkAppUpdateApp();

        //测试推送
//        NotifyManager.getInstance().init(this);
//        NotifyManager.getInstance().showNotification("测试测试");

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }


    /**
     * 检查并刷新Token
     */
    private void checkAndUpdateToken(){

        String token = getPreferenceHelper().getToken();

        if (TextUtils.isEmpty(token)) {
            getPreferenceHelper().clear();

        }else {

           getApi().refreshToken(token, new RefreshTokenCallback());
        }
    }

    class RefreshTokenCallback extends CallbackHandler {
        @Override
        public void onSuccess(JSONObject data) {
            super.onSuccess(data);

            try{
                authToken(getParseHelper().parseToken(data));
            }catch (Exception e) {

                getPreferenceHelper().clear();
            }
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            getPreferenceHelper().clear();
        }

        @Override
        public void onError(int code, JSONObject data) {
            super.onError(code, data);
            getPreferenceHelper().clear();
        }
    }

    /**
     * token认证
     * @param token
     */
    private void authToken(final String token){

        WilddogApi.getInstance().authWithToken(token, new Wilddog.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                getPreferenceHelper().putToken(token);

                Log.d("zhangn", "refresh token ===>succ");
            }

            @Override
            public void onAuthenticationError(WilddogError wilddogError) {
                getPreferenceHelper().clear();
                Log.d("zhangn", "refresh token ===>fail");
            }
        });
    }

    /**
     * 从Wilddog上加载司机任务列表
     */
    public void loadData() {

        String userPhone = getPreferenceHelper().getUserPhone();

        //此处填充数据
        WilddogApi.getInstance().getDriverTasks(userPhone, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DriveTask driveTask = ParseUtil.parseDriveTask(dataSnapshot);

                tasks = driveTask.getTasks();
                if (tasks.size() == 0) {
                    tvMyTasksEmpty.setVisibility(View.VISIBLE);
                    lvMyTasks.setVisibility(View.GONE);
                } else {
                    tvMyTasksEmpty.setVisibility(View.GONE);
                    lvMyTasks.setVisibility(View.VISIBLE);
                    taskAdapter = new DriverTaskAdapter(DriverTaskListActivity.this, R.layout.list_item_my_task, tasks);
                    lvMyTasks.setAdapter(taskAdapter);
                }
                dismissDialog();
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                Toast.makeText(DriverTaskListActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();

                lvMyTasks.setVisibility(View.GONE);
                tvMyTasksEmpty.setVisibility(View.VISIBLE);
                dismissDialog();
            }
        });
    }

    /**
     * 检查app是否有更新
     */
    private void checkAppUpdateApp() {
        UmengUpdateAgent.setUpdateOnlyWifi(false);
        UmengUpdateAgent.setDeltaUpdate(false);
        UmengUpdateAgent.update(getApplicationContext());
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(getApplicationContext(), R.string.text_press_again_to_exit, Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                ActivityStack.getInstance().finishAllActivity();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 存储司机选定的班次信息
     */
    private void saveBusInfo(BusRun busRun, Task task){

        DBManager.getInstance().insert(busRun, task);
    }

    /**
     * 启动定位服务，上报位置
     */
    private void startLocationService(){

        startService(new Intent(DriverTaskListActivity.this, LocationService.class));
    }
}
