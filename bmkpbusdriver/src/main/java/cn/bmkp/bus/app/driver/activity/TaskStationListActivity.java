package cn.bmkp.bus.app.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.adapter.BusLineStationAdapter;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.models.Latlng;

/**
 * Created by chenliang on 2016/3/19.
 */
public class TaskStationListActivity extends BaseActivity {

    @Bind(R.id.tv_task_station_start_name)
    TextView tvTaskStationStartName;
    @Bind(R.id.tv_task_station_end_name)
    TextView tvTaskStationEndName;
    @Bind(R.id.tv_task_station_depart_time)
    TextView tvTaskStationDepartTime;
    @Bind(R.id.tv_task_station_head_start_detail)
    TextView tvTaskStationHeadStartDetail;
    @Bind(R.id.tv_task_station_end_detail)
    TextView tvTaskStationEndDetail;
    @Bind(R.id.rl_task_station_info)
    RelativeLayout rlTaskStationInfo;
    @Bind(R.id.lv_bus_stations)
    ListView lvBusStations;

    private int position;

    private BusInfo mBusInfo;

    private Latlng latlng;


    /**
     * 到站
     * @param view
     */
    @OnClick(R.id.tv_arrive_station)
    public void arriveStation(View view) {

        //断当前经纬度与站点经纬度的距离

//        double latitude1 = Double.valueOf(getPreferenceHelper().getWalkerLatitude());
//        double longitude1 = Double.valueOf(getPreferenceHelper().getWalkerLongitude());
//
//        WilddogApi.getInstance().getStationLocation(getCurrentStation().getName(), new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                latlng = ParseUtil.getStationLocation(dataSnapshot);
//            }
//
//            @Override
//            public void onCancelled(WilddogError wilddogError) {
//
//            }
//        });

        //距离
//        String m = Location2Range.DistanceToText(Location2Range.GetDistance(latitude1, longitude1, latlng.getLatitude(), latlng.getLongitude()));

//        if (true) {
//            showAlertDialog("", "您当前位置距离指定停车点较远，确认已到站？", "确认", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {

                    // 发车
                    requestArrivePoint();
//
//
//                }
//            }, "取消", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                }
//            }
//                    , TaskStationListActivity.this);
//        } else {
//            startActivity(new Intent(TaskStationListActivity.this, ArriveStationActivity.class));
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_task_next_station);
        ButterKnife.bind(this);

        rlTaskStationInfo.getBackground().setAlpha(204);
        lvBusStations.getBackground().setAlpha(100);

        setData();

    }

    public void setData() {
//        Intent intent = getIntent();
//        String resource = intent.getStringExtra("resource");

//        if (resource.equals("DriverTaskDetailActivity")) {
//            position = 1;
//        } else {
//            position = Integer.valueOf(intent.getStringExtra("position"));
//            position++;
//        }
        mBusInfo = DBManager.getInstance().getBusInfo();
        tvTaskStationStartName.setText(mBusInfo.getBeginStation());
        tvTaskStationEndName.setText(mBusInfo.getEndStation());
        tvTaskStationDepartTime.setText("(" + mBusInfo.getBeginTime() + "发车)");
        tvTaskStationHeadStartDetail.setText(mBusInfo.getBeginStation());
        tvTaskStationEndDetail.setText(mBusInfo.getEndStation());

        //即将到站的position
        int nextStation = Integer.valueOf(getCurrentStation().getKey());
        BusLineStationAdapter adapter = new BusLineStationAdapter(this, mBusInfo.getStations(),nextStation );
        lvBusStations.setAdapter(adapter);
    }

    /**
     * 获取当前是哪一站
     */
    private BusStation getCurrentStation(){

        List<BusStation> stations = DBManager.getInstance().getBusInfo().getStations();

        for(BusStation station : stations) {

            if(!station.getArrive()) {
                return  station;
            }
        }
        return null;
    }


    /**
     * 触发到站
     */
    private void requestArrivePoint(){

        Intent intent = new Intent(TaskStationListActivity.this, ArriveStationActivity.class);
//                intent.putExtra("position", position + "");
        startActivity(intent);
    }

}

