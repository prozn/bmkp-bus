package cn.bmkp.bus.app.driver.base;

import android.app.Dialog;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.utils.DeviceUtil;


/**
 * Created by fanming on 2015/10/20
 */
public class BaseDialogFragment extends DialogFragment {

    private Dialog dialog;
    private ImageView ivPicture;
    private TextView tvTitle;
    private View titleLine;
    private LinearLayout layoutTitle;
    private TextView tvMessage;
    private EditText etRemark;
    private LinearLayout layoutMessage;
    private Button neutralBtn;
    private Button negativeBtn;
    private Button positiveBtn;
    private View buttonView1,buttonView2;
    private LinearLayout layoutButton;
    private LinearLayout layoutOtherBtn;
    private TextView tvOtherBtn;

    private CharSequence mTitle = null;
    private CharSequence mMessage;
    private CharSequence mPositiveButtonText;
    private View.OnClickListener mPositiveButtonListener;
    private int mPositiveButtonStyle = -1;
    private CharSequence mNegativeButtonText;
    private View.OnClickListener mNegativeButtonListener;
    private int mNegativeButtonStyle = -1;
    private CharSequence mNeutralButtonText;
    private View.OnClickListener mNeutralButtonListener;
    private int mNeutralButtonStyle = -1;
    private int mButtonViewDimen1 = -1;
    private int mButtonViewDimen2 = -1;
    private CharSequence mOtherButtonText;
    private View.OnClickListener mOtherButtonListener;
    private String mImgPath;
    private boolean isEdit, isOtherBtn;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.base_dialog, null);
        ivPicture = (ImageView) view.findViewById(R.id.iv_picture);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        titleLine = view.findViewById(R.id.titleline);
        layoutTitle = (LinearLayout) view.findViewById(R.id.rl_title);
        tvMessage = (TextView) view.findViewById(R.id.tv_message);
        etRemark = (EditText) view.findViewById(R.id.et_remark);
        layoutMessage = (LinearLayout) view.findViewById(R.id.ll_message);
        neutralBtn = (Button) view.findViewById(R.id.btn_neutral);
        negativeBtn = (Button) view.findViewById(R.id.btn_negative);
        positiveBtn = (Button) view.findViewById(R.id.btn_positive);
        buttonView1 = view.findViewById(R.id.view_button1);
        buttonView2 = view.findViewById(R.id.view_button2);
        layoutButton = (LinearLayout) view.findViewById(R.id.ll_button);
        layoutOtherBtn = (LinearLayout) view.findViewById(R.id.ll_other_btn);
        tvOtherBtn = (TextView) view.findViewById(R.id.tv_other_btn);

        ViewGroup.LayoutParams lp = new ViewGroup
                .LayoutParams((int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8)
                , ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(view, lp);

        setDialog();

        return dialog;
    }

    private void setDialog() {
        if (!TextUtils.isEmpty(mImgPath)) {
            ivPicture.setVisibility(View.VISIBLE);
            ivPicture.setImageBitmap(BitmapFactory.decodeFile(mImgPath));
        }
        if (!TextUtils.isEmpty(mTitle)) {
            layoutTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(mTitle);
        }
        if (!TextUtils.isEmpty(mMessage)) {
            layoutMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(mMessage);
        } else {
            titleLine.setVisibility(View.VISIBLE);
        }
        if (isEdit) {
            etRemark.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(mPositiveButtonText) || !TextUtils.isEmpty(mNegativeButtonText) || !TextUtils.isEmpty(mNeutralButtonText)) {
            layoutButton.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(mPositiveButtonText)) {
            positiveBtn.setVisibility(View.VISIBLE);
            positiveBtn.setText(mPositiveButtonText);
            positiveBtn.setOnClickListener(mPositiveButtonListener);
        }
        if (mPositiveButtonStyle != -1) {
            positiveBtn.setBackgroundResource(mPositiveButtonStyle);
        }
        if (!TextUtils.isEmpty(mNegativeButtonText)) {
            negativeBtn.setVisibility(View.VISIBLE);
            negativeBtn.setText(mNegativeButtonText);
            negativeBtn.setOnClickListener(mNegativeButtonListener);
        }
        if (mNegativeButtonStyle != -1) {
            negativeBtn.setBackgroundResource(mNegativeButtonStyle);
        }
        if (!TextUtils.isEmpty(mNeutralButtonText)) {
            neutralBtn.setVisibility(View.VISIBLE);
            neutralBtn.setText(mNeutralButtonText);
            neutralBtn.setOnClickListener(mNeutralButtonListener);
        }
        if (mNeutralButtonStyle != -1) {
            neutralBtn.setBackgroundResource(mNeutralButtonStyle);
        }
        if(mButtonViewDimen1 != -1){
            ViewGroup.LayoutParams lp = buttonView1.getLayoutParams();
            lp.width =  (int)getResources().getDimension(mButtonViewDimen1);
            buttonView1.setLayoutParams(lp);
        }
        if(mButtonViewDimen2 != -1){
            ViewGroup.LayoutParams lp = buttonView2.getLayoutParams();
            lp.width = (int)getResources().getDimension(mButtonViewDimen2);
            buttonView2.setLayoutParams(lp);
        }
        if(isOtherBtn){
            layoutOtherBtn.setVisibility(View.VISIBLE);
            tvOtherBtn.setText(mOtherButtonText);
            tvOtherBtn.setOnClickListener(mOtherButtonListener);
        }
    }

    public BaseDialogFragment setTitle(int titleId) {
        mTitle = getString(titleId);
        return this;
    }

    public BaseDialogFragment setTitle(CharSequence title) {
        mTitle = title;
        return this;
    }

    public BaseDialogFragment setPositiveButton(int textId, final View.OnClickListener listener) {
        mPositiveButtonText = getString(textId);
        mPositiveButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setPositiveButton(CharSequence text, final View.OnClickListener listener) {
        mPositiveButtonText = text;
        mPositiveButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setPositiveButtonStyle(int drawableId) {
        mPositiveButtonStyle = drawableId;
        return this;
    }

    public BaseDialogFragment setNegativeButton(int textId, final View.OnClickListener listener) {
        mNegativeButtonText = getString(textId);
        mNegativeButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setNegativeButton(CharSequence text, final View.OnClickListener listener) {
        mNegativeButtonText = text;
        mNegativeButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setNegativeButtonStyle(int drawableId) {
        mNegativeButtonStyle = drawableId;
        return this;
    }

    public BaseDialogFragment setNeutralButton(int textId, final View.OnClickListener listener) {
        mNeutralButtonText = getText(textId);
        mNeutralButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setNeutralButton(CharSequence text, final View.OnClickListener listener) {
        mNeutralButtonText = text;
        mNeutralButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setNeutralButtonStyle(int drawableId) {
        mNeutralButtonStyle = drawableId;
        return this;
    }

    //
    public BaseDialogFragment setMessage(int messageId) {
        mMessage = getText(messageId);
        return this;
    }

    public BaseDialogFragment setMessage(CharSequence message) {
        mMessage = message;
        return this;
    }

    public BaseDialogFragment setImage(String path) {
        mImgPath = path;
        return this;
    }

    public BaseDialogFragment setEditTextView(boolean isEdit) {
        this.isEdit = isEdit;
        return this;
    }

    public String getEditText() {
        return etRemark.getText().toString();
    }

    public BaseDialogFragment setButtonView1(int dimenId) {
        mButtonViewDimen1 = dimenId;
        return this;
    }

    public BaseDialogFragment setButtonView2(int dimenId) {
        mButtonViewDimen2 = dimenId;
        return this;
    }

    public BaseDialogFragment setOtherView(boolean isOtherBtn) {
        this.isOtherBtn = isOtherBtn;
        return this;
    }

    public BaseDialogFragment setOtherButton(int textId, final View.OnClickListener listener) {
        mOtherButtonText = getText(textId);
        mOtherButtonListener = listener;
        return this;
    }

    public BaseDialogFragment setOtherButton(CharSequence text, final View.OnClickListener listener) {
        mOtherButtonText = text;
        mOtherButtonListener = listener;
        return this;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}