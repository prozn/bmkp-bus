package cn.bmkp.bus.app.driver.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by LW on 2016/3/19.
 */
public class TestTask implements Serializable {
    private String date;
    private String time;
    private String from;
    private String to;
    private String driver_name;
    private String customer_number;
    private String car_number;
    private List<Station> stations;

    public TestTask(String date, String time, String from, String to, String driver_name, String customer_number, String car_number) {
        this.date = date;
        this.time = time;
        this.from = from;
        this.to = to;
        this.driver_name = driver_name;
        this.customer_number = customer_number;
        this.car_number = car_number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getCustomer_number() {
        return customer_number;
    }

    public void setCustomer_number(String customer_number) {
        this.customer_number = customer_number;
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }
}
