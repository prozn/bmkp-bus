package cn.bmkp.bus.app.driver.utils;

import com.wilddog.client.DataSnapshot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.bmkp.bus.app.driver.models.BusRun;
import cn.bmkp.bus.app.driver.models.DriveTask;
import cn.bmkp.bus.app.driver.models.Latlng;
import cn.bmkp.bus.app.driver.models.Station;
import cn.bmkp.bus.app.driver.models.Task;

/**
 * Created by zhangn on 2016/3/22.
 */
public class ParseUtil {

    /**
     * 解析司机的任务
     * @param dataSnapshot
     * @return
     */
    public static DriveTask parseDriveTask(DataSnapshot dataSnapshot){

        DriveTask driveTask = new DriveTask();


        if (dataSnapshot != null) {

            List<Task> taskList = new ArrayList<>();

            Iterator iterator = dataSnapshot.getChildren().iterator();

            while (iterator.hasNext()) {

                DataSnapshot childSnapshot = (DataSnapshot)iterator.next();

                Task task = (Task)childSnapshot.getValue(Task.class);

                task.setKey(childSnapshot.getKey());

                taskList.add(task);
            }

            driveTask.setTasks(taskList);
        }
        return driveTask;
    }

    /**
     * 解析实际运行的线路数据
     *
     * @param dataSnapshot
     * @return
     */
    public static BusRun parseBusRun(DataSnapshot dataSnapshot) {

        BusRun busRun = new BusRun();

        busRun.setBeginTime(getValue(dataSnapshot, "beginTime"));
        busRun.setBusNumber(getValue(dataSnapshot,"busNumber"));
        busRun.setLicenseNumber(getValue(dataSnapshot, "licenseNumber"));
        busRun.setStations(parseLineStation(dataSnapshot));
        busRun.setSeatsLeft(parseSeatsLeft(dataSnapshot));

        return busRun;
    }

    /**
     * 解析某条线路的站点
     *
     * @param dataSnapshot
     * @return
     */
    private static List<Station> parseLineStation(DataSnapshot dataSnapshot) {

        int i = 1;

        List<Station> stationList = new ArrayList<>();

        while (getValueFormSnapshot(Station.class,"station" + i, dataSnapshot) != null) {

            stationList.add(getValueFormSnapshot(Station.class, "station" + i, dataSnapshot));
            i++;
        }

        return stationList;
    }


    /**
     * 解析余票列表
     *
     * @param dataSnapshot
     * @return
     */
    private static List<String> parseSeatsLeft(DataSnapshot dataSnapshot) {

        int i = 1;

        List<String> seatsLeftList = new ArrayList<>();

        while (getValueFormSnapshot(String.class, "remain" + i, dataSnapshot) != null) {

            seatsLeftList.add(getValueFormSnapshot(String.class, "remain" + i, dataSnapshot));

            i++;
        }

        return seatsLeftList;
    }


    /**
     * 解析具体的任务列表
     * @param dataSnapshot
     * @return
     */
    private static List<Task> parseConcreteTask(DataSnapshot dataSnapshot) {

        int i = 1;

        List<Task> tasks = new ArrayList<>();

        while (getValueFormSnapshot(Task.class,"task" + i, dataSnapshot) != null) {

            Task task = getValueFormSnapshot(Task.class, "task" + i, dataSnapshot);

            tasks.add(task);
            i++;
        }

        return tasks;
    }

    /**
     * 获取某个节点下的属性值
     *
     * @param tClass
     * @param key
     * @param dataSnapshot
     * @param <T>
     * @return
     */
    public static <T> T getValueFormSnapshot(Class<T> tClass, String key, DataSnapshot dataSnapshot) {

        T t = null;

        DataSnapshot stationSnapshot = dataSnapshot.child(key);

        if (stationSnapshot.getValue() != null) {

            if (tClass.getSimpleName().equals("String")) {

                t = (T) stationSnapshot.getValue();
            } else {
                t = (T)stationSnapshot.getValue(tClass);
            }
        }
        return t;
    }


    /**
     * 获取某个Key下的值
     * @param dataSnapshot
     * @param key
     * @return
     */
    private static String getValue(DataSnapshot dataSnapshot, String key) {
        return getValueFormSnapshot(String.class,key,dataSnapshot);
    }

    /**
     * 解析站点经纬度
     * @param dataSnapshot
     * @return
     */
    public static Latlng getStationLocation(DataSnapshot dataSnapshot){
        double latitude = Double.valueOf(ParseUtil.getValueFormSnapshot(String.class, "latitude", dataSnapshot));
        double longitude = Double.valueOf(ParseUtil.getValueFormSnapshot(String.class, "longitude", dataSnapshot));
        Latlng latlng = new Latlng(latitude,longitude);
        return latlng;
    }

}
