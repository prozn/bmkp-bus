package cn.bmkp.bus.app.driver.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.utils.DeviceUtil;


/**
 * Created by pandeng on 15/10/29.
 */
public class ProgressDialogFragment extends DialogFragment {

    private Dialog dialog;
    TextView tvDetail;
    String mDetail;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_progress2, null);
        tvDetail = ((TextView) view.findViewById(R.id.tv_progress_detail));

        ViewGroup.LayoutParams lp = new ViewGroup
                .LayoutParams((int) (DeviceUtil.getScreenWidth(getActivity()) * 0.8)
                , ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(view, lp);
        dialog.setCancelable(false);
        tvDetail.setText(mDetail);

        return dialog;
    }

    public ProgressDialogFragment setDetail(String detail) {
        mDetail = detail;
        return this;
    }
}
