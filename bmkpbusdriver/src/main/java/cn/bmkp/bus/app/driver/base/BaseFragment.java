package cn.bmkp.bus.app.driver.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import cn.bmkp.bus.app.driver.BMKPBusDriverApplication;
import cn.bmkp.bus.app.driver.helper.ParseHelper;
import cn.bmkp.bus.app.driver.helper.PreferenceHelper;
import cn.bmkp.bus.app.driver.restful.CallbackHandler;
import cn.bmkp.bus.app.driver.restful.ServerApi;
import cn.bmkp.bus.app.driver.utils.AndyUtils;


public class BaseFragment extends Fragment {

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public ServerApi getApi() {
        return ((BMKPBusDriverApplication) getActivity().getApplication()).getApi();
    }

//    public DBManager getDbHelper() {
//        return ((BaseActivity) getActivity()).getDbHelper();
//    }

    public ParseHelper getParseHelper() {
        return ((BaseActivity) getActivity()).getParseHelper();
    }

    public PreferenceHelper getPreferenceHelper() {
        return ((BaseActivity) getActivity()).getPreferenceHelper();
    }

    public void showException(String error) {
        if (getActivity() != null)
            AndyUtils.showToast(error, getActivity());
    }

    public void showException(Throwable throwable) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showException(throwable);
    }

    public void showProgressDialog(String detail) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showProgressDialog(detail);
        }
    }

    public void showOKCancelDialog(String title, String content,
                                   String positiveText, View.OnClickListener positiveListener) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showOKCancelDialog(title, content, positiveText, positiveListener);
    }

    public void showTwoChoiceDialog(String title, String content,
                                    String positiveText, View.OnClickListener positiveListener,
                                    String negativeText, View.OnClickListener negativeListener) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showTwoChoiceDialog(title, content, positiveText, positiveListener, negativeText, negativeListener);
    }

    public void show2BtnDialog(String title, String content,
                               String positiveText, View.OnClickListener positiveListener,
                               String negativeText, View.OnClickListener negativeListener,
                               int negativeStyle) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).show2BtnDialog(title, content, positiveText, positiveListener, negativeText, negativeListener, negativeStyle);
    }

    public void showInfoDialog(String title, String content) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showInfoDialog(title, content);
    }

    public void showInfoDialog(String title, String content, View.OnClickListener positiveListener) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showInfoDialog(title, content, positiveListener);
    }

    public void showEditDialog(String title, View.OnClickListener positiveListener) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showEditDialog(title, positiveListener);
    }

    public void dismissDialog() {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).dismissDialog();
    }

    public BaseActivity getBaseAcitivity() {
        return (BaseActivity) getActivity();
    }

    public void setImage(ImageView view, String url) {
        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(url.trim()))
            ImageLoader.getInstance().displayImage(url, view);
    }

    protected class SimpleCallbackHandler extends CallbackHandler {

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);

            if (getActivity() != null) {
                dismissDialog();
                showException(throwable);
            }
        }
    }
}
