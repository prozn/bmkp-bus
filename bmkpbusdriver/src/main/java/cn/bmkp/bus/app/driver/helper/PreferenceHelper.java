package cn.bmkp.bus.app.driver.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import cn.bmkp.bus.app.driver.constant.AndyConstants;


public class PreferenceHelper {

    private SharedPreferences app_prefs;
    private final String WALKER_LATITUDE = "latitude";
    private final String WALKER_LONGITUDE = "longitude";
    private final String WALKER_LOCATION_ADDRESS = "location_address";
    private final String WALKER_LOCATION_SPEED = "location_speed";
    private final String WALKER_LOCATION_TYPE = "location_type";
    private final String PASSWORD = "password";
    private final String NOTIFY_TOKEN = "notify_token";
    private Context context;
    private final String LOGIN_STATUS = "login_status";
    private final String USER_NAME = "user_name";
    private final String VERIFY_CODE = "user_verifyCode";
    private final String DOWN_NUM = "down_num";

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(AndyConstants.PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putNotifyToken(String token) {
        Editor edit = app_prefs.edit();
        edit.putString(NOTIFY_TOKEN, token);
        edit.commit();
    }

    public String getToken() {
        return app_prefs.getString(NOTIFY_TOKEN, null);
    }

    public void putWalkerSpeed(float speed) {
        Editor edit = app_prefs.edit();
        edit.putFloat(WALKER_LOCATION_SPEED, speed);
        edit.commit();
    }

    public float getWalkerSpeed() {
        return app_prefs.getFloat(WALKER_LOCATION_SPEED, 0);
    }

    public void putWalkerLType(int type) {
        Editor edit = app_prefs.edit();
        edit.putInt(WALKER_LOCATION_TYPE, type);
        edit.commit();
    }

    public int getWalkerLType() {
        return app_prefs.getInt(WALKER_LOCATION_TYPE, 0);
    }

    public void putWalkerLatitude(String latitude) {
        Editor edit = app_prefs.edit();
        edit.putString(WALKER_LATITUDE, latitude);
        edit.commit();
    }

    public String getWalkerLatitude() {
        return app_prefs.getString(WALKER_LATITUDE, "");
    }

    public void putWalkerLongitude(String longitude) {
        Editor edit = app_prefs.edit();
        edit.putString(WALKER_LONGITUDE, longitude);
        edit.commit();
    }

    public String getWalkerLongitude() {
        return app_prefs.getString(WALKER_LONGITUDE, "");
    }

    public void putWalkerLocationAddress(String locationAddress) {
        Editor edit = app_prefs.edit();
        edit.putString(WALKER_LOCATION_ADDRESS, locationAddress);
        edit.commit();
    }

    public String getWalkerLocationAddress() {
        return app_prefs.getString(WALKER_LOCATION_ADDRESS, "");
    }

    public void putPassword(String password) {
        Editor edit = app_prefs.edit();
        edit.putString(PASSWORD, password);
        edit.commit();
    }

    public String getPassword() {
        return app_prefs.getString(PASSWORD, null);
    }

    public void cleanUserLoginPsw(){

        putPassword("");
    }

    public void clear() {
        Editor edit = app_prefs.edit();
        edit.clear();
        edit.commit();
    }


    public void putLoginStatus(boolean status) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(LOGIN_STATUS, status);
        edit.commit();
    }

    public Boolean getLoginStatus() {
        return app_prefs.getBoolean(LOGIN_STATUS, false);
    }

    public void putToken(String token) {
        Editor edit = app_prefs.edit();
        edit.putString(NOTIFY_TOKEN, token);
        edit.commit();
    }

    public void putUserName(String name) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_NAME, name);
        edit.commit();
    }

    public void putVerifyCode(String verifyCode) {
        Editor edit = app_prefs.edit();
        edit.putString(VERIFY_CODE, verifyCode);
        edit.commit();
    }

    public String getUserPhone() {
        return app_prefs.getString(USER_NAME, null);
    }

    public void  putDownNum(String downNum){
        Editor edit = app_prefs.edit();
        edit.putString(DOWN_NUM, downNum);
        edit.commit();
    }

    public String getDownNum() {
        return app_prefs.getString(DOWN_NUM, null);
    }

    public void cleanDownNum(){
        putDownNum("");
    }
}
