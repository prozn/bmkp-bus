package cn.bmkp.bus.app.driver.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhangn on 2016/3/22.
 *
 * description:司机的行车任务
 */
public class DriveTask implements Serializable{

    private String driverName;

    private List<Task> tasks;

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
