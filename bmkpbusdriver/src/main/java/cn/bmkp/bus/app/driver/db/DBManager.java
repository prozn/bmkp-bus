package cn.bmkp.bus.app.driver.db;

import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.DaoSession;
import cn.bmkp.bus.app.driver.models.BusRun;
import cn.bmkp.bus.app.driver.models.Station;
import cn.bmkp.bus.app.driver.models.Task;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by zhangn on 2016/3/18.
 *
 * description:数据库操作
 */
public class DBManager {

    private static DBManager dbManagerTnstance;

    private DaoSession mDaoSession;

    private DBManager(){

    }

    public void init(DaoSession daoSession) {

        mDaoSession = daoSession;
    }

    public static DBManager getInstance(){
        if(dbManagerTnstance == null) {
            dbManagerTnstance = new DBManager();
        }

        return dbManagerTnstance;
    }

    public void insertBusInfo(BusInfo busInfo){

        if(mDaoSession == null) {
            return;
        }
        mDaoSession.getBusInfoDao().insert(busInfo);
    }

    public BusInfo getBusInfo() {

        if(mDaoSession == null) {
            return null;
        }

        QueryBuilder qb = mDaoSession.getBusInfoDao().queryBuilder();
        return (BusInfo) qb.unique();
    }

    public void insertBusStation(BusStation busStation) {

        if(mDaoSession == null) {
            return;
        }
        mDaoSession.getBusStationDao().insert(busStation);

    }


    public void updateBusStation(BusStation busStation) {

        if(mDaoSession == null) {
            return;
        }

        mDaoSession.getBusStationDao().update(busStation);
    }

    public void cleanAll(){
        mDaoSession.getBusInfoDao().deleteAll();
        mDaoSession.getBusStationDao().deleteAll();
    }


    /**
     * 存入行车信息和站点
     * @param busRun
     * @param task
     */
    public void insert(BusRun busRun, Task task) {


        cleanAll();

        if(busRun != null && task !=null) {

            BusInfo busInfo = new BusInfo();
            busInfo.setBusnumber(busRun.getBusNumber());
            busInfo.setBeginStation(task.getBeginStation());
            busInfo.setEndStation(task.getEndStation());
            busInfo.setBeginTime(task.getBeginTime());
            busInfo.setLine(task.getLine());
            busInfo.setDay(task.getDay());
            busInfo.setTaskKey(task.getKey());


            // insert busInfo
            insertBusInfo(busInfo);

            long busInfoId = getBusInfo().getId();

            for(Station station : busRun.getStations()) {

                BusStation busStation = new BusStation();
                busStation.setBusInfoId(busInfoId);
                busStation.setKey(station.getKey());
                busStation.setName(station.getName());
                busStation.setDetail(station.getDetail());
                busStation.setTime(station.getTime());
                busStation.setArrive(station.isArrive());
                busStation.setDownNum(station.getDownNum());

                // insert busStation
                insertBusStation(busStation);
            }
        }
    }
}
