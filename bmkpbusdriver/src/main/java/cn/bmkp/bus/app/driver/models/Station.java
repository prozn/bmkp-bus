package cn.bmkp.bus.app.driver.models;

import java.io.Serializable;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description：在途经站点
 */
public class Station implements Serializable {

    private String key;       // 节点关键字

    private String name;      // 站点名称

    private String detail;    // 站点详细地址

    private String time;      // 到站时间

    private boolean arrive;    // 是否到站

    private String downNum; //

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isArrive() {
        return arrive;
    }

    public void setArrive(boolean arrive) {
        this.arrive = arrive;
    }

    public String getDownNum() {
        return downNum;
    }

    public void setDownNum(String downNum) {
        this.downNum = downNum;
    }
}
