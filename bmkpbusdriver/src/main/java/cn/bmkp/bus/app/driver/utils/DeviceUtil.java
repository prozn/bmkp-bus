package cn.bmkp.bus.app.driver.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.regex.Pattern;

public class DeviceUtil {

	public static synchronized boolean isART() {
		boolean result = false;

		try {
			Class<?> systemProperties = Class.forName("android.os.SystemProperties");
			Method get = systemProperties.getMethod("get", String.class, String.class);
			String value = (String) get.invoke(systemProperties, "persist.sys.dalvik.vm.lib", "Dalvik");

			result = "libart.so".equals(value) || "libartd.so".equals(value);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static synchronized int getCoreNum() {
		int result = 1;

		try {
			File[] files = new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					return Pattern.matches("cpu[0-9]", pathname.getName());
				}
			});

			result = files.length;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static synchronized long getCoreMaxFreq() {
		long freq = 0;

		try {
			String[] args = {
					"/system/bin/cat",
					"/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
			};

			Process process = new ProcessBuilder(args).start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			freq = Long.parseLong(reader.readLine()) / 1000;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return freq;
	}

	public static synchronized boolean isLowProfile() {
		return getCoreNum() < 4 || getCoreMaxFreq() < 1300;
	}

	public static synchronized float getDPI(Context ctx) {
		return ctx.getResources().getDisplayMetrics().density;
	}

	public static synchronized boolean isTablet(Context ctx) {
		return (ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
				>=
				Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public static synchronized boolean isLandscape(Context ctx) {
		return ctx.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}

	public static synchronized int getScreenWidth(Context ctx) {
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager manager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		manager.getDefaultDisplay().getMetrics(dm);

		return dm.widthPixels;
	}

	public static synchronized int getScreenHeight(Context ctx) {
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager manager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		manager.getDefaultDisplay().getMetrics(dm);

		return dm.heightPixels;
	}

	public static synchronized int getNavigationBarHeight(Context ctx) {
		int result = 0;

		Resources resources = ctx.getResources();
		int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = resources.getDimensionPixelSize(resourceId);
		}

		return result;
	}

	public static synchronized long getExternalFreeSpace() {
		long result = -1;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			StatFs fs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			result = BigDecimal.valueOf(fs.getAvailableBlocks()).multiply(BigDecimal.valueOf(fs.getBlockSize())).longValue();
		}

		return result;
	}

	public static synchronized long getExternalTotalSpace() {
		long result = 0;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			StatFs fs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			result = BigDecimal.valueOf(fs.getBlockCount()).multiply(BigDecimal.valueOf(fs.getBlockSize())).longValue();
		}

		return result;
	}

	public static synchronized boolean hasSdCard() {
		return Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState());
	}

}
