package cn.bmkp.bus.app.driver.models;

import java.util.List;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description: bus的实际线路
 */
public class BusRun extends BusLine{

    private String busNumber;

    private String licenseNumber;  // 车牌号

    private List<String> seatsLeft; // 余票

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public List<String> getSeatsLeft() {
        return seatsLeft;
    }

    public void setSeatsLeft(List<String> seatsLeft) {
        this.seatsLeft = seatsLeft;
    }
}
