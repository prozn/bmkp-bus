package cn.bmkp.bus.app.driver.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description:bus线路模型
 */
public class BusLine implements Serializable {


    private String beginTime;  // 首站的发车时间

    private String endTime;    // 末站的到达时间

    private String lineKey;

    private List<Station> stations;


    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLineKey() {
        return lineKey;
    }

    public void setLineKey(String lineKey) {
        this.lineKey = lineKey;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }
}
