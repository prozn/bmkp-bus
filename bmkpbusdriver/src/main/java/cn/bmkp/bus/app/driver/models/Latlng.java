package cn.bmkp.bus.app.driver.models;

/**
 * Created by LW on 2016/3/28.
 */
public class Latlng {
    private double latitude;
    private double longitude;

    public Latlng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
