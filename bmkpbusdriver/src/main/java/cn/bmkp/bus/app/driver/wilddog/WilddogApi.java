package cn.bmkp.bus.app.driver.wilddog;

import android.content.Context;

import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangn on 2016/3/22.
 *
 * description: wilddog api
 */
public class WilddogApi {

    private static String SERVICE_URL = "https://bmkpbus.wilddogio.com/";

    private static String DRIVER = "drivers";

    private static String RUN = "run";

    private static String BUS = "bus";

    private static String STATION = "station";

    private static String TASK = "task";

    /**
     * 定位点
     */
    private static class Point{

        public static String KEY_LAT = "latitude";
        public static String KEY_LON = "longtitude";

    }

    private static WilddogApi wilddogApi;

    private Wilddog wilddog;

    private WilddogApi(){

    }

    public void init(Context context){

        if (context == null) {
            return;
        }

        Wilddog.setAndroidContext(context);
        wilddog = new Wilddog(SERVICE_URL);
    }

    public static WilddogApi getInstance(){
        if(wilddogApi == null) {
            wilddogApi = new WilddogApi();
        }

        return wilddogApi;
    }

    /**
     * wilddog 访问 Token认证
     * @param token
     * @param authResultHandler
     */
    public void authWithToken(String token, Wilddog.AuthResultHandler authResultHandler) {

        if(wilddog == null) {
            return;
        }

        wilddog.authWithCustomToken(token, authResultHandler);
    }


    /**
     * 查询司机的任务列表(只查询未完成的任务)
     * @param driverPhone
     * @param valueEventListener
     */
    public void getDriverTasks(String driverPhone, ValueEventListener valueEventListener){

        if(wilddog == null) {
            return;
        }

        wilddog.child(DRIVER).child(driverPhone).child(TASK).orderByChild("finished").equalTo(false).
                addListenerForSingleValueEvent(valueEventListener);
    }


    /**
     * 获取司机的任务详情
     * @param lineKey
     * @param date
     * @param valueEventListener
     */
    public void getDriverTaskDetail(String lineKey, String date, ValueEventListener valueEventListener){

        if(wilddog == null){
            return;
        }

        wilddog.child(RUN).child(date).child(lineKey).addListenerForSingleValueEvent(valueEventListener);
    }


    /**
     * 司机到站更新
     * @param lineKey
     * @param date
     * @param whichStation
     * @param completionListener
     */
    public void requestArrivePoint(String lineKey, String date, String whichStation, Wilddog.CompletionListener completionListener) {


        if(wilddog == null) {
            return;
        }

        Map<String, Object> map = new HashMap<>();

        map.put("arrive", true);

        wilddog.child(RUN).child(date).child(lineKey).child("station" + whichStation).updateChildren(map, completionListener);
    }

    /**
     * 上报车的位置
     * @param busNumber
     * @param latitude
     * @param longtitude
     * @param completionListener
     */
    public void reportBusLocation(String busNumber, String latitude, String longtitude, Wilddog.CompletionListener completionListener){

        if ( wilddog == null) {
            return ;
        }

        Map<String, Object> map = new HashMap<>();

        map.put(Point.KEY_LAT, latitude);
        map.put(Point.KEY_LON, longtitude);

        wilddog.child(BUS).child("bus"+busNumber).updateChildren(map, completionListener);
    }

    /**
     * 获取当前站点的经纬度
     * @param stationName
     * @param valueEventListener
     */
    public void getStationLocation(String stationName,ValueEventListener valueEventListener) {
        if (wilddog == null) {
            return;
        }
        wilddog.child(STATION).child(stationName).addValueEventListener(valueEventListener);
    }

    /**
     * 司机完成某个任务
     * @param phone
     * @param taskKey
     * @param completionListener
     */
    public void finishTask(String phone, String taskKey, Wilddog.CompletionListener completionListener) {

        if(wilddog == null){
            return;
        }

        Map<String, Object> map = new HashMap<>();

        map.put("finished",true);

        wilddog.child(DRIVER).child(phone).child(TASK).child(taskKey).updateChildren(map,completionListener);

    }

}
