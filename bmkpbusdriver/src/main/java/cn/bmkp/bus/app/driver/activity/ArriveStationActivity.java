package cn.bmkp.bus.app.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.driver.BusInfo;
import cn.bmkp.bus.app.driver.BusStation;
import cn.bmkp.bus.app.driver.R;
import cn.bmkp.bus.app.driver.base.BaseActivity;
import cn.bmkp.bus.app.driver.db.DBManager;
import cn.bmkp.bus.app.driver.utils.ActivityStack;
import cn.bmkp.bus.app.driver.wilddog.WilddogApi;


/**
 * description:输入下车人数
 */
public class ArriveStationActivity extends BaseActivity {


    @Bind(R.id.tv_current_station_start_name)
    TextView tvCurrentStationStartName;
    @Bind(R.id.tv_current_station_end_name)
    TextView tvCurrentStationEndName;
    @Bind(R.id.tv_arrive_station_current_station)
    TextView tvArriveStationCurrentStation;
    @Bind(R.id.tv_current_station_leave_people_num)
    TextView tvCurrentStationLeavePeopleNum;
    @Bind(R.id.et_current_station_leave_real_num)
    EditText etCurrentStationLeaveRealNum;

    private int position;
    private int downNum;

    private BusInfo mBusInfo;
    BusStation busStation;

    @OnClick(R.id.ib_function_left)
    public void back(View view) {
        this.finish();
    }

    @OnClick(R.id.btn_current_confirm)
    public void confirm(View v) {
        if (!TextUtils.isEmpty(etCurrentStationLeaveRealNum.getText())) {

            //应下车人数和实际下车人数的差值
            try {
                //计算当前数据库上保存的应下车人数和上一站下车人数的差值的和
                int showDownNum = Integer.parseInt(getPreferenceHelper().getDownNum())+Integer.parseInt(busStation.getDownNum());
                downNum = showDownNum-Integer.valueOf(etCurrentStationLeaveRealNum.getText().toString());
            }catch (Exception e){
                //如果是第一站，则没有差值
                downNum = Integer.parseInt(busStation.getDownNum())-Integer.valueOf(etCurrentStationLeaveRealNum.getText().toString());
            }
            //保存应下车人数和实际下车人数的差值
            getPreferenceHelper().putDownNum(String.valueOf(downNum));

            boolean isEnd = Integer.parseInt(getCurrentStation().getKey())==mBusInfo.getStations().size();

            if (isEnd) {//到达最后一站，前往结束任务界面

                // 到达最后一站，更新状态
                finishTask();
                getPreferenceHelper().cleanDownNum();//清空PreferenceHelper存储的下车人数
                Intent intent = new Intent(ArriveStationActivity.this, FinishTaskActivity.class);
                startActivity(intent);
                ActivityStack.getInstance().finishActivity(TaskStationListActivity.class);
                ArriveStationActivity.this.finish();
            } else {//不是最后一站，前往下一站发车界面
                requestArrivePoint();
            }

        }else{
            Toast.makeText(ArriveStationActivity.this, R.string.text_arrive_input_real_down_num,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_arrive_station);
        ButterKnife.bind(this);

        setData();

    }

    private void setData() {

//        position = Integer.valueOf(getIntent().getStringExtra("position"));

        mBusInfo = DBManager.getInstance().getBusInfo();
        tvCurrentStationStartName.setText(mBusInfo.getBeginStation());
        tvCurrentStationEndName.setText(mBusInfo.getEndStation());

        busStation = getCurrentStation();
        tvArriveStationCurrentStation.setText(busStation.getName());

        try {
            int downNum = Integer.parseInt(getPreferenceHelper().getDownNum())+Integer.parseInt(busStation.getDownNum());
            tvCurrentStationLeavePeopleNum.setText(String.valueOf(downNum));
        }catch (Exception e){
            tvCurrentStationLeavePeopleNum.setText(busStation.getDownNum());
        }

    }


    /**
     * 获取当前是哪一站
     */
    private BusStation getCurrentStation(){

        List<BusStation> stations = DBManager.getInstance().getBusInfo().getStations();

        for(BusStation station : stations) {

            if(!station.getArrive()) {
                return  station;
            }
        }

       return null;
    }


    /**
     * 完成任务，更新状态
     */
    private void finishTask(){

        final BusInfo busInfo = DBManager.getInstance().getBusInfo();

        BusStation busStation = getCurrentStation();

        busStation.setArrive(true);

        updateLocalDB(busStation);

        WilddogApi.getInstance().requestArrivePoint(mBusInfo.getLine(), mBusInfo.getDay(), busStation.getKey(), new Wilddog.CompletionListener() {
            @Override
            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {

                WilddogApi.getInstance().finishTask(getPreferenceHelper().getUserPhone(), busInfo.getTaskKey(), new Wilddog.CompletionListener() {
                    @Override
                    public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                        if (wilddogError != null) {
                            Toast.makeText(ArriveStationActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });


    }

    /**
     * 确认到站
     */
    private void requestArrivePoint(){

        BusStation busStation = getCurrentStation();

        busStation.setArrive(true);

        updateLocalDB(busStation);

        WilddogApi.getInstance().requestArrivePoint(mBusInfo.getLine(), mBusInfo.getDay(), busStation.getKey(), new Wilddog.CompletionListener() {
            @Override
            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {

                Intent intent = new Intent(ArriveStationActivity.this, StationStartActivity.class);
                ActivityStack.getInstance().finishActivity(TaskStationListActivity.class);
                ArriveStationActivity.this.finish();
                startActivity(intent);
            }
        });
    }

    private void updateLocalDB(BusStation busStation){

        DBManager.getInstance().updateBusStation(busStation);
    }

}
