package cn.bmkp.bus.app.driver;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "BUS_STATION".
 */
public class BusStation {

    private Long id;
    private String key;
    private String name;
    private String detail;
    private String time;
    private Boolean arrive;
    private String downNum;
    private Long busInfoId;

    public BusStation() {
    }

    public BusStation(Long id) {
        this.id = id;
    }

    public BusStation(Long id, String key, String name, String detail, String time, Boolean arrive, String downNum, Long busInfoId) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.detail = detail;
        this.time = time;
        this.arrive = arrive;
        this.downNum = downNum;
        this.busInfoId = busInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getArrive() {
        return arrive;
    }

    public void setArrive(Boolean arrive) {
        this.arrive = arrive;
    }

    public String getDownNum() {
        return downNum;
    }

    public void setDownNum(String downNum) {
        this.downNum = downNum;
    }

    public Long getBusInfoId() {
        return busInfoId;
    }

    public void setBusInfoId(Long busInfoId) {
        this.busInfoId = busInfoId;
    }

}
