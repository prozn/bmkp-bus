package cn.bmkpbus.dao.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;


/**
 * author:zhangn
 *
 * description: GreenDao 用于构建bus乘客端数据库
 */
public class BmkpBusDaoGenerator {


    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1, "cn.bmkp.bus.app");

        addUser(schema);

        new DaoGenerator().generateAll(schema, "BMKPBus/src-gen");
    }

        private static void addUser(Schema schema) {

        Entity user = schema.addEntity("User");
        user.addIdProperty();
        user.addStringProperty("name").notNull();
    }

}
