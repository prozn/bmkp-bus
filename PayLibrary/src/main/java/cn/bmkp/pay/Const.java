package cn.bmkp.pay;

/**
 * Created by fm on 16/3/4.
 */
public class Const {

    public class Payment{
        public final static int PAY_SUCCESS_ERROR = 1;   //支付成功但是其他问题（查看errorMsg）
        public final static int PAY_SUCCESS = 0;         //支付成功
        public final static int PAY_FAIL = -1;           //支付失败
    }

}
