package cn.bmkp.pay;

/**
 * Created by fm on 16/2/29.
 */
public class AutoPay {

    /* ---------  支付宝支付数据 START ----------- */
    //支付宝支付商户PID
    private static String aliPID;
    //支付宝支付商户收款账号
    private static String aliSeller;
    //支付宝支付商户私钥，pkcs8格式
    private static String aliRsaPrivate;
    //支付宝支付公钥
    private static String aliRsaPublic;
    //支付宝支付服务端接口
    private static String aliNotifyUrl;

    public static String getAliPID() {
        return aliPID;
    }

    public static void setAliPID(String aliPID) {
        AutoPay.aliPID = aliPID;
    }

    public static String getAliSeller() {
        return aliSeller;
    }

    public static void setAliSeller(String aliSeller) {
        AutoPay.aliSeller = aliSeller;
    }

    public static String getAliRsaPrivate() {
        return aliRsaPrivate;
    }

    public static void setAliRsaPrivate(String aliRsaPrivate) {
        AutoPay.aliRsaPrivate = aliRsaPrivate;
    }

    public static String getAliRsaPublic() {
        return aliRsaPublic;
    }

    public static void setAliRsaPublic(String aliRsaPublic) {
        AutoPay.aliRsaPublic = aliRsaPublic;
    }

    public static String getAliNotifyUrl() {
        return aliNotifyUrl;
    }

    public static void setAliNotifyUrl(String aliNotifyUrl) {
        AutoPay.aliNotifyUrl = aliNotifyUrl;
    }

    /* ---------  支付宝支付数据 END ----------- */

    /* ---------  微信支付数据 START ----------- */

    //微信支付appId
    private static String wxAppID;
    //微信支付MCHID
    private static String wxMchId;
    //微信支付的APIKEY
    private static String wxApiKey;
    //微信支付接口
    private static String wxNotifyUrl;

    public static String getWxAppID() {
        return wxAppID;
    }

    public static void setWxAppID(String wxAppID) {
        AutoPay.wxAppID = wxAppID;
    }

    public static String getWxMchId() {
        return wxMchId;
    }

    public static void setWxMchId(String wxMchId) {
        AutoPay.wxMchId = wxMchId;
    }

    public static String getWxApiKey() {
        return wxApiKey;
    }

    public static void setWxApiKey(String wxApiKey) {
        AutoPay.wxApiKey = wxApiKey;
    }

    public static String getWxNotifyUrl() {
        return wxNotifyUrl;
    }

    public static void setWxNotifyUrl(String wxNotifyUrl) {
        AutoPay.wxNotifyUrl = wxNotifyUrl;
    }

    /* ---------  微信支付数据 START ----------- */

}
