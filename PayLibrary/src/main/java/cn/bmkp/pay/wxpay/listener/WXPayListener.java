package cn.bmkp.pay.wxpay.listener;

/**
 * Created by fm on 2016/2/26.
 */
public interface WXPayListener {

    void payCallBack(int errorCode, String errorMsg);

}
