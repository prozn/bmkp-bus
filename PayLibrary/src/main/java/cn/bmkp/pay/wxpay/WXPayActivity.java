package cn.bmkp.pay.wxpay;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Xml;

import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cn.bmkp.pay.AutoPay;
import cn.bmkp.pay.Const;
import cn.bmkp.pay.wxpay.listener.WXPayListener;
import cn.bmkp.pay.wxpay.utils.MD5;
import cn.bmkp.pay.wxpay.utils.WXUtils;

/**
 * Created by fm on 2016/2/28.
 */
public class WXPayActivity extends Activity {

    private PayReq req;
    private IWXAPI msgApi = null;
    private Map<String, String> resultunifiedorder;
    private StringBuffer sb;
    private UIHandler handler = null;
    private final static int PAY_FAIL = 1;
    private final static int PAY_GEN = 2;
    private final static int PAY_REQ = 3;
    WXPayListener listener;

    private String APP_ID = AutoPay.getWxAppID();
    private String MCH_ID = AutoPay.getWxMchId();
    private String API_KEY = AutoPay.getWxApiKey();
    private String NOTIFY_URL = AutoPay.getWxNotifyUrl();

    private String orderId;
    private String fee;
    private String title;

    public IWXAPI getMsgApi() {
        return msgApi;
    }

    public void setMsgApi(IWXAPI msgApi) {
        this.msgApi = msgApi;
    }

    public void setPayListener(WXPayListener listener) {
        this.listener = listener;
    }

    /**
     * call wx sdk pay. 调用SDK支付
     * @param title 商品名称
     * @param orderId  支付订单编号（可由自己生成）
     * @param fee 支付费用
     * */
    public void pay(String title, String orderId, String fee) {
        if (!getMsgApi().isWXAppInstalled()) {
            listener.payCallBack(Const.Payment.PAY_FAIL, "微信没有安装");
            return;
        }
        if (!getMsgApi().isWXAppSupportAPI()) {
            listener.payCallBack(Const.Payment.PAY_FAIL, "微信版本太低不能支付");
            return;
        }
        this.title = title;
        this.orderId = orderId;
        this.fee = fee;
        GetPrepayIdTask task = new GetPrepayIdTask();
        task.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        req = new PayReq();
        sb = new StringBuffer();
        handler = new UIHandler();
    }

    public Map<String, String> decodeXml(String content) {

        try {
            Map<String, String> xml = new HashMap<String, String>();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {

                String nodeName = parser.getName();
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if ("xml".equals(nodeName) == false) {
                            //实例化student对象
                            xml.put(nodeName, parser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                event = parser.next();
            }
            return xml;
        } catch (Exception e) {
            Log.e("orion", e.toString());
        }
        return null;

    }

    private String genProductArgs() {
        StringBuffer xml = new StringBuffer();
        try {
            String nonceStr = genNonceStr();
            xml.append("</xml>");
            List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
            packageParams.add(new BasicNameValuePair("appid", APP_ID));
            packageParams.add(new BasicNameValuePair("body", title));
            packageParams.add(new BasicNameValuePair("mch_id", MCH_ID));
            packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));
            packageParams.add(new BasicNameValuePair("notify_url", NOTIFY_URL));
            packageParams.add(new BasicNameValuePair("out_trade_no", genOutTradNo()));
            packageParams.add(new BasicNameValuePair("spbill_create_ip", "127.0.0.1"));
            packageParams.add(new BasicNameValuePair("total_fee", fee));
            packageParams.add(new BasicNameValuePair("trade_type", "APP"));
            String sign = genPackageSign(packageParams);
            packageParams.add(new BasicNameValuePair("sign", sign));
            String xmlString = toXml(packageParams);
            xmlString = new String(xmlString.getBytes("UTF-8"), "ISO-8859-1");
            return xmlString;
        } catch (Exception e) {
            return null;
        }
    }

    //生成包签名
    private String genPackageSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(API_KEY);
        String packageSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        Log.e("orion", packageSign);
        return packageSign;
    }

    //生成发送请求预支付订单数据
    private String toXml(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        sb.append("<xml>");
        for (int i = 0; i < params.size(); i++) {
            sb.append("<" + params.get(i).getName() + ">");


            sb.append(params.get(i).getValue());
            sb.append("</" + params.get(i).getName() + ">");
        }
        sb.append("</xml>");

        Log.e("orion", sb.toString());
        return sb.toString();
    }

    //生成随机字符串
    private String genNonceStr() {
        Random random = new Random();
        return MD5.getMessageDigest(String.valueOf(random.nextInt(10000)).getBytes());
    }

    //生成商户订单号
    private String genOutTradNo() {
//        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
//                Locale.getDefault());
//        Date date = new Date();
//        String tradeNo = format.format(date);
//        tradeNo = tradeNo + "-" + String.valueOf(orderId);
        return orderId;
    }

    private class GetPrepayIdTask extends AsyncTask<Void, Void, Map<String, String>> {
        @Override
        protected void onPreExecute() {}

        @Override
        protected void onPostExecute(Map<String, String> result) {
            sb.append("prepay_id\n" + result.get("prepay_id") + "\n\n");
            resultunifiedorder = result;
            if (result.get("return_code").equals("SUCCESS")) {
                //生成预支付订单去支付
                handler.sendEmptyMessage(PAY_GEN);
            } else {
                //生成预支付订单失败
                handler.sendEmptyMessage(PAY_FAIL);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Map<String, String> doInBackground(Void... params) {
            String url = String.format("https://api.mch.weixin.qq.com/pay/unifiedorder");
            String entity = genProductArgs();
            byte[] buf = WXUtils.httpPost(url, entity);
            String content = new String(buf);
            Map<String, String> xml = decodeXml(content);
            return xml;
        }
    }

    //生成签名
    private String genAppSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).getName());
            sb.append('=');
            sb.append(params.get(i).getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(API_KEY);

        this.sb.append("sign str\n" + sb.toString() + "\n\n");
        String appSign = MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        return appSign;
    }

    //生成时间戳
    private long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    //生成支付请求
    private void genPayReq() {
        req.appId = APP_ID;
        req.partnerId = MCH_ID;
        req.prepayId = resultunifiedorder.get("prepay_id");
        req.packageValue = "Sign=WXPay";
        req.nonceStr = genNonceStr();
        req.timeStamp = String.valueOf(genTimeStamp());
        List<NameValuePair> signParams = new LinkedList<NameValuePair>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));
        req.sign = genAppSign(signParams);
        sb.append("sign\n" + req.sign + "\n\n");
        handler.sendEmptyMessage(PAY_REQ);
    }

    //支付请求
    private void sendPayReq() {
        getMsgApi().sendReq(req);
    }

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PAY_FAIL:
                    listener.payCallBack(Const.Payment.PAY_FAIL, resultunifiedorder.get("return_msg"));
                    break;
                case PAY_GEN:
                    genPayReq();
                    break;
                case PAY_REQ:
                    sendPayReq();
                    break;
                default:
                    break;
            }

        }
    }

}
