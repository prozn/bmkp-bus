package cn.bmkp.pay.alipay.listener;

/**
 * Created by fm on 2016/2/26.
 */
public interface ALiPayListener {

    void payCallBack(int errorCode,String errorMsg);

}
