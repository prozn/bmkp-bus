package cn.bmkp.pay.alipay;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cn.bmkp.pay.AutoPay;
import cn.bmkp.pay.Const;
import cn.bmkp.pay.alipay.listener.ALiPayListener;
import cn.bmkp.pay.alipay.utils.PayResult;
import cn.bmkp.pay.alipay.utils.SignUtils;

/**
 * Created by fm on 2016/2/26.
 */
public class ALiPay {

    //商户PID
    public static final String PARTNER = AutoPay.getAliPID();
    //商户收款账号
    public static final String SELLER = AutoPay.getAliSeller();
    //商户私钥，pkcs8格式
    public static final String RSA_PRIVATE = AutoPay.getAliRsaPrivate();
    //支付宝公钥
    public static final String RSA_PUBLIC = AutoPay.getAliRsaPublic();
    private static final int SDK_PAY_FLAG = 1;

    private ALiPayListener mListener;
    private Activity activity;

    public void initALiPay(Activity activity, ALiPayListener listener) {
        this.activity = activity;
        mListener = listener;
    }

    /**
     * call alipay sdk pay. 调用SDK支付
     * @param title 商品名称
     * @param feeType 费用类型
     * @param orderId  支付订单编号（可由自己生成）
     * @param fee 支付费用
     */
    public void pay(String title, String feeType, String orderId, String fee) {
        // 订单
        String orderInfo = getOrderInfo(title, feeType, orderId, fee);

        // 对订单做RSA 签名
        String sign = sign(orderInfo);
        try {
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 完整的符合支付宝参数规范的订单信息
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
                + getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(activity);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * create the order info. 创建订单信息
     */
    public String getOrderInfo(String subject, String body, String orderId, String price) {
        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";
        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";
        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + orderId + "\"";
        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";
        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";
        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";
        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + AutoPay.getAliNotifyUrl() + "\"";
        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";
        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";
        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";
        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";
        // extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
        // orderInfo += "&extern_token=" + "\"" + extern_token + "\"";
        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";
        // 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
        // orderInfo += "&paymethod=\"expressGateway\"";
        return orderInfo;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    public String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    public boolean verify(String content, String sign) {
        return SignUtils.verify(content, sign, RSA_PUBLIC);
    }


    /**
     * get the sign type we use. 获取签名方式
     */
    public String getSignType() {
        return "sign_type=\"RSA\"";
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);

                    String resultInfo = payResult.getResult();
                    String resultStatus = payResult.getResultStatus();

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
//                        UserLocation.getInstance().setPayStatus(2);
                        mListener.payCallBack(Const.Payment.PAY_SUCCESS, null);
                        // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                        String resultOrderInfo = resultInfo.substring(0, resultInfo.indexOf("&sign_type="));

                        if (TextUtils.isEmpty(resultInfo)) {
                            //"支付成功 [返回商品信息为空]",
                            mListener.payCallBack(Const.Payment.PAY_SUCCESS_ERROR, "支付成功 [返回商品信息为空]");
                            return;
                        }
                        String resultSign = resultInfo.substring(resultInfo.indexOf("&sign=\"") + 7, resultInfo.lastIndexOf("\""));
                        if (!verify(resultOrderInfo, resultSign)) {
                            //"支付成功 [验签失败]",
                            mListener.payCallBack(Const.Payment.PAY_SUCCESS_ERROR, "支付成功 [验签失败]");
                            return;
                        }
                        //"支付成功",
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            //"支付结果确认中",
                            mListener.payCallBack(Const.Payment.PAY_FAIL, "支付结果确认中");
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            mListener.payCallBack(Const.Payment.PAY_FAIL, resultInfo);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };

}
