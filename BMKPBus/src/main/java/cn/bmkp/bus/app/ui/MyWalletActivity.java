package cn.bmkp.bus.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.WilddogError;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.wilddog.WilddogApi;

public class MyWalletActivity extends BaseActivity {

    @Bind(R.id.tv_my_money)
    TextView tvMyMoney;
    @Bind(R.id.rl_wallet)
    LinearLayout rlWallet;
    @Bind(R.id.tv_wallet_use)
    TextView tvWalletUse;

    @OnClick(R.id.ib_function_left)
    public void ibBack(View view) {
        this.finish();
    }

    @OnClick(R.id.tv_wallet_use)
    public void walletUse(View view) {
        startActivity(new Intent(MyWalletActivity.this, MainActivity.class));
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_wallet);
        ButterKnife.bind(this);

        rlWallet.getBackground().setAlpha(204);

        if (AndyUtils.isNetworkAvailable(this)) {
            showProgressDialog(getString(R.string.text_is_loading));
        } else {
            showNetworkDialog();
        }

        WilddogApi.getInstance().getOwnerInfo(getPreferenceHelper().getUserName(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dismissDialog();
                tvMyMoney.setText("￥" + ParseUtil.parseOwnerBalance(dataSnapshot));
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                dismissDialog();
            }
        });
    }

}
