package cn.bmkp.bus.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.adapter.BusLineStationAdapter;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.BusLine;
import cn.bmkp.bus.app.wxapi.WXPayEntryActivity;

/**
 * Created by fm on 16/3/19.
 */
public class BusLineIntroActivity extends BaseActivity {

    @Bind(R.id.tv_bus_line_start_name)
    TextView tvBusLineStartName;
    @Bind(R.id.tv_bus_line_end_name)
    TextView tvBusLineEndName;
    private BusLine busLine;

    @Bind(R.id.tv_depart_time)
    TextView tvDepartTime;


    @Bind(R.id.tv_bus_line_start_detail)
    TextView tvBusLineStartDetail;

    @Bind(R.id.tv_bus_line_end_detail)
    TextView tvBusLineEndDetail;


    @Bind(R.id.rl_bus_line_info)
    RelativeLayout rlBusLineInfo;

    @Bind(R.id.lv_bus_stations)
    ListView lvBusStations;


    private String lineKey;

    @OnClick(R.id.tv_buy_ticket)
    void buyTicket(View view) {

        Bundle bundle = new Bundle();
        bundle.putSerializable("busLineObj", busLine);
        if(getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(this, WXPayEntryActivity.class).putExtras(bundle);
            intent.putExtra(Const.BUS_LINE_SELECTED_DATE, getIntent().getStringExtra(Const.BUS_LINE_SELECTED_DATE));
            startActivity(intent);
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
//            intent.putExtras(bundle);
//            intent.putExtra("from",BusLineIntroActivity.class.getSimpleName());
            startActivity(intent);
        }
    }

    @OnClick(R.id.ib_function_left)
    void back(View view) {
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_line_intro);
        ButterKnife.bind(this);

        initView();
        initData();

        rlBusLineInfo.getBackground().setAlpha(204);
        lvBusStations.getBackground().setAlpha(100);

    }

    private void initView() {
        lvBusStations.setDividerHeight(0);
    }

    private void initData() {

        busLine = JSON.parseObject(getIntent().getStringExtra("busline"), BusLine.class);

        lineKey = busLine.getLineKey();

        tvBusLineStartName.setText(getString(R.string.text_line)+busLine.getStations().get(0).getName());
        tvBusLineEndName.setText(busLine.getStations().get(busLine.getStations().size()-1).getName());
        tvBusLineStartDetail.setText(busLine.getStations().get(0).getDetail());
        tvBusLineEndDetail.setText(busLine.getStations().get(busLine.getStations().size() - 1).getDetail());
        tvDepartTime.setText(busLine.getBeginTime() + getString(R.string.text_start));

        BusLineStationAdapter adapter = new BusLineStationAdapter(this, busLine.getStations());
        lvBusStations.setAdapter(adapter);
    }
}
