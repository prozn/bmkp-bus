package cn.bmkp.bus.app.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description: bus的实际线路
 */
public class BusRun extends BusLine implements Serializable {

    private String busNumber;

    private String licenseNumber;  // 车牌号

    private String driverName;

    private List<String> seatsLeft; // 余票

    private List<StationPrice> stationPrices;

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public List<String> getSeatsLeft() {
        return seatsLeft;
    }

    public void setSeatsLeft(List<String> seatsLeft) {
        this.seatsLeft = seatsLeft;
    }

    @Override
    public List<StationPrice> getStationPrices() {
        return stationPrices;
    }

    @Override
    public void setStationPrices(List<StationPrice> stationPrices) {
        this.stationPrices = stationPrices;
    }
}
