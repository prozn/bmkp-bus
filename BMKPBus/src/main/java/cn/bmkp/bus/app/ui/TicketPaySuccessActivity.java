package cn.bmkp.bus.app.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.Ticket;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.widget.CustomToast;
import cn.bmkp.bus.app.widget.SliderRelativeLayout;
import cn.bmkp.bus.app.wilddog.WilddogApi;
import cn.bmkp.bus.app.wxapi.WXPayEntryActivity;

/**
 * Created by chenliang on 2016/3/23.
 */
public class TicketPaySuccessActivity extends BaseActivity {

    @Bind(R.id.sliderLayout)
    SliderRelativeLayout sliderRelativeLayout;
    private String phone = "4000833359";
    private Ticket ticket;

    GestureLibrary mLibrary;
    GestureOverlayView gestures;

    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.rl_title)
    RelativeLayout rlTitle;
    @Bind(R.id.tv_start_station)
    TextView tvStartStation;
    @Bind(R.id.tv_end_station)
    TextView tvEndStation2;
    @Bind(R.id.tv_riding_time)
    TextView tvRidingTime;
    @Bind(R.id.tv_ticket_count)
    TextView tvTicketCount;
    @Bind(R.id.tv_car_number)
    TextView tvCarNumber;
    @Bind(R.id.tv_license_number)
    TextView tvLicenseNumber;
    @Bind(R.id.tv_notification_detail2)
    TextView tvNotificationDetail2;

    @Bind(R.id.rl_pay_success)
    RelativeLayout rlPaySuccess;

    @Bind(R.id.rl_car_location)
    RelativeLayout rlCarLocation;

    @Bind(R.id.btn_ticket_info_car_cancel)
    TextView tvCarCancel;

    @Bind(R.id.iv_title_my_ticket)
    ImageView ivMyTicket;

    @Bind(R.id.ll_ticket_end)
    LinearLayout llTicketEnd;

    @Bind(R.id.iv_checked)
    ImageView ivChecked;

    @Bind(R.id.ll_content)
    LinearLayout llContent;

    @Bind(R.id.tv_check_remain)
    TextView tvCheckRemain;

    public static int MSG_OK = 1;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        goToMainActivity();

    }

    //查看已购买车票
    @OnClick(R.id.iv_title_my_ticket)
    void checkMyTickets(View view) {
        if (getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(TicketPaySuccessActivity.this, MyTicketsActivity.class);
            startActivity(intent);
        } else {
            showAlertDialog(getString(R.string.text_not_login_please_login), "", getString(R.string.search_confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(TicketPaySuccessActivity.this, LoginActivity.class);
                    intent.putExtra("from", MainActivity.class.getSimpleName());
                    startActivity(intent);
                }
            }, getString(R.string.text_cancel), null, this);
        }
    }

    //退票
    @OnClick(R.id.btn_ticket_info_car_cancel)
    public void refundTicket(View view) {

        if (ticket == null) {
            Toast.makeText(TicketPaySuccessActivity.this, "获取车票信息失败", Toast.LENGTH_SHORT).show();
            return;
        }

        String sBegin = ticket.getStations().get(0).getTime();
        int hour = Integer.parseInt(sBegin.substring(0, sBegin.indexOf(":")));
        int minute = Integer.parseInt(sBegin.substring(sBegin.indexOf(":") + 1, sBegin.length()));

        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);

        Calendar beginTime = Calendar.getInstance();
        try {
            Date date = df.parse(ticket.getDay());
            beginTime.setTime(date);
        } catch (ParseException e) {
            Toast.makeText(this, "日期格式错误", Toast.LENGTH_SHORT).show();
        }
        beginTime.set(Calendar.HOUR_OF_DAY, hour);
        beginTime.set(Calendar.MINUTE, minute);
        Calendar nowTime = Calendar.getInstance();

        long time = beginTime.getTimeInMillis() - nowTime.getTimeInMillis();
        if (time < 0) {
            showAlertDialog("", "已发车", "确定", null, "取消", null, TicketPaySuccessActivity.this);

//            btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);
//            btnTicketInfoCarCheck.setText("已发车");
//            btnTicketInfoCarCheck.setEnabled(false);

            return;

        } else if (time < Const.TIME_STOP_REFUND_TICKET) {

            showAlertDialog("", "车辆始发前30分钟内停止退票", "确定", null, "取消", null, TicketPaySuccessActivity.this);

//            btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);

            return;
        }

        showAlertDialog("确定退票？", "退票请咨询" + phone, "确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phone));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
//                        startActivity(callIntent);
                        Toast.makeText(TicketPaySuccessActivity.this, "没有允许电话权限", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                startActivity(callIntent);

                // 改为由客服完成
//                WilddogApi.getIntance().getRefundForTicket(getPreferenceHelper().getUserName(), ticket.getTicketKey(),
//                        new Wilddog.CompletionListener() {
//                            @Override
//                            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
//                                Toast.makeText(TicketInfoActivity.this, "请耐心等待", Toast.LENGTH_SHORT).show();
//                                btnTicketInfoCarCheck.setEnabled(false);
//                                btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);
//                            }
//                        });

            }
        }, "取消", null, TicketPaySuccessActivity.this);
    }


    //查看车辆位置
    @OnClick(R.id.btn_ticket_info_car_location)
    public void markerDriver(View view) {
        Intent intent = new Intent(TicketPaySuccessActivity.this, CarLocationActivity.class);
        intent.putExtra("busNumber", ticket.getBusNumber());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_buy_success);
        ButterKnife.bind(this);

        sliderRelativeLayout.setMainHandler(handler);

//        initGesture();

        String ticketKey = getIntent().getStringExtra("ticketKey");
        String from = getIntent().getStringExtra("from");

        if (from != null && from.equals(WXPayEntryActivity.class.getSimpleName())) {
            rlCarLocation.setVisibility(View.GONE);
            tvCarCancel.setVisibility(View.GONE);
//            sliderRelativeLayout.setVisibility(View.GONE);
        } else {
            tvTitle.setText("车票详情");
            rlPaySuccess.setVisibility(View.GONE);
            ivMyTicket.setVisibility(View.GONE);
        }

        if (!AndyUtils.isNetworkAvailable(this)) {
            showNetworkDialog();
        }

        if (ticketKey != null) {
            WilddogApi.getInstance().setTicketListener(getPreferenceHelper().getUserName(), ticketKey, new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ticket = ParseUtil.parseUserTicket(dataSnapshot);
                    initView(ticket);
                }

                @Override
                public void onCancelled(WilddogError wilddogError) {
                    Toast.makeText(TicketPaySuccessActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            WilddogApi.getInstance().getTicketLimitToLast(getPreferenceHelper().getUserName(), 1, new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<Ticket> tickets = ParseUtil.parseUserTickets(dataSnapshot);

                    if (tickets.size() == 1)
                        ticket = tickets.get(0);
                    initView(ticket);

                }

                @Override
                public void onCancelled(WilddogError wilddogError) {

                    Toast.makeText(TicketPaySuccessActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (MSG_OK == msg.what) {
                WilddogApi.getInstance().checkTicket(getPreferenceHelper().getUserName(), ticket.getTicketKey(),
                        new Wilddog.CompletionListener() {
                            @Override
                            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                                if (wilddogError == null) {
                                    Toast.makeText(TicketPaySuccessActivity.this, getString(R.string.text_check_ticket_success), Toast.LENGTH_SHORT).show();
                                    rotateAnimation(llTicketEnd);
                                    llTicketEnd.setVisibility(View.INVISIBLE);
                                    tvCarCancel.setVisibility(View.GONE);
                                    tvCheckRemain.setVisibility(View.GONE);
                                    sliderRelativeLayout.setVisibility(View.GONE);
                                    scaleAnimation(ivChecked);
                                    ivChecked.setVisibility(View.VISIBLE);
                                    tvTitle.setText(getString(R.string.text_checked));
                                    llContent.setBackground(getResources().getDrawable(R.drawable.bg_ticket_checked));
                                }else{
                                    CustomToast.showToast(TicketPaySuccessActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT);
                                }
                            }
                        });
            }
        }
    };

    void initView(Ticket ticket) {

        String ridingTime;
        //班次时间
        try {
            SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);
            Date date = df.parse(ticket.getDay());
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);

            ridingTime = String.valueOf(ca.get(Calendar.MONTH) + 1) + "月"
                    + String.valueOf(ca.get(Calendar.DATE)) + "日"
                    + ticket.getStations().get(0).getTime();

        } catch (ParseException e) {
            ridingTime = ticket.getDay() + ticket.getStations().get(0).getTime();
        }

        tvStartStation.setText(ticket.getStations().get(0).getName());
        tvEndStation2.setText(ticket.getStations().get(ticket.getStations().size() - 1).getName());
        tvRidingTime.setText(ridingTime);
        tvTicketCount.setText(ticket.getTicketNum() + "人票");
        tvCarNumber.setText("NO." + ticket.getBusNumber());
        tvLicenseNumber.setText(ticket.getLicenseNumber());
        tvNotificationDetail2.setText(ticket.getStations().get(0).getDetail());

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            goToMainActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void goToMainActivity() {
        String from = getIntent().getStringExtra("from");

        if (from != null && from.equals(WXPayEntryActivity.class.getSimpleName())) {
            startActivity(new Intent(TicketPaySuccessActivity.this, MainActivity.class));
        }

        this.finish();
    }

    /**
     * 旋转动画
     */
    private void rotateAnimation(View view) {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_PARENT, 0.5f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(rotateAnimation);
        animationSet.setDuration(1 * 1000);

        //animationSet.setRepeatCount(int repeatCount);//设置重复次数
        //animationSet.setFillAfter(boolean);//动画执行完后是否停留在执行完的状态
        //animationSet.setStartOffset(long startOffset);//执行前的等待时间
        view.startAnimation(animationSet);
    }

    private void scaleAnimation(View view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(3.0f, 1.0f, 3.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setDuration(1 * 1000);
        view.startAnimation(animationSet);
    }

}
