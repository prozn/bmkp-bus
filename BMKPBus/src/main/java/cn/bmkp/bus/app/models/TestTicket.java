package cn.bmkp.bus.app.models;

import java.io.Serializable;

/**
 * Created by LW on 2016/3/19.
 */
public class TestTicket implements Serializable {
    private String date;
    private String time;
    private String from;
    private String to;
    private String driver_name;
    private String car_number;
    private boolean check_status;
    private int ticket_number;

    public TestTicket(String date, String time, String from, String to, String driver_name, String car_number, boolean check_status, int ticket_number) {
        this.date = date;
        this.time = time;
        this.from = from;
        this.to = to;
        this.driver_name = driver_name;
        this.car_number = car_number;
        this.check_status = check_status;
        this.ticket_number = ticket_number;
    }

    public int getTicket_number() {
        return ticket_number;
    }

    public void setTicket_number(int ticket_number) {
        this.ticket_number = ticket_number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public boolean getCheck_status() {
        return check_status;
    }

    public void setCheck_status(boolean check_status) {
        this.check_status = check_status;
    }
}
