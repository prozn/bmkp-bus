package cn.bmkp.bus.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.Ticket;

/**
 * Created by lw on 16/3/19.
 */

public class MyTicketsAdapter extends ArrayAdapter<Ticket> {

    private Context context;
    private ViewHolder holder;

    public MyTicketsAdapter(Context context, int resource, List<Ticket> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Ticket ticket = getItem(position);

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_my_ticket, parent,
                    false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);
        try {
            Date date = df.parse(ticket.getDay());
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);
            holder.tvItemMyTicketDate.setText(String.valueOf(ca.get(Calendar.MONTH)+1) + "月"
                    + String.valueOf(ca.get(Calendar.DATE)) + "日");
        } catch (ParseException e) {
            holder.tvItemMyTicketDate.setText(ticket.getDay());
        }

        holder.tvItemMyTicketTime.setText(ticket.getStations().get(0).getTime());
        holder.tvItemMyTicketFrom.setText("由："+ticket.getStations().get(0).getName());
        holder.tvItemMyTicketTo.setText("到："+ticket.getStations().get(ticket.getStations().size()-1).getName());


        holder.tvItemMyTicketDate.setTextColor(context.getResources().getColor(R.color.black_87));
        holder.tvItemMyTicketTime.setTextColor(context.getResources().getColor(R.color.black_87));
        holder.tvItemMyTicketFrom.setTextColor(context.getResources().getColor(R.color.black_87));
        holder.tvItemMyTicketTo.setTextColor(context.getResources().getColor(R.color.black_87));
        holder.tvItemMyTicketDot1.setTextColor(context.getResources().getColor(R.color.theme));
        holder.tvItemMyTicketDot2.setTextColor(context.getResources().getColor(R.color.theme));
        if (ticket.getState() == Const.TICKET_STATE_CHECKED) {
            holder.btnItemMyTicketStatus.setText("");
            holder.ivItemMyTicketStatus.setBackground(context.getResources().getDrawable(R.drawable.ic_checked));
            holder.tvItemMyTicketDate.setTextColor(context.getResources().getColor(R.color.black_54));
            holder.tvItemMyTicketTime.setTextColor(context.getResources().getColor(R.color.black_54));
            holder.tvItemMyTicketFrom.setTextColor(context.getResources().getColor(R.color.black_54));
            holder.tvItemMyTicketTo.setTextColor(context.getResources().getColor(R.color.black_54));
            holder.tvItemMyTicketDot1.setTextColor(Color.rgb(247, 165, 143));
            holder.tvItemMyTicketDot2.setTextColor(Color.rgb(247, 165, 143));
        }else if (ticket.getState() == Const.TICKET_STATE_PAID) {
            holder.ivItemMyTicketStatus.setBackground(null);
            holder.btnItemMyTicketStatus.setText(R.string.text_check_details);
        }else if (ticket.getState() == Const.TICKET_STATE_REFUNDED) {
            holder.btnItemMyTicketStatus.setText("");
            holder.ivItemMyTicketStatus.setBackground(context.getResources().getDrawable(R.drawable.ic_refunded));
            holder.rlTicketHead.setBackground(context.getResources().getDrawable(R.drawable.bg_ticket_head_gray));
        }else if (ticket.getState() == Const.TICKET_STATE_WAIT_REFUND) {
            holder.btnItemMyTicketStatus.setText("");
            holder.ivItemMyTicketStatus.setBackground(context.getResources().getDrawable(R.drawable.ic_wait_refund));
            holder.rlTicketHead.setBackground(context.getResources().getDrawable(R.drawable.bg_ticket_head_gray));
        }

        return convertView;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_my_ticket.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @Bind(R.id.tv_item_my_ticket_date)
        TextView tvItemMyTicketDate;
        @Bind(R.id.tv_item_my_ticket_time)
        TextView tvItemMyTicketTime;
        @Bind(R.id.tv_item_my_ticket_from)
        TextView tvItemMyTicketFrom;
        @Bind(R.id.tv_item_my_ticket_to)
        TextView tvItemMyTicketTo;
        @Bind(R.id.btn_item_my_ticket_status)
        TextView btnItemMyTicketStatus;
        @Bind(R.id.iv_item_my_ticket_status)
        ImageView ivItemMyTicketStatus;
        @Bind(R.id.tv_item_my_ticket_dot2)
        TextView tvItemMyTicketDot2;
        @Bind(R.id.tv_item_my_ticket_dot1)
        TextView tvItemMyTicketDot1;
        @Bind(R.id.rl_ticket_head)
        RelativeLayout rlTicketHead;
        @Bind(R.id.rl_address_more)
        RelativeLayout rlAddressMore;



        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
