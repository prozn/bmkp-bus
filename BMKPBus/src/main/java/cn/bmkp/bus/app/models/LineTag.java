package cn.bmkp.bus.app.models;

/**
 * Created by zhangn on 2016/3/23.
 *
 * description：线路的Tag
 */
public class LineTag {

    private String key;

    private String name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
