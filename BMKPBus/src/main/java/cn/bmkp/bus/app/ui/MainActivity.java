package cn.bmkp.bus.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.alibaba.fastjson.JSON;
import com.umeng.update.UmengUpdateAgent;
import com.wilddog.client.AuthData;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.adapter.BusLineAdapter;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.BusLine;
import cn.bmkp.bus.app.models.LineTag;
import cn.bmkp.bus.app.retrofit.CallbackHandler;
import cn.bmkp.bus.app.util.ActivityStack;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.CalendarUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.widget.CustomToast;
import cn.bmkp.bus.app.widget.DateAdapter;
import cn.bmkp.bus.app.widget.SpecialCalendar;
import cn.bmkp.bus.app.wilddog.WilddogApi;

public class MainActivity extends BaseActivity implements GestureDetector.OnGestureListener {

    @Bind(R.id.tv_date)
    TextView tvDate;
    @Bind(R.id.ll_week)
    LinearLayout llWeek;
    @Bind(R.id.flipper1)
    ViewFlipper flipper1;
    @Bind(R.id.rl_choose_date)
    RelativeLayout rlChooseDate;


    private long exitTime = 0;

    @Bind(R.id.iv_toolbar_exit)
    ImageView ivExit;
    @Bind(R.id.lv_bus_lines)
    ListView lvBusLines;
    @Bind(R.id.tv_show_not_item)
    TextView tvShowNotItem;
    @Bind(R.id.sp_choice_area)
    Spinner spChoiceArea;

    private List<BusLine> busLines;
    private List<LineTag> lineTags;
    private BusLineAdapter adapter;

    //滑动日历相关
    private static String TAG = "MainActivity";
    private GridView gridView = null;
    private GestureDetector gestureDetector = null;
    private int year_c = 0;
    private int month_c = 0;
    private int day_c = 0;
    private int week_c = 0;
    private int week_num = 0;
    private String currentDate = "";
    private DateAdapter dateAdapter;
    private int daysOfMonth = 0; // 某月的天数
    private int dayOfWeek = 0; // 具体某一天是星期几
    private int weeksOfMonth = 0;
    private SpecialCalendar sc = null;
    private boolean isLeapyear = false; // 是否为闰年
    private int selectPostion = 0;
    private String dayNumbers[] = new String[7];
    private int currentYear;
    private int currentMonth;
    private int currentWeek;
    private int currentDay;
    private int currentNum;

    private int selectMonth;
    private int selectDay;

    //退出登录
    @OnClick(R.id.iv_toolbar_exit)
    void exit(View view) {
        showAlertDialog(getString(R.string.text_exit), "", getString(R.string.search_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getPreferenceHelper().clear();
                ivExit.setVisibility(getPreferenceHelper().getLoginStatus() ? View.VISIBLE : View.INVISIBLE);
            }
        }, getString(R.string.text_cancel), null, this);
    }

    //查看钱包
    @OnClick(R.id.iv_toolbar_my_wallet)
    void myWallet(View view) {
        if (getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(MainActivity.this, MyWalletActivity.class);
            startActivity(intent);
        } else {
            showAlertDialog(getString(R.string.text_not_login_please_login), "", getString(R.string.search_confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra(Const.ACTIVITY_TO, MyWalletActivity.class.getSimpleName());
                    startActivity(intent);
                }
            }, getString(R.string.text_cancel), null, this);
        }
    }

    //查看已购买车票
    @OnClick(R.id.iv_toolbar_my_ticket)
    void checkMyTickets(View view) {
        if (getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(MainActivity.this, MyTicketsActivity.class);
            startActivity(intent);
        } else {
            showAlertDialog(getString(R.string.text_not_login_please_login), "", getString(R.string.search_confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra(Const.ACTIVITY_TO, MyTicketsActivity.class.getSimpleName());
                    startActivity(intent);
                }
            }, getString(R.string.text_cancel), null, this);
        }
    }

    @OnItemClick(R.id.lv_bus_lines)
    void itemBusLineClick(AdapterView<?> parent, View view, int position, long id) {
        //进入巴士路线详情
        Intent intent = new Intent(this, BusLineIntroActivity.class);
        intent.putExtra("busline", JSON.toJSONString(busLines.get(position)));
        //获取选择的日期
        Calendar pickDate = Calendar.getInstance();
        pickDate.set(currentYear, selectMonth - 1, selectDay);
        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);

        String date = df.format(pickDate.getTime());
        intent.putExtra(Const.BUS_LINE_SELECTED_DATE, date);
        startActivity(intent);
    }

//    @OnClick(R.id.rl_collect_line)
//    void tvCollectLine(View view) {
//        showCollectLineDialog();
//    }

    private String startCity, endCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_choose_bus_line);
        ButterKnife.bind(this);

//        initView();
        initCalendar();

        // 提示登录送金币的弹框
        if (getIntent().getStringExtra("from") != null
                && getIntent().getStringExtra("from").equals(LauncherActivity.class.getSimpleName())
                && !getPreferenceHelper().getLoginStatus()) {
            showLoginSendGoldsDialog();
        }

        if (AndyUtils.isNetworkAvailable(this)) {
            showProgressDialog(getString(R.string.text_is_loading));
            checkAndUpdateToken();
            checkAppUpdate();
        } else {
            showNetworkDialog();
            noNetWorkShow();
        }

        loadLineTags();

        tvDate.setText(month_c + "月");
        gestureDetector = new GestureDetector(this);
        dateAdapter = new DateAdapter(this, currentYear, currentMonth,
                currentWeek, currentWeek == 1 ? true : false);
        addGridView();
        dayNumbers = dateAdapter.getDayNumbers();
        gridView.setAdapter(dateAdapter);
        selectPostion = dateAdapter.getTodayPosition();
        gridView.setSelection(selectPostion);
        flipper1.addView(gridView, 0);
    }


    /**
     * 检查并更新Token
     */
    private void checkAndUpdateToken(){

        String token = getPreferenceHelper().getToken();

        if(TextUtils.isEmpty(token)) {
            getPreferenceHelper().clear();
        }else {
            getApi().refreshToken(token, new RefreshTokenCallback());
        }
    }

    /**
     * 刷新Token的请求回调
     */
    class RefreshTokenCallback extends CallbackHandler {

        @Override
        public void onSuccess(JSONObject data) {
            super.onSuccess(data);

            try{
                // 认证
                authToken(getParseHelper().parseToken(data));

            }catch (Exception e) {
                getPreferenceHelper().clear();
            }
        }

        @Override
        public void onError(int code, JSONObject data) {
            super.onError(code, data);
            getPreferenceHelper().clear();
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            getPreferenceHelper().clear();
        }
    }

    /**
     * token认证
     * @param token
     */
    private void authToken(final String token){

        WilddogApi.getInstance().authWithToken(token, new Wilddog.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                getPreferenceHelper().putToken(token);

                Log.d("zhangn", "refresh token ===>succ");
            }

            @Override
            public void onAuthenticationError(WilddogError wilddogError) {
                getPreferenceHelper().clear();
                Log.d("zhangn", "refresh token ===>fail");
            }
        });
    }

    /**
     * 初始化日历显示
     */
    private void initCalendar() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
        currentDate = sdf.format(date);
        year_c = Integer.parseInt(currentDate.split("-")[0]);
        month_c = Integer.parseInt(currentDate.split("-")[1]);
        day_c = Integer.parseInt(currentDate.split("-")[2]);
        currentYear = year_c;
        currentMonth = month_c;
        currentDay = day_c;
        // 选择的月和日
        selectMonth = month_c;
        selectDay = day_c;
        sc = new SpecialCalendar();

        isLeapyear = Boolean.valueOf(CalendarUtils.getCalendar(year_c, month_c,sc).get(0));
        daysOfMonth = Integer.valueOf(CalendarUtils.getCalendar(year_c, month_c,sc).get(1));
        dayOfWeek = Integer.valueOf(CalendarUtils.getCalendar(year_c, month_c,sc).get(2));

        week_num = CalendarUtils.getWeeksOfMonth(dayOfWeek, daysOfMonth);
        currentNum = week_num;
        if (dayOfWeek == 7) {
            week_c = currentDay / 7 + 1;
        } else {
            if (currentDay <= (7 - dayOfWeek)) {
                week_c = 1;
            } else {
                if ((currentDay - (7 - dayOfWeek)) % 7 == 0) {
                    week_c = (currentDay - (7 - dayOfWeek)) / 7 + 1;
                } else {
                    week_c = (currentDay - (7 - dayOfWeek)) / 7 + 2;
                }
            }
        }
        currentWeek = week_c;
        getCurrent();

    }


    /**
     * 加载线路标签
     */
    private void loadLineTags() {

        WilddogApi.getInstance().getLineTags(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                Log.d("zhangn", "======>" + dataSnapshot);

                lineTags = ParseUtil.parseLineTags(dataSnapshot);
                if (lineTags.size() > 0) {
                    initView();
                } else {

                    // TODO tag 列表为0
                    noLineItemShow();
                }
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {

                Log.d("zhangn", "===>" + wilddogError.getMessage() + wilddogError.getCode());

            }
        });
    }

    private void initView() {

        String[] arr = new String[lineTags.size()];

        for (int i = 0; i < lineTags.size(); i++) {
            arr[i] = lineTags.get(i).getName();
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spChoiceArea.setAdapter(arrayAdapter);


        spChoiceArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(MainActivity.this, parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

                if ((selectMonth * 100 + selectDay) < (month_c * 100 + day_c)) {
//                    busLines.clear();
//                    adapter.notifyDataSetChanged();

                    noLineItemShow();
                } else {
                    setListData();
                }
//                showProgressDialog("加载中…");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        setListData();

    }

    private void setListData() {

        if(!AndyUtils.isNetworkAvailable(this)){
            noNetWorkShow();
        }
        //获取选择的日期
        Calendar pickDate = Calendar.getInstance();
        pickDate.set(currentYear, selectMonth - 1, selectDay);
        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);

        String date = df.format(pickDate.getTime());
        String tag = "tag" + String.valueOf(spChoiceArea.getSelectedItemPosition() + 1);

        WilddogApi.getInstance().getBusRunInfo(date, tag, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                busLines = ParseUtil.parseBusRuns(dataSnapshot);
                adapter = new BusLineAdapter(MainActivity.this, busLines);
                lvBusLines.setAdapter(adapter);
                if (busLines.size() > 0) {


                    lvBusLines.setVisibility(View.VISIBLE);
                    tvShowNotItem.setVisibility(View.GONE);
                } else {
                    noLineItemShow();

                }

                dismissDialog();
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                Toast.makeText(MainActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();

                noLineItemShow();
                dismissDialog();
            }
        });
    }

    private void noLineItemShow() {
        lvBusLines.setVisibility(View.GONE);
        tvShowNotItem.setText(R.string.tv_bus_line_not_item_tip);
        tvShowNotItem.setVisibility(View.VISIBLE);
    }

    private void noNetWorkShow() {
        lvBusLines.setVisibility(View.GONE);
        tvShowNotItem.setText(R.string.text_network_no_tip);
        tvShowNotItem.setVisibility(View.VISIBLE);
    }
    /**
     * 检查app是否有更新
     */
    private void checkAppUpdate() {
        UmengUpdateAgent.setUpdateOnlyWifi(false);
        UmengUpdateAgent.setDeltaUpdate(false);
        UmengUpdateAgent.update(getApplicationContext());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(getApplicationContext(), R.string.text_press_again_to_exit, Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                finish();
                ActivityStack.getInstance().finishAllActivity();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void addGridView() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        gridView = new GridView(this);
        gridView.setNumColumns(7);
        gridView.setGravity(Gravity.CENTER_VERTICAL);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView.setVerticalSpacing(1);
        gridView.setHorizontalSpacing(1);
        gridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return MainActivity.this.gestureDetector.onTouchEvent(event);
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                selectDay = Integer.parseInt(dayNumbers[position]);
                selectMonth = dateAdapter.getCurrentMonth(position);

                Log.i(TAG, "day:" + selectDay);
                selectPostion = position;
                dateAdapter.setSeclection(position);
                dateAdapter.notifyDataSetChanged();
                int today = month_c * 100 + day_c;
                int selectDate = (dateAdapter.getCurrentMonth(selectPostion)) * 100 + selectDay;

                if (selectDate < today) {
                    CustomToast.showToast(MainActivity.this, getString(R.string.text_can_not_buy_old_tickets), 2000);
//                    busLines.clear();
//                    adapter.notifyDataSetChanged();

                    noLineItemShow();
                } else {
                    tvDate.setText(dateAdapter.getCurrentMonth(selectPostion) + getString(R.string.text_month));

                    //刷新路线
                    setListData();
                }
            }
        });
        gridView.setLayoutParams(params);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // jumpWeek = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ivExit.setVisibility(getPreferenceHelper().getLoginStatus() ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public boolean onDown(MotionEvent e) {

        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }



    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        int gvFlag = 0;
        if (e1.getX() - e2.getX() > 80) {
            // 向左滑
            addGridView();
            currentWeek++;

            getCurrent();

            dateAdapter = new DateAdapter(this, currentYear, currentMonth,
                    currentWeek, currentWeek == 1 ? true : false);
            dayNumbers = dateAdapter.getDayNumbers();
            gridView.setAdapter(dateAdapter);
            tvDate.setText(dateAdapter.getCurrentMonth(selectPostion) + "月");
            gvFlag++;
            flipper1.addView(gridView, gvFlag);
            dateAdapter.setSeclection(selectPostion);
            this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
                    R.anim.push_left_in));
            this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
                    R.anim.push_left_out));
            this.flipper1.showNext();
            flipper1.removeViewAt(0);
            return true;

        } else if (e1.getX() - e2.getX() < -80) {
            addGridView();
            currentWeek--;
            getCurrent();

            dateAdapter = new DateAdapter(this, currentYear, currentMonth,
                    currentWeek, currentWeek == 1 ? true : false);
            dayNumbers = dateAdapter.getDayNumbers();
            gridView.setAdapter(dateAdapter);
            tvDate.setText(dateAdapter.getCurrentMonth(selectPostion) + "月");
            gvFlag++;
            flipper1.addView(gridView, gvFlag);
            dateAdapter.setSeclection(selectPostion);
            this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
                    R.anim.push_right_in));
            this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
                    R.anim.push_right_out));
            this.flipper1.showPrevious();
            flipper1.removeViewAt(0);
            return true;
        }
        return false;
    }

    /**
     * 重新计算当前的年月
     */
    public void getCurrent() {
        List<Integer> integers = new ArrayList<>();
        if (currentWeek > currentNum) {
            if (currentMonth + 1 <= 12) {
                currentMonth++;
            } else {
                currentMonth = 1;
                currentYear++;
            }
            currentWeek = 1;
            currentNum = CalendarUtils.getWeeksOfMonth(currentYear, currentMonth,sc);
        } else if (currentWeek == currentNum) {
            if (CalendarUtils.getLastDayOfWeek(currentYear, currentMonth,sc) == 6) {
            } else {
                if (currentMonth + 1 <= 12) {
                    currentMonth++;
                } else {
                    currentMonth = 1;
                    currentYear++;
                }
                currentWeek = 1;
                currentNum =  CalendarUtils.getWeeksOfMonth(currentYear, currentMonth,sc);
            }

        } else if (currentWeek < 1) {
            if (currentMonth - 1 >= 1) {
                currentMonth--;
            } else {
                currentMonth = 12;
                currentYear--;
            }
            currentNum =  CalendarUtils.getWeeksOfMonth(currentYear, currentMonth,sc);
            currentWeek = currentNum - 1;
        }
    }

}
