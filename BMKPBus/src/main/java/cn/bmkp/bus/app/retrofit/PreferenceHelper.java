package cn.bmkp.bus.app.retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import cn.bmkp.bus.app.common.Const;


public class PreferenceHelper {

    private SharedPreferences app_prefs;
    private final String LOGIN_STATUS = "login_status";
    private final String USER_NAME = "user_name";
    private final String VERIFY_CODE = "user_verifyCode";
    private final String NOTIFY_TOKEN = "notify_token";
    private Context context;

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(Const.PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putLoginStatus(boolean status) {
        Editor edit = app_prefs.edit();
        edit.putBoolean(LOGIN_STATUS, status);
        edit.commit();
    }

    public Boolean getLoginStatus() {
        return app_prefs.getBoolean(LOGIN_STATUS, false);
    }

    public void putToken(String token) {
        Editor edit = app_prefs.edit();
        edit.putString(NOTIFY_TOKEN, token);
        edit.commit();
    }

    public String getToken() {
        return app_prefs.getString(NOTIFY_TOKEN, null);
    }

    public void putUserName(String name) {
        Editor edit = app_prefs.edit();
        edit.putString(USER_NAME, name);
        edit.commit();
    }

    public String getUserName() {
        return app_prefs.getString(USER_NAME, null);
    }

    public void putVerifyCode(String verifyCode) {
        Editor edit = app_prefs.edit();
        edit.putString(VERIFY_CODE, verifyCode);
        edit.commit();
    }

    public String getVerifyCode() {
        return app_prefs.getString(VERIFY_CODE, null);
    }
    public void clear() {
        Editor edit = app_prefs.edit();
        edit.clear();
        edit.commit();
    }
}
