package cn.bmkp.bus.app.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.BmkpBusApplication;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by chenliang on 2016/4/5.
 */

public class LineCollectionDialog extends DialogFragment {


    @Bind(R.id.et_collect_start_point)
    MaterialEditText etCollectStartPoint;
    @Bind(R.id.et_collect_end_point)
    MaterialEditText etCollectEndPoint;
    @Bind(R.id.sp_collect_line_hour)
    Spinner spCollectLineHour;
    @Bind(R.id.sp_collect_line_minute)
    Spinner spCollectLineMinute;
    private Dialog dialog;

    private Timer dismissTimer;

    @OnClick(R.id.ib_clean_dialog)
    public void ibCleanDialog(View view) {
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.btn_collect_line)
    public void dialogColletLine(View view) {
        if (TextUtils.isEmpty(etCollectStartPoint.getText())) {
            Toast.makeText(getActivity(), "出发地地址不能为空", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(etCollectEndPoint.getText())){
            Toast.makeText(getActivity(), "目的地地址不能为空", Toast.LENGTH_SHORT).show();
        }else{
            // 存储 路线
            String start = etCollectStartPoint.getText().toString();
            String end = etCollectEndPoint.getText().toString();
            String hour = spCollectLineHour.getSelectedItem().toString();
            String minute = spCollectLineMinute.getSelectedItem().toString();

//            Toast.makeText(getActivity(), "起点:" + start + "\n终点:" + end
//                    +"\n hour:" + hour +"\nminute:" + minute, Toast.LENGTH_SHORT).show();

            WilddogApi.getInstance().collectLine(start, end, hour + ":" + minute,
                    ((BmkpBusApplication) getActivity().getApplication()).getPreferenceHelper().getUserName(),
                    new Wilddog.CompletionListener() {
                @Override
                public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                    if (wilddogError == null) {

                        TextView title = new TextView(getActivity());
                        title.setText("提交成功!");
                        title.setTextColor(Color.BLACK);
                        title.setTextSize(16);
                        title.setGravity(android.view.Gravity.CENTER_HORIZONTAL);

                        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                .setCustomTitle(title)
                                .setMessage("我们已收录您的路线信息," +
                                        "\n如有开通会即刻通知您.")
                                .setCancelable(false)
                                .show();

                        dismissTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                alertDialog.dismiss();
                                dismissAllowingStateLoss();
                            }
                        }, 3000);

                    } else {
                        Toast.makeText(getActivity(), "Error:" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dismissTimer = new Timer();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_line_collection, null);

        ButterKnife.bind(this, view);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(view, lp);

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        dismissTimer.cancel();
    }
}
