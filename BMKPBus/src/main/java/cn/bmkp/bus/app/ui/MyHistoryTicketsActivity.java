package cn.bmkp.bus.app.ui;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.WilddogError;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.adapter.MyTicketsAdapter;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.Ticket;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by chenliang on 2016/3/28.
 */
public class MyHistoryTicketsActivity extends BaseActivity {

    @Bind(R.id.lv_my_tickets)
    ListView lvMyTickets;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_show_not_item)
    TextView tvShowNotItem;

//    private List<Ticket> tickets = null;
//
//    private List<Ticket> unusedTickets;

    private List<Ticket> usedTickets;

    private MyTicketsAdapter ticketsAdapter;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_my_history_tickets);

        ButterKnife.bind(this);

        tvTitle.setText(R.string.text_history_ticket);


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (AndyUtils.isNetworkAvailable(this)) {
            showProgressDialog(getString(R.string.text_is_loading));
        } else {
            showNetworkDialog();
        }

        setData();

        lvMyTickets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (usedTickets.get(position).getState() == Const.TICKET_STATE_CHECKED) {
                    Toast.makeText(MyHistoryTicketsActivity.this, R.string.text_ticket_used, Toast.LENGTH_SHORT).show();
                }else if (usedTickets.get(position).getState() == Const.TICKET_STATE_REFUNDED) {
                    Toast.makeText(MyHistoryTicketsActivity.this, R.string.text_ticket_cancel, Toast.LENGTH_SHORT).show();
                }else if (usedTickets.get(position).getState() == Const.TICKET_STATE_WAIT_REFUND) {
                    Toast.makeText(MyHistoryTicketsActivity.this, R.string.text_ticket_wait_for_cancel, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setData() {
        if (getPreferenceHelper().getLoginStatus() && getPreferenceHelper().getUserName()!=null) {
            WilddogApi.getInstance().getUsedTicket(getPreferenceHelper().getUserName(), new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

//                    tickets = ParseUtil.parseUserTickets(dataSnapshot);
//
//                    unusedTickets = new ArrayList<Ticket>();

                    usedTickets = ParseUtil.parseUserTickets(dataSnapshot);

//                    for (Ticket ticket : tickets) {
//
//                        if (ticket.getState() == Const.TICKET_STATE_PAID) {
//                            unusedTickets.add(ticket);
//                        } else if (ticket.getState() == Const.TICKET_STATE_CHECKED ||
//                                ticket.getState() == Const.TICKET_STATE_REFUNDED
//                                || ticket.getState() == Const.TICKET_STATE_WAIT_REFUND) {
//                            usedTickets.add(ticket);
//                        }
//                    }

                    if (usedTickets.size() > 0) {
                        ticketsAdapter = new MyTicketsAdapter(MyHistoryTicketsActivity.this, R.layout.list_item_my_ticket, usedTickets);
                        lvMyTickets.setAdapter(ticketsAdapter);
                        tvShowNotItem.setVisibility(View.GONE);
                    } else {
                        tvShowNotItem.setVisibility(View.VISIBLE);
                    }

                    dismissDialog();
                }

                @Override
                public void onCancelled(WilddogError wilddogError) {
                    dismissDialog();
                }
            });
        }

    }
}
