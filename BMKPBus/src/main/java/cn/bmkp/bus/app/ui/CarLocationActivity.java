package cn.bmkp.bus.app.ui;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.WilddogError;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.models.Bus;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by LW on 2016/3/19.
 */

public class CarLocationActivity extends BaseActivity {

    @Bind(R.id.mapview_driver)
    MapView mapView;
    @Bind(R.id.tv_title)
    TextView tvTitle;

    private AMap aMap;

    private Bus bus;


    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_car_location);
        ButterKnife.bind(this);

        tvTitle.setText(R.string.text_driver_location);

        showProgressDialog(getString(R.string.text_location));
        mapView.onCreate(savedInstanceState);
        initMap();

        loadBusInfo(getIntent().getStringExtra("busNumber"));
    }

    /**
     * 初始化地图
     */
    private void initMap() {
        if (mapView != null) {
            aMap = mapView.getMap();
            //设置当前地图是否支持所有手势
            aMap.getUiSettings().setAllGesturesEnabled(true);
            //设置定位按钮是否显示
            aMap.getUiSettings().setMyLocationButtonEnabled(false);
            //设置比例尺功能是否可用
            aMap.getUiSettings().setScaleControlsEnabled(false);
            //地图是否允许通过手势来移动
            aMap.getUiSettings().setScrollGesturesEnabled(true);
            //地图是否允许显示缩放按钮
            aMap.getUiSettings().setZoomControlsEnabled(true);
            //地图是否允许通过手势来缩放
            aMap.getUiSettings().setZoomGesturesEnabled(true);

        }
    }


    private void loadBusInfo(String busNumber) {

        // busNumber 从ticket中获得
        WilddogApi.getInstance().getBusInfo(busNumber, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                dismissDialog();

                bus = (Bus) dataSnapshot.getValue(Bus.class);

                // 标记司机位置
                if (bus.getLatitude() != null && bus.getLongtitude() != null) {

                    markerCar(bus.getLatitude(), bus.getLongtitude());
                } else {
                    Toast.makeText(CarLocationActivity.this, R.string.text_get_driver_location_fail, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                dismissDialog();

                Toast.makeText(CarLocationActivity.this, R.string.text_get_driver_location_fail, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null) {
            mapView.onSaveInstanceState(outState);
        }
    }

    /**
     * 标记车辆位置
     */
    public void markerCar(String lat, String lon) {
        LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));//车辆位置经纬度
        aMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));
        aMap.addMarker(new MarkerOptions()
                .position(latLng)
                        //标记图标  待修改
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location)));
    }
}
