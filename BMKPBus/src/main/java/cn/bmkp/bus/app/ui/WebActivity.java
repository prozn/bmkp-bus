package cn.bmkp.bus.app.ui;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;

public class WebActivity extends BaseActivity {

    @Bind(R.id.webview)
    WebView webview;

    @OnClick(R.id.ib_function_left)
    public void toBack(View view){
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);

        webview.loadUrl("http://api.bmkp.cn/page/refund-instruction");
        webview.requestFocus(); //获取焦点

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

}
