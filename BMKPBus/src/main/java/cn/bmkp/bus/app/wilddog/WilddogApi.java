package cn.bmkp.bus.app.wilddog;

import android.content.Context;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.BusRun;
import cn.bmkp.bus.app.util.ParseUtil;

/**
 * Created by zhangn on 2016/3/19.
 * <p/>
 * description:Wilddog api
 */
public class WilddogApi {


    private static String SERVICE_URL = "https://bmkpbus.wilddogio.com/";

//    private static String SERVICE_URL = "https://data2016.wilddogio.com/";

    private static String LINE = "line"; // 线路根节点

    private static String COLLECT_LINE = "collectLine";  // 线路征集根节点

    private static String RUN = "run";  // 实际的运行线路

    private static String TICKET = "ticket";

    private static String BUS = "bus";

    private static String KEYWORD = "keyword";

    private static String OWNERS = "owners";

    private Wilddog mWilddog;

    private static WilddogApi wilddogApi;

    private WilddogApi() {

    }

    /**
     * Wilddog初始化
     *
     * @param context
     */
    public void init(Context context) {

        if (context == null) {
            return;
        }

        Wilddog.setAndroidContext(context);
        mWilddog = new Wilddog(SERVICE_URL);
    }

    public static WilddogApi getInstance() {

        if (wilddogApi == null) {
            wilddogApi = new WilddogApi();
        }

        return wilddogApi;
    }


    /**
     * token 认证
     * @param token
     * @param authResultHandler
     */
    public void authWithToken(String token,Wilddog.AuthResultHandler authResultHandler){

        if(mWilddog == null) {
            return;
        }

       mWilddog.authWithCustomToken(token, authResultHandler);
    }


    /**
     * 查询线路信息
     *
     * @param lineTag     线路标签
     * @param valueEventListener
     */
    public void getBusLineInfo(String lineTag, ValueEventListener valueEventListener) {

        if (mWilddog == null) {
            return;
        }

        // 班次信息只需查询一次
        mWilddog.child(LINE).orderByChild(lineTag).equalTo(true).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 查询线路动态信息
     *
     * @param lineTag     线路标签
     * @param valueEventListener
     */
    public void getBusRunInfo(String date, String lineTag, ValueEventListener valueEventListener) {

        if (mWilddog == null) {
            return;
        }

        // 班次信息只需查询一次
        mWilddog.child(RUN).child(date).orderByChild(lineTag).equalTo(true).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 查询余票信息
     *
     * @param lineKey
     * @param date
     * @param valueEventListener
     */
    public void getBusRemainTicket(String lineKey, String date, String startStation, String endStation, ValueEventListener valueEventListener) {
        if (mWilddog == null) {
            return;
        }

        mWilddog.child(RUN).child(date).child(lineKey).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 监听余票信息
     *
     * @param lineKey
     * @param date
     * @param valueEventListener
     */
    public void setBusRemainTicketListener(String lineKey, String date, String startStation, String endStation, ValueEventListener valueEventListener) {
        if (mWilddog == null) {
            return;
        }

        mWilddog.child(RUN).child(date).child(lineKey).addValueEventListener(valueEventListener);
    }

    /**
     * 清除余票信息监听
     *
     * @param lineKey
     * @param date
     * @param valueEventListener
     */
    public void removeBusRemainTicketListener(String lineKey, String date, String startStation, String endStation, ValueEventListener valueEventListener) {
        if (mWilddog == null) {
            return;
        }

        mWilddog.child(RUN).child(date).child(lineKey).removeEventListener(valueEventListener);
    }

    /**
     * 买票
     *
     * @param busRun       线路
     * @param date         日期
     * @param startStation 起点站
     * @param endStation   终点站
     * @param ticketNum    终点站
     * @param listener
     */
    public void buyTicket(final String lineKey, final String userPhone, final String orderno,final BusRun busRun, final String date,
                          final int startStation, final int endStation, final int ticketNum, final Wilddog.CompletionListener listener) {


        //查询余票
        getBusRemainTicket(lineKey, date, "", "", new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                BusRun run = ParseUtil.parseBusRun(dataSnapshot);

                Map<String, Object> seats = new HashMap<>();
                for (int i = startStation; i < endStation; i++) {

                    int seatLeft = Integer.parseInt(run.getSeatsLeft().get(i)) - ticketNum;

                    if (seatLeft >= 0) {
                        seats.put(KeyConst.BUS_RUN_REMAIN + String.valueOf(i + 1), String.valueOf(seatLeft));
                    } else {
                        //余票不足
                        listener.onComplete(new WilddogError(1, "余票不足"), null);
                        return;
                    }
                }

                //减少座位数
                mWilddog.child(RUN).child(date).child(lineKey).updateChildren(seats, new Wilddog.CompletionListener() {
                    @Override
                    public void onComplete(WilddogError wilddogError, Wilddog wilddog) {

                        if (wilddogError != null) {

                            listener.onComplete(wilddogError, wilddog);

                        } else {

                            // 向Owner/Ticket节点中用户帐号下增加一条数据

                            addOwnerTicket(userPhone, orderno, busRun, date, startStation, endStation, ticketNum, listener);
                        }
                    }
                });
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {

                listener.onComplete(wilddogError, null);

            }
        });
    }

    /**
     * 增加下车人数
     *
     * @param lineKey       线路
     * @param date         日期
     * @param endStation   终点站
     * @param downNumber    人数
     * @param listener
     */
    public void addPeopleDown(final String lineKey, final int downNumber, final String date,
                              final int endStation, final Wilddog.CompletionListener listener) {

        //查询余票
        getBusRemainTicket(lineKey, date, "", "", new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                BusRun run = ParseUtil.parseBusRun(dataSnapshot);

                int downBefore = 0;
                if (run.getStations().get(endStation).getDownNum() != null
                        && run.getStations().get(endStation).getDownNum().length() > 0) {
                    downBefore = Integer.parseInt(run.getStations().get(endStation).getDownNum());
                }

                String downAmount = String.valueOf(downBefore + downNumber);

                Map<String, Object> down = new HashMap<>();
                down.put(KeyConst.STATION_DOWN_NUM, downAmount);

                //增加人数
                mWilddog.child(RUN).child(date).child(lineKey).child(KeyConst.STATION + String.valueOf(endStation + 1))
                        .updateChildren(down, listener);
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                listener.onComplete(wilddogError, null);
            }
        });
    }

    /**
     * 添加用户的购买Ticket
     */
    public void addOwnerTicket(String userPhone, final String orderNo, final BusRun busRun, final String date,
                               final int startStation, final int endStation, final int ticketNum, final Wilddog.CompletionListener listener) {

        // 向Ticket节点中用户帐号下增加一条数据
        Map ticket = getUserTicketInfo(busRun, date, startStation, endStation, ticketNum);

        Map<String, Object> map = new HashMap<>();
        map.put(orderNo, ticket);

        mWilddog.child(OWNERS).child(userPhone).child(TICKET).updateChildren(map, listener);
    }

    /**
     * 获取用户的购买记录
     */
    public void getUserTicketHistory(String phone, ValueEventListener valueEventListener) {


       // mWilddog.child(TICKET).child(phone).addListenerForSingleValueEvent(valueEventListener);

        mWilddog.child(OWNERS).child(phone).child(TICKET).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 生成车票
     */
    private Map<String, Object> getUserTicketInfo(BusRun busRun, String date, int startStation, int endStation, int ticketNum) {

        Map<String, Object> map = new HashMap<>();

        map.put(KeyConst.TICKET_LICENSE_NUMBER, busRun.getLicenseNumber());
        map.put(KeyConst.TICKET_LINE, busRun.getLineKey());
        map.put(KeyConst.TICKET_PAYMENT, String.valueOf(1));
        map.put(KeyConst.TICKET_CHECK, String.valueOf(false));
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        map.put(KeyConst.TICKET_CREATE_AT, df.format(Calendar.getInstance().getTime()));
        map.put(KeyConst.TICKET_DAY, String.valueOf(date));
        map.put(KeyConst.TICKET_TICKET_NUMBER, String.valueOf(ticketNum));
        map.put(KeyConst.TICKET_BUS_NUMBER, busRun.getBusNumber());
        map.put(KeyConst.TICKET_DRIVER_NAME, busRun.getDriverName());
        map.put(KeyConst.TICKET_STATE, Const.TICKET_STATE_PAID);

        for (int i = 0; i < busRun.getStations().size(); i++) {
            Map<String, Object> station = new HashMap<>();
            if (i < startStation || i > endStation) {
                station.put(KeyConst.STATION_DETAIL, "");
                station.put(KeyConst.STATION_NAME, "");
                station.put(KeyConst.STATION_TIME, "");
            } else {
                station.put(KeyConst.STATION_DETAIL, busRun.getStations().get(i).getDetail());
                station.put(KeyConst.STATION_NAME, busRun.getStations().get(i).getName());
                station.put(KeyConst.STATION_TIME, busRun.getStations().get(i).getTime());
            }
            map.put(KeyConst.STATION + String.valueOf(i + 1), station);
        }

        Map<String, Object> payMap = new HashMap<>();

        payMap.put(KeyConst.TICKET_FROM_WX, "");
        payMap.put(KeyConst.TICKET_FROM_ALIPAY, "test@qq.com");

        map.put(KeyConst.TICKET_FROM, payMap);

        return map;
    }


    /**
     * 获取bus的信息(包括经纬度)
     * @param busNumer
     * @param valueEventListener
     */
    public void getBusInfo(String busNumer, ValueEventListener valueEventListener){

        if(mWilddog == null) {
            return;
        }

        mWilddog.child(BUS).child("bus" + busNumer).addListenerForSingleValueEvent(valueEventListener);
    }


    /**
     * 获取所有线路的Tag
     * @param valueEventListener
     */
    public void getLineTags(ValueEventListener valueEventListener){

        if(mWilddog == null) {
            return;
        }

        mWilddog.child(KEYWORD).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 检票
     *
     * @param userPhone       用户名
     * @param ticketKey       key
     * @param listener
     */
    public void checkTicket(final String userPhone, final String ticketKey, final Wilddog.CompletionListener listener) {
        Map ticket = new HashMap();

        ticket.put(KeyConst.TICKET_STATE, Const.TICKET_STATE_CHECKED);
        ticket.put(KeyConst.TICKET_CHECK, String.valueOf(true));
        mWilddog.child(OWNERS).child(userPhone).child(TICKET).child(ticketKey).updateChildren(ticket, listener);

    }

    /**
     * 退票
     *
     * @param userPhone       用户名
     * @param ticketKey       key
     * @param listener
     */
    public void getRefundForTicket(final String userPhone, final String ticketKey, final Wilddog.CompletionListener listener) {
        Map ticket = new HashMap();

        ticket.put(KeyConst.TICKET_STATE, Const.TICKET_STATE_WAIT_REFUND);
        mWilddog.child(OWNERS).child(userPhone).child(TICKET).child(ticketKey).updateChildren(ticket, listener);

    }

    /**
     * 监听票的状态
     *
     * @param userPhone       用户名
     * @param ticketKey       key
     * @param listener
     */
    public void setTicketListener(final String userPhone, final String ticketKey, final ValueEventListener listener) {

        mWilddog.child(OWNERS).child(userPhone).child(TICKET).child(ticketKey).addValueEventListener(listener);

    }


    /**
     * 查询用户余额等信息
     *
     * @param userPhone
     * @param valueEventListener
     */
    public void getOwnerInfo(String userPhone, ValueEventListener valueEventListener) {
        if (mWilddog == null) {
            return;
        }

        mWilddog.child(OWNERS).child(userPhone).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 扣除票钱
     *
     * @param userPhone       用户名
     * @param money       减去的金额
     * @param listener
     */
    public void modifyOwnerBalance(final String userPhone, final Float money, final Wilddog.CompletionListener listener) {

        if (mWilddog == null) {
            return;
        }

        mWilddog.child(OWNERS).child(userPhone).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Float balance = ParseUtil.parseOwnerBalance(dataSnapshot) - money;
                Map map = new HashMap();

                map.put(KeyConst.USER_BALANCE, String.valueOf(balance));
                mWilddog.child(OWNERS).child(userPhone).updateChildren(map, listener);
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                listener.onComplete(wilddogError, null);
            }
        });
    }


    /**
     * 向Ticket池中插入ticket
     * @param day
     * @param linekey
     * @param phone
     * @param ticketKey
     */
    public void insertUserTicketToTicketPool(String day, String linekey, String phone, String ticketKey, Wilddog.CompletionListener completionListener){

        if(mWilddog == null) {
            return;
        }

        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put(KeyConst.USER_PHONE,phone);
        paramMap.put(KeyConst.TICKET_KEY, ticketKey);

        mWilddog.child(TICKET).child(day).child(linekey).push().setValue(paramMap, completionListener);

    }

    /**
     * 查询最新生成的车票信息
     *
     * @param userPhone       用户名
     * @param ticketCount     数量
     * @param listener
     */
    public void getTicketLimitToLast(final String userPhone, final int ticketCount, final ValueEventListener listener) {

        mWilddog.child(OWNERS).child(userPhone).child(TICKET).limitToLast(ticketCount).addListenerForSingleValueEvent(listener);
    }

    /**
     * 获取用户未使用的车票
     */
    public void getUnusedTicket(String phone, ValueEventListener valueEventListener) {

        mWilddog.child(OWNERS).child(phone).child(TICKET).orderByChild(KeyConst.TICKET_STATE).equalTo(Const.TICKET_STATE_PAID).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 获取用户已使用的车票
     */
    public void getUsedTicket(String phone, ValueEventListener valueEventListener) {

        mWilddog.child(OWNERS).child(phone).child(TICKET).orderByChild(KeyConst.TICKET_CHECK).equalTo(String.valueOf(true)).addListenerForSingleValueEvent(valueEventListener);
    }

    /**
     * 路线征集
     * @param start  起点
     * @param end   终点
     * @param time  乘车时间
     */
    public void collectLine(String start, String end, String time, String userName, Wilddog.CompletionListener completionListener){

        if(mWilddog == null) {
            return;
        }

        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put(KeyConst.USER_PHONE, userName);
        paramMap.put("startAddress", start);
        paramMap.put("endAddress", end);
        paramMap.put("startTime", time);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        mWilddog.child(COLLECT_LINE).child(df.format(Calendar.getInstance().getTime())).push().setValue(paramMap, completionListener);

    }

    public class KeyConst {
        public static final String BUS_RUN_REMAIN = "remain";

        public static final  String TICKET_DAY = "day";
        public static final  String TICKET_DRIVER_NAME = "driverName";
        public static final  String TICKET_LICENSE_NUMBER = "licenseNumber";
        public static final  String TICKET_LINE = "line";
        public static final  String TICKET_TICKET_NUMBER = "ticketNum";
        public static final  String TICKET_BUS_NUMBER = "busNumber";
        public static final  String TICKET_PAYMENT = "payment";
        public static final  String TICKET_STATE = "state";
        public static final  String TICKET_CHECK = "check";
        public static final  String TICKET_CREATE_AT = "createAt";
        public static final  String TICKET_UPDATE_AT = "updateAt";
        public static final  String TICKET_VALIDITY = "validity";
        public static final  String TICKET_FROM = "from";
        public static final  String TICKET_FROM_WX = "weixin";
        public static final  String TICKET_FROM_ALIPAY = "alipay";
        public static final  String TICKET_KEY = "ticketKey";



        public static final  String STATION = "station";
        public static final  String STATION_DETAIL = "detail";
        public static final  String STATION_NAME = "name";
        public static final  String STATION_TIME = "time";
        public static final  String STATION_KEY = "key";
        public static final  String STATION_DOWN_NUM = "downNum";

        public static final  String BUS_BEGIN_TIME = "beginTime";
        public static final  String BUS_END_TIME = "endTime";

        public static final  String BUS_BUS_NUMBER = "busNumber";
        public static final  String BUS_LICENSE_NUMBER = "licenseNumber";
        public static final  String BUS_DRIVER_NAME = "driverName";


        public static final String USER_PHONE = "phone";
        public static final String USER_BALANCE = "balance";


    }

}
