package cn.bmkp.bus.app.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.WilddogError;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.BusLine;
import cn.bmkp.bus.app.models.BusRun;
import cn.bmkp.bus.app.models.Station;
import cn.bmkp.bus.app.models.StationPrice;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by fm on 16/3/19.
 */
public class BuyTicketActivity extends BaseActivity {

    @Bind(R.id.tv_my_wallet_money)
    TextView tvMyWalletMoney;
    @Bind(R.id.rl_buy_ticket_pay_use_wallet)
    RelativeLayout rlBuyTicketPayUseWallet;
    @Bind(R.id.ll_my_ticket_warning)
    LinearLayout llMyTicketWarning;
    private String spinnerDateFormat = "yyyy-MM-dd";

    private BusLine busLine;

    private String lineKey;
    private int startStation;
    private int endStation;
    private int ticketCount;
    private BusRun busRun;
    //起点适配器
    private ArrayAdapter start_adapter;
    //终点适配器
    private ArrayAdapter end_adapter;
    //余票适配器
    private ArrayAdapter ticket_adapter;

    private int datePosition = 0;

    //日期下拉选项的起始是从今天还是明天
    private int firstDay = 0;

    @Bind(R.id.ib_function_left)
    Button ibFunctionLeft;
    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.iv_title_my_ticket)
    ImageView ivTitleMyTicket;
    @Bind(R.id.rl_title)
    RelativeLayout rlTitle;
    @Bind(R.id.ll_buy_ticket_title)
    LinearLayout llBuyTicketTitle;
    @Bind(R.id.tv_buy_ticket_start)
    TextView tvBuyTicketStart;
    @Bind(R.id.tv_buy_ticket_end)
    TextView tvBuyTicketEnd;
    @Bind(R.id.ll_station)
    LinearLayout llStation;
    @Bind(R.id.rl_bus_line_head)
    RelativeLayout rlBusLineHead;
    @Bind(R.id.spinner_date)
    Spinner spinnerDate;
    @Bind(R.id.ll_date)
    LinearLayout llDate;
    @Bind(R.id.spinner_abroad_station)
    Spinner spinnerAbroadStation;
    @Bind(R.id.ll_aboard_station)
    LinearLayout llAboardStation;
    @Bind(R.id.spinner_debarkation_station)
    Spinner spinnerDebarkationStation;
    @Bind(R.id.ll_debarkation_station)
    LinearLayout llDebarkationStation;
    @Bind(R.id.tv_choose_num)
    TextView tvChooseNum;
    @Bind(R.id.spinner_ticket_num)
    Spinner spinnerTicketNum;
    @Bind(R.id.rl_choose_num)
    RelativeLayout rlChooseNum;
    @Bind(R.id.tv_buy_ticket_tip_time)
    TextView tvBuyTicketTipTime;
    @Bind(R.id.tv_buy_ticket_tip_address)
    TextView tvBuyTicketTipAddress;
    @Bind(R.id.ll_abroad_time)
    LinearLayout llAbroadTime;
    @Bind(R.id.rl_buy_ticket_info)
    RelativeLayout rlBuyTicketInfo;
    @Bind(R.id.tv_pay_num)
    TextView tvPayNum;
    @Bind(R.id.rl_buy_ticket_pay_info)
    RelativeLayout rlBuyTicketPayInfo;
    @Bind(R.id.tv_buy_ticket_choose_tip)
    TextView tvBuyTicketChooseTip;
    @Bind(R.id.ll_alipay)
    LinearLayout llAlipay;
    @Bind(R.id.ll_wechat)
    LinearLayout llWechat;
    @Bind(R.id.rb_alipay)
    RadioButton rbAlipay;
    @Bind(R.id.rb_wechat)
    RadioButton rbWechat;
    @Bind(R.id.rg_payment)
    RadioGroup rgPayment;
    @Bind(R.id.rl_payment)
    RelativeLayout rlPayment;
    @Bind(R.id.btn_pay_confirm)
    Button btnPayConfirm;
    @Bind(R.id.rl_my_ticket_function)
    LinearLayout rlMyTicketFunction;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        this.finish();
    }

    //购票、退票规则
    @OnClick(R.id.ll_my_ticket_warning)
    void checkRules(View view) {
        Intent intent = new Intent(BuyTicketActivity.this, WebActivity.class);
        startActivity(intent);
    }

    //查看已购买车票
    @OnClick(R.id.iv_title_my_ticket)
    void checkMyTickets(View view) {
        if (getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(BuyTicketActivity.this, MyTicketsActivity.class);
            startActivity(intent);
        } else {
            showAlertDialog(getString(R.string.text_not_login_please_login), "", getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(BuyTicketActivity.this, LoginActivity.class);
                    intent.putExtra("from", BuyTicketActivity.class.getSimpleName());
                    startActivity(intent);
                }
            }, getString(R.string.text_cancel), null, this);
        }
    }

    @OnClick(R.id.btn_pay_confirm)
    void goToPayment(View view) {

        // 为未来其他选择票数形式做预留
        if (ticketCount > getTicketLeft()) {
            Toast.makeText(this, R.string.text_remain_ticket_not_enough, Toast.LENGTH_SHORT).show();
            return;
        }

//        WilddogApi.getInstance().buyTicket(lineKey, getPreferenceHelper().getUserName(), busRun, getYearMonthDay(datePosition + firstDay),
//                startStation, endStation, ticketCount, new Wilddog.CompletionListener() {
//                    @Override
//                    public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
//                        Toast.makeText(BuyTicketActivity.this, "买票成功", Toast.LENGTH_SHORT).show();
//
//                        Bundle bundle = new Bundle();
//                        bundle.putSerializable("busRunObj", busRun);
//                        Intent intent = new Intent(BuyTicketActivity.this, TicketPaySuccessActivity.class);
//                        intent.putExtras(bundle);
//                        intent.putExtra("start", startStation);
//                        intent.putExtra("end", endStation);
//                        intent.putExtra("ticketCount", ticketCount);
//                        intent.putExtra("datePosition", datePosition);
//                        BuyTicketActivity.this.startActivity(intent);
//
//                        BuyTicketActivity.this.finish();
//                    }
//                });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket);
        ButterKnife.bind(this);

        // TODO 判断当前发车时间是否已过，暂时先从明天开始，今天为0
        firstDay = 1;

        busLine = (BusLine) getIntent().getSerializableExtra("busLineObj");

        if (busLine == null) {
            return;
        }
        startStation = 0;
        endStation = busLine.getStations().size() - 1;
        ticketCount = 1;

        initData(busLine);


        lineKey = busLine.getLineKey();

        WilddogApi.getInstance().getBusRemainTicket(lineKey, getYearMonthDay(firstDay), "", "", new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                busRun = ParseUtil.parseBusRun(dataSnapshot);

                Log.d("zhangn", "===>" + busRun.getSeatsLeft());

                refreshTicket();

                spinnerDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        WilddogApi.getInstance().removeBusRemainTicketListener(lineKey, getYearMonthDay(position + firstDay), "", "", myTicketList);
                        WilddogApi.getInstance().setBusRemainTicketListener(lineKey, getYearMonthDay(position + firstDay), "", "", myTicketList);
                        datePosition = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerAbroadStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        startStation = getPosition(parent.getSelectedItem().toString());
                        //刷新票数和终点的选择范围
                        refreshTicket();
                        refreshEndSpinner();
                        refreshPayCount();

                        tvBuyTicketTipTime.setText(busLine.getStations().get(startStation).getTime());
                        tvBuyTicketTipAddress.setText(busLine.getStations().get(startStation).getDetail());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerDebarkationStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        endStation = getPosition(parent.getSelectedItem().toString());
                        //刷新票数和起点的选择范围
                        refreshStartSpinner();
                        refreshTicket();
                        refreshPayCount();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerTicketNum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ticketCount = Integer.parseInt(parent.getSelectedItem().toString());
                        //刷新总价格
                        refreshPayCount();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {

            }
        });

        WilddogApi.getInstance().setBusRemainTicketListener(lineKey, getYearMonthDay(datePosition + firstDay), "", "", myTicketList);
    }

    private void initData(BusLine busLine) {
        String startName = busLine.getStations().get(startStation).getName();
        String endName = busLine.getStations().get(endStation).getName();

        tvBuyTicketStart.setText(startName);
        tvBuyTicketEnd.setText(endName);

        //日期Spinner
        SimpleDateFormat df = new SimpleDateFormat(spinnerDateFormat);
        //数据
        List<String> date_list = new ArrayList();
        Date date;
        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.DATE, firstDay);
        for (int i = 0; i < 8; i++) {
            date = cal.getTime();
            cal.add(Calendar.DATE, 1);

            date_list.add(df.format(date));
        }
        //适配器
        ArrayAdapter arr_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, date_list);
        //设置样式
        arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerDate.setAdapter(arr_adapter);

        //起点Spinner数据
        List<String> station_list = new ArrayList();
        for (int k = 0; k < busLine.getStations().size() - 1; k++) {
            Station stat = busLine.getStations().get(k);
            station_list.add(stat.getName());
        }
        start_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, station_list);
        //设置样式
        start_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerAbroadStation.setAdapter(start_adapter);

        //终点Spinner数据
        List<String> end_station_list = new ArrayList();
        for (int j = 1; j < busLine.getStations().size(); j++) {
            Station sta = busLine.getStations().get(j);
            end_station_list.add(sta.getName());
        }
        end_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, end_station_list);
        //设置样式
        end_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerDebarkationStation.setAdapter(end_adapter);
        spinnerDebarkationStation.setSelection(end_station_list.size() - 1, true);

        //刷新票数数据
        List<String> num_list = new ArrayList();
        num_list.add(String.valueOf(ticketCount));
        ticket_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, num_list);
        //设置样式
        ticket_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerTicketNum.setAdapter(ticket_adapter);

        tvBuyTicketTipTime.setText(busLine.getBeginTime());
        tvBuyTicketTipAddress.setText(busLine.getStations().get(startStation).getDetail());

        String price = getPrice(busLine.getStations(), busLine.getStationPrices());
        tvPayNum.setText(price + "元");

    }


    private void refreshTicket() {
        int ticketLeft = getTicketLeft();

        refreshTicketSpinner(ticketLeft);
    }

    private void refreshStartSpinner() {
        start_adapter.clear();
        for (int i = 0; i < endStation; i++) {
            start_adapter.add(busLine.getStations().get(i).getName());
        }
    }

    private void refreshEndSpinner() {
        end_adapter.clear();
        for (int i = startStation + 1; i < busLine.getStations().size(); i++) {
            end_adapter.add(busLine.getStations().get(i).getName());
        }
    }

    private void refreshTicketSpinner(int ticket) {
        //刷新票数数据
        ticket_adapter.clear();
        for (int i = 1; i <= ticket; i++) {
            ticket_adapter.add(String.valueOf(i));
        }

        if (ticket == 0) {
            ticket_adapter.add(String.valueOf(0));
        }
    }

    private int getTicketLeft() {
        if (endStation > startStation) {
            int[] tickets = new int[endStation - startStation];
            if (busRun.getSeatsLeft().size() >= tickets.length) {
                for (int i = startStation; i < endStation; i++) {
                    tickets[i - startStation] = Integer.parseInt(busRun.getSeatsLeft().get(i));
                }

                Arrays.sort(tickets);

                return tickets[0];
            }
        }
        return 0;
    }

    private int getPosition(String name) {
        for (int i = 0; i < busLine.getStations().size(); i++) {
            if (name.equals(busLine.getStations().get(i).getName())) {
                return i;
            }
        }
        return 0;
    }

    private String getPrice(List<Station> stations, List<StationPrice> priceList) {
        if (stations.size() > endStation) {
            String start = stations.get(startStation).getName();
            String end = stations.get(endStation).getName();
            for (StationPrice stationPrice : priceList) {
                if (start.equals(stationPrice.getStartName()) && end.equals(stationPrice.getEndName())) {
                    return stationPrice.getPrice();
                }
            }
        }

        return "0";
    }

    ValueEventListener myTicketList = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            busRun = ParseUtil.parseBusRun(dataSnapshot);

            Log.d("zhangn", "===>" + busRun.getSeatsLeft());

            refreshTicket();
            refreshPayCount();
        }

        @Override
        public void onCancelled(WilddogError wilddogError) {

        }
    };

    //获取BusRun dateKey
    private String getYearMonthDay(int after) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, after);
        Date date = cal.getTime();
        //日期Spinner
        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);
        return df.format(date);
    }

    private void refreshPayCount() {

        float price = Float.parseFloat(getPrice(busRun.getStations(), busRun.getStationPrices()));
        String total = new DecimalFormat("0.0").format(price * ticketCount);
        tvPayNum.setText(total + getString(R.string.text_yuan));

    }
}
