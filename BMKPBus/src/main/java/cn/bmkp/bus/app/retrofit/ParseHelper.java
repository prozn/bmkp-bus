package cn.bmkp.bus.app.retrofit;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import cn.bmkp.bus.app.BmkpBusApplication;
import cn.bmkp.bus.app.db.DBHelper;


public class ParseHelper {

    private Context context;

    DecimalFormat df0 = new DecimalFormat("0");
    DecimalFormat df0_0 = new DecimalFormat("0.0");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private DBHelper dbHelper;
    private ServerApi serverApi;

    public ParseHelper(Context context) {
        this.context = context;

        dbHelper = ((BmkpBusApplication) context.getApplicationContext()).getDBHelper();
        serverApi = ((BmkpBusApplication) context.getApplicationContext()).getServerApi();
    }

    public DBHelper getDbHelper() {
        return ((BmkpBusApplication) context.getApplicationContext()).getDBHelper();
    }

    //解析验证码
    public String getVerifyId(JSONObject data) throws JSONException {
        if (data == null) {
            return "";
        }
        return data.getString("verify_id");
    }

    //解析登录后的数据
    public String parseToken(JSONObject data) throws JSONException {
        if (data == null) {
            return "";
        }
        return data.getString("token");
    }
}
