package cn.bmkp.bus.app.retrofit;

import java.util.HashMap;

import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.util.LogHelper;
import retrofit.Callback;
import retrofit.client.Response;

/**
 * Created by pandeng on 15/9/10.
 */
public class ServerApi {

    BaseApi baseApi;

    private String client_type = "android";
    public ServerApi(BaseApi api) {
        baseApi = api;
    }

    //获取验证码短信
    public void getSms(String phone, Callback<Response> response) {
        HashMap<String, String> map = new HashMap();
        map.put(Const.PHONE, phone);
        baseApi.getSms(map, response);
    }


    //快速登录界面登录
    public void verifyLogin(String phone, String verifyId,
                            String verifyCode, String deviceToken, Callback<Response> response) {
        HashMap<String, String> map = new HashMap();
        map.put(Const.PHONE, phone);
        map.put(Const.VERIFY_ID, verifyId);
        map.put(Const.VERIFY_CODE, verifyCode);
        map.put(Const.CLIENT_TYPE, Const.DEVICE_TYPE_ANDROID);
        map.put(Const.CLIENT_TOKEN, deviceToken);
        baseApi.verifyLogin(map, response);
        LogHelper.info("====verifyLogin", String.valueOf("hashCode:" + response.hashCode()) + ",start:" + System.nanoTime());
    }


    /**
     * 刷新Token
     * @param token  已经获取到的token
     * @param response
     */
    public void refreshToken(String token, Callback<Response> response) {

        baseApi.refreshToken(token, new HashMap<String, String>(), response);
    }
}
