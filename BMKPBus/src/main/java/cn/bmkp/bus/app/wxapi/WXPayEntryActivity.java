package cn.bmkp.bus.app.wxapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.BmkpBusApplication;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.models.BusLine;
import cn.bmkp.bus.app.models.BusRun;
import cn.bmkp.bus.app.models.Station;
import cn.bmkp.bus.app.models.StationPrice;
import cn.bmkp.bus.app.retrofit.PreferenceHelper;
import cn.bmkp.bus.app.ui.LoginActivity;
import cn.bmkp.bus.app.ui.MyTicketsActivity;
import cn.bmkp.bus.app.ui.TicketPaySuccessActivity;
import cn.bmkp.bus.app.ui.WebActivity;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.util.PayConst;
import cn.bmkp.bus.app.wilddog.WilddogApi;
import cn.bmkp.pay.Const;
import cn.bmkp.pay.alipay.ALiPay;
import cn.bmkp.pay.alipay.listener.ALiPayListener;
import cn.bmkp.pay.wxpay.WXPayActivity;
import cn.bmkp.pay.wxpay.listener.WXPayListener;

/**
 * Created by fm on 16/3/19.
 */
public class WXPayEntryActivity extends WXPayActivity implements IWXAPIEventHandler {


    private String spinnerDateFormat = "yyyy-MM-dd";

    private BusLine busLine;

    private String lineKey;
    private int startStation;
    private int endStation;

    //实时的票数spinner中的值
    private int ticketCount;
    //最后的买票数量
    private int lastTicketCount;

    private BusRun busRun;
    //起点适配器
    private ArrayAdapter start_adapter;
    //终点适配器
    private ArrayAdapter end_adapter;
    //余票适配器
    private ArrayAdapter ticket_adapter;

    private int datePosition = 0;

    //日期下拉选项的起始是从今天还是明天
    private int firstDay = 0;

    //账户余额
    private float balance;

    @Bind(R.id.tv_buy_ticket_start)
    TextView tvBuyTicketStart;
    @Bind(R.id.tv_buy_ticket_end)
    TextView tvBuyTicketEnd;
    @Bind(R.id.spinner_date)
    Spinner spinnerDate;
    @Bind(R.id.spinner_abroad_station)
    Spinner spinnerAbroadStation;
    @Bind(R.id.spinner_debarkation_station)
    Spinner spinnerDebarkationStation;
    @Bind(R.id.spinner_ticket_num)
    Spinner spinnerTicketNum;
    @Bind(R.id.tv_buy_ticket_tip_time)
    TextView tvBuyTicketTipTime;
    @Bind(R.id.tv_buy_ticket_tip_address)
    TextView tvBuyTicketTipAddress;
    @Bind(R.id.tv_pay_num)
    TextView tvPayNum;

    @Bind(R.id.rg_payment)
    RadioGroup rgPayment;


    @Bind(R.id.tv_my_wallet_money)
    TextView tvMyWalletMoney;

    @OnClick(R.id.ib_function_left)
    public void back(View v){
        this.finish();
    }

    private int PAYMENT_TYPE = 1;
    private final int PAYMENT_ALI = 1;
    private final int PAYMENT_WECHAT = 2;
    private IWXAPI iwxapi;

    private ALiPay aLiPay;
    private String orderId = "";

    public PreferenceHelper getPreferenceHelper() {
        return ((BmkpBusApplication) getApplication()).getPreferenceHelper();
    }

    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;

    public void showAlertDialog(String title, String msg,
                                String positive, DialogInterface.OnClickListener positiveListener,
                                String negative, DialogInterface.OnClickListener negativeListener,
                                Context context) {
        if(alertDialog == null || !alertDialog.isShowing()){
            alertDialog = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .show();
        }
    }

    public void showProgressDialog(String detail) {
        if (isFinishing())
            return;

        dismissDialog();
        progressDialog = ProgressDialog.show(WXPayEntryActivity.this, "", detail, true, false);
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    //购票、退票规则
    @OnClick(R.id.ll_my_ticket_warning)
    void checkRules(View view) {
        Intent intent = new Intent(WXPayEntryActivity.this, WebActivity.class);
        startActivity(intent);
    }

    //查看已购买车票
    @OnClick(R.id.iv_title_my_ticket)
    void checkMyTickets(View view) {
        if (getPreferenceHelper().getLoginStatus()) {
            Intent intent = new Intent(WXPayEntryActivity.this, MyTicketsActivity.class);
            startActivity(intent);
        } else {
            showAlertDialog("您未登录！登录？", "", "确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(WXPayEntryActivity.this, LoginActivity.class);
                    intent.putExtra("from", WXPayEntryActivity.class.getSimpleName());
                    startActivity(intent);
                }
            }, "取消", null, this);
        }
    }


    /**
     * 获取用户的手机号
     * @return
     */
    private String getUserPhone(){

        return getPreferenceHelper().getUserName();
    }

    /**
     * 获取订单号 （格式：手机号：UUID）
     * @return
     */

    private String getOrderNo(){

        return orderId;

    }

    private String uuid;

    @OnClick(R.id.btn_pay_confirm)
    void goToPayment(View view) {

        if(!AndyUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, "网络连接不可用", Toast.LENGTH_SHORT).show();
            return;
        }

        // 为未来其他选择票数形式做预留
        if (getTicketLeft() < 1 || getTicketLeft() < ticketCount) {
            Toast.makeText(this, "余票不足", Toast.LENGTH_SHORT).show();
            return;
        }

        lastTicketCount = ticketCount;

        float price = Float.parseFloat(getPrice(busRun.getStations(), busRun.getStationPrices()));
        orderId = getUserPhone()+"-"+System.currentTimeMillis();

        if (balance >= price * ticketCount) {

            WilddogApi.getInstance().modifyOwnerBalance(getPreferenceHelper().getUserName(), price * ticketCount, new Wilddog.CompletionListener() {
                @Override
                public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                    if (wilddogError != null) {
                        Toast.makeText(WXPayEntryActivity.this, "Error :" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                    } else {
                        buyTicket();
                    }
                }
            });

        } else {

            String wxTotal = new DecimalFormat("0").format(price * ticketCount * 100);
            String aliTotal = new DecimalFormat("0.0").format(price * ticketCount);

            //TODO 为了方便测试先暂时写测试价格
            switch (PAYMENT_TYPE) {
                case PAYMENT_ALI:
                    //元为单位
                    aLiPay.pay("斑马快跑","车票",orderId, aliTotal);
                    break;

                case PAYMENT_WECHAT:
                    //分为单位
                    pay("斑马快跑", orderId, wxTotal);
            break;
        }

        }
    }


    /**
     * 付款成功后，更新服务端数据
     */
    private void buyTicket() {

        showProgressDialog("生成车票中，请耐心等待…");

        WilddogApi.getInstance().buyTicket(lineKey, getPreferenceHelper().getUserName(), getOrderNo(),
                busRun, getYearMonthDay(datePosition + firstDay),
                startStation, endStation, ticketCount, new Wilddog.CompletionListener() {
                    @Override
                    public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
//                        Toast.makeText(WXPayEntryActivity.this, "买票成功", Toast.LENGTH_SHORT).show();

                        if(wilddogError == null) {

                            WilddogApi.getInstance().insertUserTicketToTicketPool(getYearMonthDay(datePosition + firstDay),
                                    lineKey, getPreferenceHelper().getUserName(), getOrderNo(), new Wilddog.CompletionListener() {
                                        @Override
                                        public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                                            if (wilddogError == null) {
                                            } else {

                                                Toast.makeText(WXPayEntryActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                            WilddogApi.getInstance().addPeopleDown(lineKey, ticketCount, getYearMonthDay(datePosition + firstDay),
                                    endStation, new Wilddog.CompletionListener() {
                                        @Override
                                        public void onComplete(WilddogError wilddogError, Wilddog wilddog) {

                                            if (wilddogError == null) {
//                                Toast.makeText(WXPayEntryActivity.this, "增加下车人数成功", Toast.LENGTH_SHORT).show();
                                            } else {

                                                Toast.makeText(WXPayEntryActivity.this, "Error ：" + wilddogError.getCode(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                            Intent intent = new Intent(WXPayEntryActivity.this, TicketPaySuccessActivity.class);
                            intent.putExtra("ticketKey", getOrderNo());
                            intent.putExtra("from", WXPayEntryActivity.class.getSimpleName());
                            WXPayEntryActivity.this.startActivity(intent);

                            WXPayEntryActivity.this.finish();

                        } else {
                            dismissDialog();
                            Toast.makeText(WXPayEntryActivity.this, "Error ：" + wilddogError.getCode() , Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }


    @BindString(R.string.payment_title)
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket);
        ButterKnife.bind(this);

        initView();
        initWXPay();
        initALipay();

        busLine = (BusLine) getIntent().getSerializableExtra("busLineObj");

        if (busLine == null) {
            return;
        }

        // 判断当前发车时间是否已过，暂时先从明天开始，今天为0
        String sBegin = busLine.getBeginTime();
        int hour = Integer.parseInt(sBegin.substring(0, sBegin.indexOf(":")));
        int minute = Integer.parseInt(sBegin.substring(sBegin.indexOf(":") + 1, sBegin.length()));
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Calendar.HOUR_OF_DAY, hour);
        beginTime.set(Calendar.MINUTE, minute);
        Calendar nowTime = Calendar.getInstance();

        if (beginTime.getTimeInMillis() - nowTime.getTimeInMillis() > cn.bmkp.bus.app.common.Const.TIME_STOP_BUY_TICKET) {
            firstDay = 0;
        } else {
            firstDay = 1;
        }

        //主界面选择的日期
        String sDate = getIntent().getStringExtra(cn.bmkp.bus.app.common.Const.BUS_LINE_SELECTED_DATE);
        SimpleDateFormat sdf = new SimpleDateFormat(cn.bmkp.bus.app.common.Const.BUS_RUN_DATE_KEY_FORMAT);
        Date selectDate;
        try {
            selectDate = sdf.parse(sDate);
        } catch (ParseException e) {
            selectDate = new Date();
            e.printStackTrace();
        }

        startStation = 0;
        endStation = busLine.getStations().size() - 1;
        ticketCount = 0;

        Calendar ca = Calendar.getInstance();
        ca.setTime(selectDate);
        initData(busLine, ca);

        lineKey = busLine.getLineKey();

        WilddogApi.getInstance().getBusRemainTicket(lineKey, sDate, "", "", new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                busRun = ParseUtil.parseBusRun(dataSnapshot);

                Log.d("zhangn", "===>" + busRun.getSeatsLeft());

                refreshTicket();

                WilddogApi.getInstance().getOwnerInfo(getPreferenceHelper().getUserName(), new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        balance = ParseUtil.parseOwnerBalance(dataSnapshot);
                        refreshPayCount();
                    }

                    @Override
                    public void onCancelled(WilddogError wilddogError) {

                    }
                });

                spinnerDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        WilddogApi.getInstance().removeBusRemainTicketListener(lineKey, getYearMonthDay(position + firstDay), "", "", myTicketList);
                        WilddogApi.getInstance().setBusRemainTicketListener(lineKey, getYearMonthDay(position + firstDay), "", "", myTicketList);
                        datePosition = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerAbroadStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        startStation = getPosition(parent.getSelectedItem().toString());
                        //刷新票数和终点的选择范围
                        refreshTicket();
                        refreshEndSpinner();
                        refreshPayCount();

                        tvBuyTicketTipTime.setText(busLine.getStations().get(startStation).getTime());
                        tvBuyTicketTipAddress.setText(busLine.getStations().get(startStation).getDetail());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerDebarkationStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        endStation = getPosition(parent.getSelectedItem().toString());
                        //刷新票数和起点的选择范围
                        refreshStartSpinner();
                        refreshTicket();
                        refreshPayCount();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinnerTicketNum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ticketCount = Integer.parseInt(parent.getSelectedItem().toString());
                        //刷新总价格
                        refreshPayCount();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onCancelled(WilddogError wilddogError) {
                Toast.makeText(WXPayEntryActivity.this, "Error ：" + wilddogError.getCode() , Toast.LENGTH_SHORT).show();
            }
        });

        WilddogApi.getInstance().setBusRemainTicketListener(lineKey, getYearMonthDay(datePosition + firstDay), "", "", myTicketList);
    }


    private void initData(BusLine busLine, Calendar selectedDate) {
        String startName = busLine.getStations().get(startStation).getName();
        String endName = busLine.getStations().get(endStation).getName();

        tvBuyTicketStart.setText(startName);
        tvBuyTicketEnd.setText(endName);

        //日期Spinner
        SimpleDateFormat df = new SimpleDateFormat(spinnerDateFormat);
        //数据
        List<String> date_list = new ArrayList();
        Date date;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, firstDay);
        for (int i = 0; i < 8; i++) {
            date = cal.getTime();
            date_list.add(df.format(date));

            if (cal.get(Calendar.MONTH) == selectedDate.get(Calendar.MONTH)
                    && cal.get(Calendar.DAY_OF_MONTH) == selectedDate.get(Calendar.DAY_OF_MONTH)) {
                datePosition = i;
            }

            cal.add(Calendar.DATE, 1);

        }
        //适配器
        ArrayAdapter arr_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, date_list);
        //设置样式
        arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerDate.setAdapter(arr_adapter);
        spinnerDate.setSelection(datePosition, true);

        //起点Spinner数据
        List<String> station_list = new ArrayList();
        for (int k = 0; k < busLine.getStations().size() - 1; k++) {
            Station stat = busLine.getStations().get(k);
            station_list.add(stat.getName());
        }
        start_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, station_list);
        //设置样式
        start_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerAbroadStation.setAdapter(start_adapter);

        //终点Spinner数据
        List<String> end_station_list = new ArrayList();
        for (int j = 1; j < busLine.getStations().size(); j++) {
            Station sta = busLine.getStations().get(j);
            end_station_list.add(sta.getName());
        }
        end_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, end_station_list);
        //设置样式
        end_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerDebarkationStation.setAdapter(end_adapter);
        spinnerDebarkationStation.setSelection(end_station_list.size() -1, true);

        //刷新票数数据
        List<String> num_list = new ArrayList();
        num_list.add(String.valueOf(ticketCount));
        ticket_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, num_list);
        //设置样式
        ticket_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinnerTicketNum.setAdapter(ticket_adapter);

        tvBuyTicketTipTime.setText(busLine.getBeginTime());
        tvBuyTicketTipAddress.setText(busLine.getStations().get(startStation).getDetail());

        String price = getPrice(busLine.getStations(), busLine.getStationPrices());
        tvPayNum.setText(price + "元");

    }


    private void refreshTicket() {
        int ticketLeft = getTicketLeft();

        refreshTicketSpinner(ticketLeft);

        if(ticket_adapter.getCount() <= spinnerTicketNum.getSelectedItemPosition()) {

            ticketCount = Integer.parseInt(ticket_adapter.getItem(ticket_adapter.getCount() -1).toString());
        }else {

            ticketCount = Integer.parseInt(spinnerTicketNum.getSelectedItem().toString());
        }
    }

    private void refreshStartSpinner() {
        start_adapter.clear();
        for (int i = 0; i < endStation; i++) {
            start_adapter.add(busLine.getStations().get(i).getName());
        }
    }

    private void refreshEndSpinner() {
        end_adapter.clear();
        for (int i = startStation + 1; i < busLine.getStations().size(); i++) {
            end_adapter.add(busLine.getStations().get(i).getName());
        }
    }

    private void refreshTicketSpinner(int ticket) {
        //刷新票数数据
        ticket_adapter.clear();
        for (int i = 1; i <= ticket; i++) {
            ticket_adapter.add(String.valueOf(i));
        }

        if (ticket == 0) {
            ticket_adapter.add(String.valueOf(0));
        }
    }

    private int getTicketLeft() {
        if (endStation > startStation) {
            int[] tickets = new int[endStation - startStation];
            if (busRun != null && busRun.getSeatsLeft() != null
                    && busRun.getSeatsLeft().size() >= tickets.length) {
                for (int i = startStation; i < endStation; i++) {
                    tickets[i - startStation] = Integer.parseInt(busRun.getSeatsLeft().get(i));
                }

                Arrays.sort(tickets);

                return tickets[0];
            }
        }
        return 0;
    }

    private int getPosition(String name) {
        for (int i = 0; i < busLine.getStations().size(); i++) {
            if (name.equals(busLine.getStations().get(i).getName())) {
                return i;
            }
        }
        return 0;
    }

    private String getPrice(List<Station> stations, List<StationPrice> priceList) {
        if (stations.size() > endStation) {
            String start = stations.get(startStation).getName();
            String end = stations.get(endStation).getName();
            for (StationPrice stationPrice : priceList) {
                if (start.equals(stationPrice.getStartName()) && end.equals(stationPrice.getEndName())) {
                    return stationPrice.getPrice();
                }
            }
        }

        return "0";
    }

    ValueEventListener myTicketList = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            busRun = ParseUtil.parseBusRun(dataSnapshot);

            Log.d("zhangn", "===>" + busRun.getSeatsLeft());

            refreshTicket();
            refreshPayCount();
        }

        @Override
        public void onCancelled(WilddogError wilddogError) {

        }
    };

    //获取BusRun dateKey
    private String getYearMonthDay(int after) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, after);
        Date date = cal.getTime();
        //日期Spinner
        SimpleDateFormat df = new SimpleDateFormat(cn.bmkp.bus.app.common.Const.BUS_RUN_DATE_KEY_FORMAT);
        return df.format(date);
    }

    private void refreshPayCount() {

        float price = Float.parseFloat(getPrice(busRun.getStations(), busRun.getStationPrices()));
        String total = new DecimalFormat("0.0").format(price * ticketCount);
        tvPayNum.setText(total + "元");

        if(balance >= price * ticketCount) {
            tvMyWalletMoney.setText("-" + total + "元");
        }
        else {
            tvMyWalletMoney.setText("余额不足");
        }

    }

    private void initView() {
        rgPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_alipay) {
                    PAYMENT_TYPE = PAYMENT_ALI;
                } else if (checkedId == R.id.rb_wechat) {
                    PAYMENT_TYPE = PAYMENT_WECHAT;
                }
            }
        });
    }

    private void initWXPay() {
        iwxapi = WXAPIFactory.createWXAPI(this, null);
        iwxapi.registerApp(PayConst.WeChat.APP_ID);
        iwxapi.handleIntent(getIntent(), this);
        setMsgApi(iwxapi);

        setPayListener(new WXPayListener() {
            @Override
            public void payCallBack(int errorCode, String errorMsg) {
                //支付的回调在本界面生成，所以这里只是处理创建支付的错误
                if (errorCode == Const.Payment.PAY_FAIL) {
                    //创建支付失败

                }
            }
        });
    }


    private void initALipay(){
        // 支付成功以后应该转到支付成功页面
        aLiPay = new ALiPay();
        aLiPay.initALiPay(this, new ALiPayListener() {

            @Override
            public void payCallBack(int errorCode, String errorMsg) {
                Intent data = new Intent();
                switch (errorCode) {
                    case Const.Payment.PAY_SUCCESS:
                        //支付成功
                        AndyUtils.showToast(getString(R.string.text_ali_pay_success), WXPayEntryActivity.this);
                        buyTicket();
                        finish();
                        break;
                    case Const.Payment.PAY_SUCCESS_ERROR:
                        //支付成功但有问题
                        AndyUtils.showToast("支付宝支付成功！", WXPayEntryActivity.this);
                        buyTicket();
                        finish();
                        break;
                    case Const.Payment.PAY_FAIL:
                        //支付失败
                        AndyUtils.showToast("支付宝支付失败！", WXPayEntryActivity.this);
                        break;
                }
            }
        });
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        iwxapi.handleIntent(intent, this);
        setMsgApi(iwxapi);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {
        if (baseResp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            switch (baseResp.errCode) {
                case 0:
                    //微信支付成功
                    AndyUtils.showToast(getString(R.string.text_wexin_pay_success), WXPayEntryActivity.this);
                    buyTicket();
                    break;
                case -1:
                    //微信支付失败
                    AndyUtils.showToast(getString(R.string.text_wexin_pay_fail), WXPayEntryActivity.this);
                    break;
                case -2:
                    //微信支付取消
                    AndyUtils.showToast(getString(R.string.text_wexin_pay_cancel), WXPayEntryActivity.this);
                    break;
                default:
                    AndyUtils.showToast(getString(R.string.text_wexin_pay_fail), WXPayEntryActivity.this);
                    //微信支付失败
                    break;
            }
        }
    }
}
