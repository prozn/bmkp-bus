package cn.bmkp.bus.app.common;

/**
 * Created by Micheal on 2016/3/22.
 */
public class Const {

    public static final String PREF_NAME = "BMKP-BUS";
    public static final String DEVICE_TYPE_ANDROID = "android";


    //ServerApi
    public static final String PHONE = "phone";
    public static final String VERIFY_ID = "verify_id";
    public static final String VERIFY_CODE = "verify_code";
    public static final String CLIENT_TYPE = "client_type";
    public static final String CLIENT_TOKEN = "client_token";


    //BusRun DateKey Format
    public static final String BUS_RUN_DATE_KEY_FORMAT = "yyyyMMdd";

    //Activity Intent StringExtra Name
    public static final String BUS_LINE_SELECTED_DATE = "selectedDate";

    //Activity Intent StringExtra Activity-to
    public static final String ACTIVITY_TO = "activity_to";

    //ticket状态

    public static final int TICKET_STATE_NOT_PAY      = 0; //未支付
    public static final int TICKET_STATE_PAID         = 1; //已支付
    public static final int TICKET_STATE_CHECKED      = 2; //已检票
    public static final int TICKET_STATE_WAIT_REFUND = -1; //待退款
    public static final int TICKET_STATE_REFUNDED     = -2; //已退款


    // ms
    public static final int TIME_STOP_BUY_TICKET = 10 * 60 *1000;
    public static final int TIME_STOP_REFUND_TICKET = 30 * 60 *1000;
}
