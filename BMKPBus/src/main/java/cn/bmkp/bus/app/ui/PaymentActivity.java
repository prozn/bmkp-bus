package cn.bmkp.bus.app.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;

/**
 * Created by fm on 16/3/19.
 */
public class PaymentActivity extends AppCompatActivity {

    @Bind(R.id.rl_toolbar)
    Toolbar toolbar;
    @Bind(R.id.ll_station)
    LinearLayout llStation;
    @Bind(R.id.tv_depart_time)
    TextView tvDepartTime;
    @Bind(R.id.rl_bus_line_head)
    RelativeLayout rlBusLineHead;
    @Bind(R.id.tv_date)
    TextView tvDate;
    @Bind(R.id.ll_date)
    LinearLayout llDate;
    @Bind(R.id.tv_abroad_station)
    TextView tvAbroadStation;
    @Bind(R.id.ll_aboard_station)
    LinearLayout llAboardStation;
    @Bind(R.id.tv_debarkation_station)
    TextView tvDebarkationStation;
    @Bind(R.id.ll_debarkation_station)
    LinearLayout llDebarkationStation;
    @Bind(R.id.ll_abroad_time)
    LinearLayout llAbroadTime;
    @Bind(R.id.tv_remain_seat_num)
    TextView tvRemainSeatNum;
    @Bind(R.id.tv_buy_ticket_num_tip)
    TextView tvBuyTicketNumTip;
    @Bind(R.id.tv_pay_num)
    TextView tvPayNum;
    @Bind(R.id.ll_alipay)
    LinearLayout llAlipay;
    @Bind(R.id.ll_wechat)
    LinearLayout llWechat;
    @Bind(R.id.rb_alipay)
    RadioButton rbAlipay;
    @Bind(R.id.rb_wechat)
    RadioButton rbWechat;
    @Bind(R.id.rg_payment)
    RadioGroup rgPayment;
    @Bind(R.id.rl_payment)
    RelativeLayout rlPayment;
    @Bind(R.id.tv_payment)
    TextView tvPayment;

    @BindString(R.string.payment_title)
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
    }
}
