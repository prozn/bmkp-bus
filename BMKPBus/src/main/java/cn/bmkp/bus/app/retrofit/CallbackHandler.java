package cn.bmkp.bus.app.retrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cn.bmkp.bus.app.util.LogHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by pandeng on 15/9/8.
 */
public class CallbackHandler implements Callback<Response> {

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_ERROR_CODE = "error_code";

    public void onSuccess(JSONObject data) {

    }

    public void onError(int code, JSONObject data) {

    }

    public void onFailure(Throwable throwable) {

    }

    @Override
    public void success(Response response, Response response2) {
        try {
            String tt = new String(((TypedByteArray) response.getBody()).getBytes(), "UTF-8");
            LogHelper.info("======", String.valueOf("hashCode:" + this.hashCode()) + ",end:" + System.nanoTime() + ",byte:" + tt.length());
            JSONObject object = new JSONObject(tt);

            JSONObject data = null;
            if (object.has(KEY_DATA))
                data = object.optJSONObject(KEY_DATA);
            if (object.getBoolean(KEY_SUCCESS)) {
                onSuccess(data);
            } else {
                int code = object.getInt(KEY_ERROR_CODE);
                onError(code, data);
            }
        } catch (JSONException e) {
            onFailure(e);
        } catch (UnsupportedEncodingException e) {
            onFailure(e);
        } catch (Exception e){
            onFailure(e);
        }
    }

    @Override
    public void failure(RetrofitError error) {
        if (error.getCause() != null)
            onFailure(error.getCause());
        else
            onFailure(new Throwable(error.getMessage()));
    }
}
