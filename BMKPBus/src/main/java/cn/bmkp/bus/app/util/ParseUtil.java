package cn.bmkp.bus.app.util;

import android.text.TextUtils;

import com.wilddog.client.DataSnapshot;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.bmkp.bus.app.models.BusLine;
import cn.bmkp.bus.app.models.BusRun;
import cn.bmkp.bus.app.models.LineTag;
import cn.bmkp.bus.app.models.Station;
import cn.bmkp.bus.app.models.StationPrice;
import cn.bmkp.bus.app.models.Ticket;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by zhangn on 2016/3/21.
 * <p/>
 * description:解析wilddog数据
 */
public class ParseUtil {

    /**
     * 解析线路数据
     *
     * @param dataSnapshot
     * @return
     */
    public static List<BusLine> parseBusLine(DataSnapshot dataSnapshot) {

        List<BusLine> list = new ArrayList<>();

        if (dataSnapshot != null) {

            Iterator iterator = dataSnapshot.getChildren().iterator();
            while (iterator.hasNext()) {

                BusLine busLine = new BusLine();
                DataSnapshot snapshot = (DataSnapshot) iterator.next();

                busLine.setBeginTime((String) snapshot.child(WilddogApi.KeyConst.BUS_BEGIN_TIME).getValue());
                busLine.setStations(parseLineStation(snapshot));
                busLine.setLineKey(snapshot.getKey());

                busLine.setStationPrices(parseStationPrice(busLine.getStations(), snapshot));

                list.add(busLine);
            }
        }

        return list;
    }

    /**
     * 解析某条线路的站点
     *
     * @param dataSnapshot
     * @return
     */
    private static List<Station> parseLineStation(DataSnapshot dataSnapshot) {

        int i = 1;

        List<Station> stationList = new ArrayList<>();

        while (getStationFromSnapshot(WilddogApi.KeyConst.STATION + i, dataSnapshot) != null) {
            if(!(TextUtils.isEmpty(getStationFromSnapshot(WilddogApi.KeyConst.STATION + i, dataSnapshot).getName()))){
                stationList.add(getStationFromSnapshot(WilddogApi.KeyConst.STATION + i, dataSnapshot));
//            stationList.add(getValueFormSnapshot(Station.class, "station" + i, dataSnapshot));
            }
            i++;
        }

        return stationList;
    }


    /**
     * 获取Station
     *
     * @param key
     * @param dataSnapshot
     * @return
     */
    private static Station getStationFromSnapshot(String key, DataSnapshot dataSnapshot) {

//        Station station = new Station();

        Station station = null;
        DataSnapshot stationSnapshot = dataSnapshot.child(key);

        if (stationSnapshot.getValue() != null) {

            station = (Station) stationSnapshot.getValue(Station.class);
//            station.setTime(getValueFormSnapshot(String.class, "time", stationSnapshot));
//            station.setArrive((Boolean) stationSnapshot.child("arrive").getValue());
//            station.setDetail(getValueFormSnapshot(String.class, "detail", stationSnapshot));
//            station.setDownNum(getValueFormSnapshot(String.class, "downNum", stationSnapshot));
//            station.setKey(getValueFormSnapshot(String.class, "key", stationSnapshot));
//            station.setName(getValueFormSnapshot(String.class, "name", stationSnapshot));

        }

        return station;
    }

    /**
     * 解析多条RUN数据
     *
     * @param dataSnapshot
     * @return
     */
    public static List<BusLine> parseBusRuns(DataSnapshot dataSnapshot) {

        List<BusLine> list = new ArrayList<>();

        if (dataSnapshot != null) {

            Iterator iterator = dataSnapshot.getChildren().iterator();
            while (iterator.hasNext()) {

                BusRun busRun = new BusRun();
                DataSnapshot snapshot = (DataSnapshot) iterator.next();

                busRun.setBeginTime((String) snapshot.child(WilddogApi.KeyConst.BUS_BEGIN_TIME).getValue());
                busRun.setStations(parseLineStation(snapshot));
                busRun.setLineKey(snapshot.getKey());

                busRun.setStationPrices(parseStationPrice(busRun.getStations(), snapshot));

                list.add(busRun);
            }
        }

        return list;
    }

    /**
     * 解析实际运行的线路数据
     *
     * @param dataSnapshot
     * @return
     */
    public static BusRun parseBusRun(DataSnapshot dataSnapshot) {

        BusRun busRun = new BusRun();
        busRun.setLineKey(dataSnapshot.getKey());
        busRun.setBeginTime(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_BEGIN_TIME, dataSnapshot));
        busRun.setEndTime(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_END_TIME, dataSnapshot));
        busRun.setBusNumber(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_BUS_NUMBER, dataSnapshot));
        busRun.setLicenseNumber(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_LICENSE_NUMBER, dataSnapshot));
        busRun.setDriverName(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_DRIVER_NAME, dataSnapshot));
        busRun.setSeatsLeft(parseSeatsLeft(dataSnapshot));
        busRun.setStations(parseLineStation(dataSnapshot));

        busRun.setStationPrices(parseStationPrice(busRun.getStations(), dataSnapshot));

        return busRun;
    }


    /**
     * 解析余票列表
     *
     * @param dataSnapshot
     * @return
     */
    private static List<String> parseSeatsLeft(DataSnapshot dataSnapshot) {

        int i = 1;

        List<String> seatsLeftList = new ArrayList<>();

        while (getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_RUN_REMAIN + i, dataSnapshot) != null) {

//            seatsLeftList.add(getValueFormSnapshot(String.class, WilddogApi.KeyConst.BUS_RUN_REMAIN + i, dataSnapshot));
            seatsLeftList.add(String.valueOf(dataSnapshot.child(WilddogApi.KeyConst.BUS_RUN_REMAIN + i).getValue()));

            i++;
        }

        return seatsLeftList;
    }


    /**
     * 获取某个节点下的属性值
     *
     * @param tClass
     * @param key
     * @param dataSnapshot
     * @param <T>
     * @return
     */
    private static <T> T getValueFormSnapshot(Class<T> tClass, String key, DataSnapshot dataSnapshot) {

        T t = null;

        DataSnapshot stationSnapshot = dataSnapshot.child(key);

        if (stationSnapshot.getValue() != null) {

            if (tClass.getClass().equals(String.class.getClass())) {

                t = (T) stationSnapshot.getValue();
            } else {
                t = (T) stationSnapshot.getValue(tClass);
            }
        }
        return t;
    }

    /**
     * 解析用户购票列表
     *
     * @param dataSnapshot
     * @return
     */
    public static List<Ticket> parseUserTickets(DataSnapshot dataSnapshot) {

        List<Ticket> list = new ArrayList<>();

        if (dataSnapshot != null) {

            Iterator iterator = dataSnapshot.getChildren().iterator();
            while (iterator.hasNext()) {

                DataSnapshot snapshot = (DataSnapshot) iterator.next();

                list.add(parseUserTicket(snapshot));
            }
        }

        return list;
    }


    /**
     * 解析一张车票
     *
     * @param snapshot
     * @return
     */
    public static Ticket parseUserTicket(DataSnapshot snapshot) {

        Ticket ticket = new Ticket();

        ticket.setTicketKey(snapshot.getKey());
        ticket.setDay((String) snapshot.child(WilddogApi.KeyConst.TICKET_DAY).getValue());
        ticket.setDriverName((String) snapshot.child(WilddogApi.KeyConst.TICKET_DRIVER_NAME).getValue());
        ticket.setLicenseNumber((String) snapshot.child(WilddogApi.KeyConst.TICKET_LICENSE_NUMBER).getValue());
        ticket.setBusNumber((String) snapshot.child(WilddogApi.KeyConst.TICKET_BUS_NUMBER).getValue());
        ticket.setStations(parseLineStation(snapshot));
        ticket.setTicketNum(Integer.parseInt((String) snapshot.child(WilddogApi.KeyConst.TICKET_TICKET_NUMBER).getValue()));
        ticket.setCheck_status(Boolean.parseBoolean((String) snapshot.child(WilddogApi.KeyConst.TICKET_CHECK).getValue()));
        ticket.setState(Long.parseLong(String.valueOf(snapshot.child(WilddogApi.KeyConst.TICKET_STATE).getValue())));
        return ticket;
    }

    /**
     * 解析用户票下的站点
     *
     * @param dataSnapshot
     * @return
     */
    private static List<String> parsePassStations(DataSnapshot dataSnapshot) {

        int i = 1;

        List<String> passStation = new ArrayList<>();

        while (i < 6) {

            String station = getValueFormSnapshot(String.class, WilddogApi.KeyConst.STATION + i, dataSnapshot);

            if (!TextUtils.isEmpty(station)) {
                passStation.add(station);
            }
            i++;
        }

        return passStation;
    }


    /**
     * 解析线路的Tag
     * @param dataSnapshot
     * @return
     */
    public static List<LineTag> parseLineTags(DataSnapshot dataSnapshot) {

        List<LineTag> tagList = new ArrayList<>();

        if (dataSnapshot != null) {

            Iterator iterator = dataSnapshot.getChildren().iterator();

            while (iterator.hasNext()) {

                DataSnapshot snapshot = (DataSnapshot) iterator.next();

                LineTag tag = new LineTag();

                tag.setKey(snapshot.getKey());
                tag.setName((String) snapshot.getValue());

                tagList.add(tag);
            }
        }

        return tagList;
    }

    /**
     * 解析某条线路的站点票价
     *
     * @param dataSnapshot
     * @return
     */
    private static List<StationPrice> parseStationPrice(List<Station> stationList, DataSnapshot dataSnapshot) {

        List<StationPrice> priceList = new ArrayList<>();

        for(Station station : stationList) {
            if (dataSnapshot.child(station.getName()).getValue() != null) {
                Iterator iterator = dataSnapshot.child(station.getName()).getChildren().iterator();
                while (iterator.hasNext()) {

                    StationPrice price = new StationPrice();
                    DataSnapshot snapshot = (DataSnapshot) iterator.next();

                    price.setStartName(dataSnapshot.child(station.getName()).getKey());
                    price.setEndName(snapshot.getKey());

//                    price.setPrice((String) snapshot.child("price").getValue());
//                    price.setTime((String) snapshot.child("time").getValue());


                    price.setPrice(String.valueOf(snapshot.child("price").getValue()));
                    price.setTime(String.valueOf(snapshot.child("time").getValue()));

                    priceList.add(price);
                }
            }
        }

        return priceList;
    }

    /**
     * 解析用户余额
     *
     * @param snapshot
     * @return
     */
    public static float parseOwnerBalance(DataSnapshot snapshot) {

        float balance = 0;
        if(snapshot.hasChild("balance")) {

            balance = Float.parseFloat(String.valueOf(snapshot.child("balance").getValue()));
        }

        return balance;
    }

}
