package cn.bmkp.bus.app.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.ui.LoginActivity;
import cn.bmkp.bus.app.util.AndyUtils;

/**
 * Created by chenliang on 2016/3/30.
 */

public class SimpleDialog extends DialogFragment {


    private CharSequence mMessage;
    private Dialog dialog;

    @OnClick(R.id.ib_clean_dialog)
    public void ibCleanDialog(View view) {
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.rl_simple_dialog)
    public void dialogOnClick(View view) {

        startActivity(new Intent(getActivity(), LoginActivity.class));
        dismissAllowingStateLoss();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), R.style.Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_simple_base, null);

        ButterKnife.bind(this, view);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(AndyUtils.getScreenWidth(getActivity())* 2 / 3,
                AndyUtils.getScreenWidth(getActivity()));
        dialog.setContentView(view, lp);

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
