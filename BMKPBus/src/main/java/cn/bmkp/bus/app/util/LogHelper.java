package cn.bmkp.bus.app.util;

import android.text.TextUtils;
import android.util.Log;


public class LogHelper {

    private static boolean DEBUG = true;

    public static void warn(Class c, String msg) {
        if (DEBUG) {
            Log.w("warn：" + c, !TextUtils.isEmpty(msg) ? msg : "");
        }
    }

    public static void warn(Class c, int msg) {
        if (DEBUG) {
            Log.w("warn：" + c, String.valueOf(msg));
        }
    }

    public static void warn(Class c, long msg) {
        if (DEBUG) {
            Log.w("warn：" + c, String.valueOf(msg));
        }
    }

    public static void warn(Class c, float msg) {
        if (DEBUG) {
            Log.w("warn：" + c, String.valueOf(msg));
        }
    }

    public static void warn(Class c, double msg) {
        if (DEBUG) {
            Log.w("warn：" + c, String.valueOf(msg));
        }
    }

    public static void warn(Class c, boolean msg) {
        if (DEBUG) {
            Log.w("warn：" + c, String.valueOf(msg));
        }
    }

    public static void warn(String flag, String msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, !TextUtils.isEmpty(msg) ? msg : "");
        }
    }

    public static void warn(String flag, int msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, String.valueOf(msg));
        }
    }

    public static void warn(String flag, long msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, String.valueOf(msg));
        }
    }

    public static void warn(String flag, float msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, String.valueOf(msg));
        }
    }

    public static void warn(String flag, double msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, String.valueOf(msg));
        }
    }

    public static void warn(String flag, boolean msg) {
        if (DEBUG) {
            Log.w("warn：" + flag, String.valueOf(msg));
        }
    }

    public static void e(String flag, Object o) {
        if (DEBUG) {
            Log.e("error：" + flag, o != null ? String.valueOf(o) : "");
        }
    }

    public static void e(Class c, String msg) {
        if (DEBUG) {
            Log.e("error：" + c, !TextUtils.isEmpty(msg) ? msg : "");
        }
    }

    public static void info(Class c, String msg) {
        if (DEBUG) {
            Log.i("info：" + c, !TextUtils.isEmpty(msg) ? msg : "");
        }
    }

    public static void info(String tag, String msg, Throwable tr) {
        if (DEBUG) {
            Log.i("info：" + tag, !TextUtils.isEmpty(msg) ? msg : "", tr);
        }
    }

    public static void info(Class c, int msg) {
        if (DEBUG) {
            Log.i("info：" + c, String.valueOf(msg));
        }
    }

    public static void info(Class c, long msg) {
        if (DEBUG) {
            Log.i("info：" + c, String.valueOf(msg));
        }
    }

    public static void info(Class c, float msg) {
        if (DEBUG) {
            Log.i("info：" + c, String.valueOf(msg));
        }
    }

    public static void info(Class c, double msg) {
        if (DEBUG) {
            Log.i("info：" + c, String.valueOf(msg));
        }
    }

    public static void info(Class c, boolean msg) {
        if (DEBUG) {
            Log.i("info：" + c, String.valueOf(msg));
        }
    }

    public static void info(String flag, String msg) {
        if (DEBUG) {
            Log.i("info：" + flag, !TextUtils.isEmpty(msg) ? msg : "");
        }
    }

    public static void info(String flag, int msg) {
        if (DEBUG) {
            Log.i("info：" + flag, String.valueOf(msg));
        }
    }

    public static void info(String flag, long msg) {
        if (DEBUG) {
            Log.i("info：" + flag, String.valueOf(msg));
        }
    }

    public static void info(String flag, float msg) {
        if (DEBUG) {
            Log.i("info：" + flag, String.valueOf(msg));
        }
    }

    public static void info(String flag, double msg) {
        if (DEBUG) {
            Log.i("info：" + flag, String.valueOf(msg));
        }
    }

    public static void info(String flag, boolean msg) {
        if (DEBUG) {
            Log.i("info：" + flag, String.valueOf(msg));
        }
    }
}
