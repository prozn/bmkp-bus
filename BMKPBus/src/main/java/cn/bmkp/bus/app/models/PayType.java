package cn.bmkp.bus.app.models;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description: 支付账户
 */
public class PayType {

    private String alipay;

    private String weixin;

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    @Override
    public String toString() {
        return "PayType{" +
                "alipay='" + alipay + '\'' +
                ", weixin='" + weixin + '\'' +
                '}';
    }
}
