package cn.bmkp.bus.app.util;

/**
 * Created by fm on 16/3/18.
 */
public class PayConst {

    public class ALi{
        public final static String PID = "2088021273428911";
        public final static String SELLER = "wf_huang@bmkp.cn";
        public final static String RSA_PRIVATE = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBALDv+rzBHZ3Fg67D" +
                "RPbY5EBA7oMjCEnGpJMJkuaA4VKMSt88Rx4scP+7ORFLIkX5E6a0O9A0HIotpcyX" +
                "9jAmu2zx5CHPb6Wjsqge6yGqFXij8onRw4LMYdLLQtFACfGXZZGdfvcdH0WbLOTw" +
                "Bcmeu4L7+jT4Afdilq3ZG2aLXPrVAgMBAAECgYBlNS/YLiJxr5wOKBXXmOKeukVz" +
                "72L7SEu064ICpW+9VxAwtvr7EHKmZQJUmQSGv910up2ID3bPtyKib73bdxw3VnRU" +
                "NWJ6MbHcpHbRNriuwTOPCxMS9AbBjKyPkzKXVE31bBXpdVx+n7dCVDcLrnSfabQC" +
                "IMxOmw3pj77PPdEolQJBANjVXiA1zVz+OkhgoIgOIy8315E+piu2zfZqWR9SBuTN" +
                "+Z2KpTjaUTrxCp5QGJsT7ijDurJtAonXRL43ijEYl+sCQQDQ5cWh+RMGczWgRH7Q" +
                "b6FhyIz9Dd2fvATskaMFylhBrA/uVwskGcST/Co487FmbXzW2YydrGXS4MXkufp2" +
                "4Mg/AkAuEK5Ng4Cch/oT1EtmfDJnXqKyXa/py41YE2HZsJB8XXHxUTomqOLm9bx+" +
                "w59mmsZW7LYmH9iRNiWJj70RDxt/AkAw+o66CXJCguTB7Q1mxaWrDaCw/H1IJIdr" +
                "CEKW6viCfVtG4LXGAxyqLeegbtLbVzR8E4n6th8xsG310P0+vjl1AkAocKX3zqfl" +
                "KP8OJtHIc/2+Y//KRhxukaKPgC2UCQbqefxuzNUQ0O08UEqUSf5/6aDjTOODJ3Ow" +
                "91+rrG4dtrl/";
        public final static String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFl" +
                "jhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/" +
                "VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG" +
                "9zpgmLCUYuLkxpLQIDAQAB";
        public final static String NOTIFY_URL = "http://106.75.132.41:89/notify/alipay";
    }

    //微信
    public class WeChat {
        public final static String APP_ID = "wx7b987e8c69471a47";
        public final static String MCH_ID = "1323891301";
        public final static String API_KEY = "bmkp12345678910111131415bmkpbmkp";
        public final static String NOTIFY_URL = "http://106.75.132.41:89/notify/weixin";
    }
}
