package cn.bmkp.bus.app.util;

import android.app.Activity;

import java.util.Stack;

/**
 * Created by fanming on 2015/7/21.
 */
public class ActivityStack {

    private static ActivityStack instance;
    public static Stack<Activity> stack;

    public static ActivityStack getInstance() {
        if (instance == null) {
            synchronized(ActivityStack.class) {
                if (instance == null)
                    instance = new ActivityStack();
            }
        }
        return instance;
    }

    public static void pushActivity(Activity a) {
        if (stack == null)
            stack = new Stack<Activity>();
        stack.add(a);
    }

    public void popActivity(Activity activity) {
        if (activity != null) {
            activity.finish();
            if (stack != null && stack.size() > 0)
                stack.remove(activity);
        }
    }

    public void popActivity() {
        if (stack != null && stack.size() > 0) {
            Activity activity = stack.lastElement();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    public Activity currentActivity() {
        if (stack != null && stack.size() > 0) {
            Activity activity = stack.lastElement();
            return activity;
        }
        return null;
    }

    public void popAllActivityExceptOne(Class cls) {
        while (true) {
            Activity activity = currentActivity();
            if (activity == null)
                break;
            if (activity.getClass().equals(cls))
                break;
            popActivity(activity);
        }
    }

    public void popAllActivity() {
        while (true) {
            Activity activity = currentActivity();
            if (activity == null)
                break;
            popActivity(activity);
        }
    }

    public void finishAllActivity() {
        if (stack != null && stack.size() > 0) {
            for (Activity activity : stack) {
                if (null != activity)
                    activity.finish();
            }
        }
    }
}
