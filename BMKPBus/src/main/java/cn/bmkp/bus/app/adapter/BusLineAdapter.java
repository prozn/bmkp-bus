package cn.bmkp.bus.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.models.BusLine;

/**
 * Created by fm on 2015/11/18.
 */
public class BusLineAdapter extends BaseAdapter {

    private Context mContext;
    private List<BusLine> list;

    public BusLineAdapter(Context context, List<BusLine> list) {
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final BusLine busLine = list.get(position);
        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_bus_line, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.tvBuslineStartAddress.setText(busLine.getStations().get(0).getName());
        holder.tvBuslineEndAddress.setText(busLine.getStations().get(busLine.getStations().size()-1).getName());
        holder.tvBuslineDepartTime.setText(busLine.getBeginTime()+mContext.getString(R.string.text_start));
        holder.tvBuslineDepartWeekend.setText("");

        return convertView;
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_bus_line.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @Bind(R.id.iv_bus_img)
        ImageView ivBusImg;
        @Bind(R.id.iv_point_line)
        ImageView ivPointLine;
        @Bind(R.id.tv_busline_start_address)
        TextView tvBuslineStartAddress;
        @Bind(R.id.tv_busline_end_address)
        TextView tvBuslineEndAddress;
        @Bind(R.id.tv_busline_depart_time)
        TextView tvBuslineDepartTime;
        @Bind(R.id.ll_depart_time)
        LinearLayout llDepartTime;
        @Bind(R.id.tv_busline_depart_weekend)
        TextView tvBuslineDepartWeekend;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
