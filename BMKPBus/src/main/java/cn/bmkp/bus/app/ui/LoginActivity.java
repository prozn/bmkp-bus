package cn.bmkp.bus.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.wilddog.client.AuthData;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;

import cn.bmkp.bus.app.common.Const;


import cn.bmkp.bus.app.retrofit.CallbackHandler;
import cn.bmkp.bus.app.wilddog.WilddogApi;

public class LoginActivity extends BaseActivity {

    private SMSBroadcastReceiver mSMSBroadcastReceiver;
    private static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    private String verifyId;
    private String verifyPhone;
    private Timer timer;
    private int time;

    @Bind(R.id.tv_login_get_verifycode)
    TextView tvLoginGetVerifycode;
    @Bind(R.id.tv_login)
    TextView tvLogin;
    @Bind(R.id.et_login_phone)
    MaterialEditText etLoginPhone;

    @Bind(R.id.et_login_verifycode)
    MaterialEditText etLoginVerifycode;

    @OnClick(R.id.tv_login)
    public void login(View v) {

        if (isValidate()) {
            showProgressDialog(getString(R.string.text_in_login));
            getApi().verifyLogin(etLoginPhone.getText().toString(),
                    verifyId,
                    etLoginVerifycode.getText().toString(),
                    Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID),
                    new LoginCallBack());
        }
    }


    @OnClick(R.id.ib_function_left)
    public void ibBack(View view) {
        this.finish();
    }

    @OnClick(R.id.tv_login_get_verifycode)
    public void getVerifycode(View view) {
        if (etLoginPhone.getText().length() == 11) {
            time = 60;
            tvLoginGetVerifycode.setEnabled(false);
            timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (tvLoginGetVerifycode != null) {
                                tvLoginGetVerifycode.setText(String.valueOf(time--) + getString(R.string.text_second));
                            }
                            if (time < 0) {
                                time = 60;
                                tvLoginGetVerifycode.setText(R.string.text_send_again);
                                tvLoginGetVerifycode.setEnabled(true);
                                timer.cancel();
                            }
                        }
                    });
                }
            }, 0, 1000);

            showProgressDialog(getString(R.string.text_verify_code_is_sending));
            getApi().getSms(etLoginPhone.getText().toString(), new SMSCodeCallBack());

        } else {
            Toast.makeText(LoginActivity.this, R.string.text_wrong_phone_number, Toast.LENGTH_SHORT).show();
        }
    }

    protected boolean isValidate() {
        String msg = null;
        if (TextUtils.isEmpty(etLoginPhone.getText())) {
            msg =  getResources().getString(R.string.text_tel_can_not_empty);
        } else if (etLoginPhone.getText().length() != 11) {
            msg =  getString(R.string.text_wrong_phone_number);
        } else if (TextUtils.isEmpty(verifyId)) {
            msg = getResources().getString(R.string.text_get_verify_first);
        } else if (TextUtils.isEmpty(etLoginVerifycode.getText().toString())) {
            msg = getResources().getString(R.string.text_verify_code_can_not_empty);
        } else if (verifyPhone != null && !verifyPhone.equals(etLoginPhone.getText().toString())) {
            msg = getResources().getString(R.string.text_tel_not_match_verify_code);
        }
        if (msg == null)
            return true;

        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mSMSBroadcastReceiver = new SMSBroadcastReceiver();
        //实例化过滤器并设置要过滤的广播
        IntentFilter intentFilter = new IntentFilter(SMS_RECEIVED_ACTION);
        intentFilter.setPriority(1000);
        //注册广播
        registerReceiver(mSMSBroadcastReceiver, intentFilter);

        WilddogApi.getInstance().init(this);

    }

    class SMSCodeCallBack extends CallbackHandler {
        @Override
        public void onSuccess(JSONObject data) {
            super.onSuccess(data);
            dismissDialog();
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.text_send_verify_success_please_wait), Toast.LENGTH_SHORT).show();
            try {
                verifyId = getParseHelper().getVerifyId(data);
                verifyPhone = etLoginPhone.getText().toString();
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.text_fail_and_retry), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onError(int code, JSONObject data) {
            super.onError(code, data);
            dismissDialog();
            cancelTimer();
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.text_send_verify_code_fail_and_retry), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            dismissDialog();
            cancelTimer();
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.text_send_verify_code_fail_and_retry), Toast.LENGTH_SHORT).show();
        }
    }

    private void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
        tvLoginGetVerifycode.setEnabled(true);
        tvLoginGetVerifycode.setText(getResources().getString(R.string.text_find_password_get_verify_code));
    }

    class SMSBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {

                Object[] objs = (Object[]) intent.getExtras().get("pdus");
                for (Object obj : objs) {
                    byte[] pdu = (byte[]) obj;
                    SmsMessage sms = SmsMessage.createFromPdu(pdu);
                    // 短息的手机号
                    String from = sms.getOriginatingAddress();
                    // 短信的内容
                    String message = sms.getMessageBody();

                    if (message.contains(getString(R.string.text_bmkp)) && message.contains(getString(R.string.text_verify_code))) {
                        String regEx="[^0-9]";
                        Pattern p = Pattern.compile(regEx);
                        Matcher m = p.matcher(message);
                        etLoginVerifycode.setText(m.replaceAll("").trim());
                    }

                }
            }
        }

    }




    /**
     * token认证
     * @param token
     */
    private void authToken(final String token){

        WilddogApi.getInstance().authWithToken(token, new Wilddog.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {

                // TODO 页面跳转

                Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();

                getPreferenceHelper().putToken(token);
                getPreferenceHelper().putLoginStatus(true);
                getPreferenceHelper().putUserName(etLoginPhone.getText().toString());
//                getPreferenceHelper().putVerifyCode(etLoginVerifycode.getText().toString());

                String to = getIntent().getStringExtra(Const.ACTIVITY_TO);
                if (to != null && to.equals(MyWalletActivity.class.getSimpleName())) {
                    startActivity(new Intent(LoginActivity.this, MyWalletActivity.class));

                }else if (to != null && to.equals(MyTicketsActivity.class.getSimpleName())) {
                    startActivity(new Intent(LoginActivity.this, MyTicketsActivity.class));
                }
//                else {
                 //   startActivity(new Intent(LoginActivity.this, BuyTicketActivity.class));
//                }

                LoginActivity.this.finish();
            }

            @Override
            public void onAuthenticationError(WilddogError wilddogError) {

                // TODO 提示认证失败，重新登录
                Toast.makeText(LoginActivity.this, "登录失败，请重新登录", Toast.LENGTH_SHORT).show();

            }
        });
    }

    class LoginCallBack extends CallbackHandler {
        @Override
        public void onSuccess(JSONObject data) {
            super.onSuccess(data);
            dismissDialog();

            try {
                // token 认证
                authToken(getParseHelper().parseToken(data));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int code, JSONObject data) {
            super.onError(code, data);
            dismissDialog();
            Toast.makeText(LoginActivity.this, "Code:" + code, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            dismissDialog();
            Toast.makeText(LoginActivity.this, R.string.text_login_out_time_please_retry, Toast.LENGTH_SHORT).show();
        }
    }
}
