package cn.bmkp.bus.app;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import cn.bmkp.bus.app.db.DBHelper;
import cn.bmkp.bus.app.retrofit.BaseApi;
import cn.bmkp.bus.app.retrofit.ParseHelper;
import cn.bmkp.bus.app.retrofit.PreferenceHelper;
import cn.bmkp.bus.app.retrofit.ServerApi;
import cn.bmkp.bus.app.util.PayConst;
import cn.bmkp.bus.app.wilddog.WilddogApi;
import cn.bmkp.pay.AutoPay;
import retrofit.RestAdapter;

/**
 * Created by zhangn on 2016/3/18.
 */
public class BmkpBusApplication extends Application {

    private ServerApi serverApi;
    private BaseApi baseApi;
    private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private ParseHelper parseHelper;
    private DBHelper dbHelper;
    private PreferenceHelper preferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        initDB();

        initAutoPay();

        initWilddog();

        //初始化网络通信
        initRest();

        parseHelper = new ParseHelper(this);
        preferenceHelper = new PreferenceHelper(this);
    }

    private void initWilddog() {
        WilddogApi.getInstance().init(this);
    }


    /**
     * 初始化数据库
     */
    private void initDB() {

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "bmkpbus-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        dbHelper = new DBHelper(daoMaster.newSession());
    }

    /**
     * 初始化支付
     * */
    private void initAutoPay() {
        //支付宝支付
        AutoPay.setAliPID(PayConst.ALi.PID);
        AutoPay.setAliSeller(PayConst.ALi.SELLER);
        AutoPay.setAliRsaPrivate(PayConst.ALi.RSA_PRIVATE);
        AutoPay.setAliRsaPublic(PayConst.ALi.RSA_PUBLIC);
        AutoPay.setAliNotifyUrl(PayConst.ALi.NOTIFY_URL);
//        //微信支付
        AutoPay.setWxAppID(PayConst.WeChat.APP_ID);
        AutoPay.setWxMchId(PayConst.WeChat.MCH_ID);
        AutoPay.setWxApiKey(PayConst.WeChat.API_KEY);
        AutoPay.setWxNotifyUrl(PayConst.WeChat.NOTIFY_URL);
    }

    private void initRest() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BaseApi.SERVER_IP)
                .build();
        baseApi = restAdapter.create(BaseApi.class);
        serverApi = new ServerApi(baseApi);
    }

    public ParseHelper getParseHelper() {
        return parseHelper;
    }

    public ServerApi getServerApi() {
        return serverApi;
    }
    public DBHelper getDBHelper() {
        return dbHelper;
    }

    public PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }
}
