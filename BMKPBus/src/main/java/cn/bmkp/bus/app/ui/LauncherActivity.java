package cn.bmkp.bus.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;

public class LauncherActivity extends BaseActivity {

    private final Runnable mShowRunnable = new Runnable() {
        @Override
        public void run() {
            show();
            startActivity(new Intent(LauncherActivity.this, MainActivity.class).putExtra("from", LauncherActivity.class.getSimpleName()));
            LauncherActivity.this.finish();
        }
    };

    @Bind(R.id.fullscreen_content)
    ImageView fullscreenContent;


    private final Handler mShowHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logo);
        ButterKnife.bind(this);

        fullscreenContent.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        delayedShow(2000);

    }

    private void show() {
        fullscreenContent.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
    }

    private void delayedShow(int delayMillis) {
        mShowHandler.removeCallbacks(mShowRunnable);
        mShowHandler.postDelayed(mShowRunnable, delayMillis);
    }
}
