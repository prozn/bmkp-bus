package cn.bmkp.bus.app.generator;

import java.util.UUID;

/**
 * Created by zhangn on 2016/3/30.
 *
 * description:UUID的生成key
 */
public class UUIDGenerator {

    public static String getUUID() {

        return UUID.randomUUID().toString();
    }
}
