package cn.bmkp.bus.app.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhangn on 2016/3/21.
 *
 * description: 票信息
 */
public class Ticket implements Serializable {

    private String ticketKey;

    private String licenseNumber;

    private String busNumber;

    private String line;

    private String payment;

    private PayType payType;

    private String driverName;

    private String day; // 乘车日期

    private String createdAt;

    //废弃
    private boolean check_status;

    private int ticketNum;

    //车票状态、支付，检票，退票等
    private long state;

    public String getTicketKey() {
        return ticketKey;
    }

    public void setTicketKey(String ticketKey) {
        this.ticketKey = ticketKey;
    }

    public int getTicketNum() {
        return ticketNum;
    }

    public void setTicketNum(int ticketNum) {
        this.ticketNum = ticketNum;
    }

    private List<Station> stations;

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }


    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public boolean isCheck_status() {
        return check_status;
    }

    public void setCheck_status(boolean check_status) {
        this.check_status = check_status;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public long getState() {
        return state;
    }

    public void setState(long state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "licenseNumber='" + licenseNumber + '\'' +
                ", line='" + line + '\'' +
                ", payment='" + payment + '\'' +
                ", payType=" + payType +
                ", driverName='" + driverName + '\'' +
                ", day='" + day + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", stations=" + stations +
                '}';
    }
}
