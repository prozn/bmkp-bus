package cn.bmkp.bus.app.base;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.umeng.analytics.MobclickAgent;

import cn.bmkp.bus.app.BmkpBusApplication;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.dialog.LineCollectionDialog;
import cn.bmkp.bus.app.dialog.SimpleDialog;
import cn.bmkp.bus.app.retrofit.ParseHelper;
import cn.bmkp.bus.app.retrofit.PreferenceHelper;
import cn.bmkp.bus.app.retrofit.ServerApi;
import cn.bmkp.bus.app.util.ActivityStack;
import cn.bmkp.bus.app.util.AndyUtils;

/**
 * Created by fm on 16/3/18.
 */
public class BaseActivity extends FragmentActivity {

    ProgressDialog progressDialog;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //入栈
        ActivityStack.getInstance().pushActivity(this);

        //网络检测
//        if (!AndyUtils.isNetworkAvailable(this))
//            showNetworkDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //出栈
        ActivityStack.getInstance().popActivity(this);
    }

    public ServerApi getApi() {
        return ((BmkpBusApplication) getApplication()).getServerApi();
    }

    public ParseHelper getParseHelper() {
        return ((BmkpBusApplication) getApplication()).getParseHelper();
    }

    public PreferenceHelper getPreferenceHelper() {
        return ((BmkpBusApplication) getApplication()).getPreferenceHelper();
    }

    public void showProgressDialog(String detail) {
        if (isFinishing())
            return;

        dismissDialog();
        progressDialog = ProgressDialog.show(BaseActivity.this, "", detail, true, false);
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public void showAlertDialog(String title, String msg,
                                String positive, DialogInterface.OnClickListener positiveListener,
                                String negative, DialogInterface.OnClickListener negativeListener,
                                Context context) {
        if(alertDialog == null || !alertDialog.isShowing()){
            alertDialog = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .show();
        }
    }

    /**
     * 登录送金币
     */
    public void showLoginSendGoldsDialog() {
        SimpleDialog dialog = new SimpleDialog();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showCollectLineDialog() {
        LineCollectionDialog dialog = new LineCollectionDialog();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
    }

    public void showNetworkDialog() {
        showAlertDialog(getString(R.string.text_network_setup_tips), getString(R.string.text_no_network), getString(R.string.text_setting), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismissDialog();
                try {
                    Intent intent = null;
                    if (android.os.Build.VERSION.SDK_INT > 10) {
                        intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                    } else {
                        intent = new Intent();
                        ComponentName component = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");
                        intent.setComponent(component);
                        intent.setAction("android.intent.action.VIEW");
                    }
                    BaseActivity.this.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "取消", null, this);
    }

}
