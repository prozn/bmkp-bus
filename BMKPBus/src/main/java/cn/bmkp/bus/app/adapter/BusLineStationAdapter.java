package cn.bmkp.bus.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.models.Station;

/**
 * Created by fm on 2015/11/18.
 */
public class BusLineStationAdapter extends BaseAdapter {

    private Context mContext;
    private List<Station> list;

    public BusLineStationAdapter(Context context, List<Station> list) {
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Station station = list.get(position);
        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_bus_line_station, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.tvDepartStationName.setText(station.getName());
        holder.tvStationDetail.setText(station.getDetail());
        holder.tvDepartTime.setText("("+station.getTime()+")");

        return convertView;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_bus_line_station.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @Bind(R.id.tv_depart_station_name)
        TextView tvDepartStationName;
        @Bind(R.id.tv_depart_time)
        TextView tvDepartTime;
        @Bind(R.id.tv_station_detail)
        TextView tvStationDetail;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
