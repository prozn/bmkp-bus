package cn.bmkp.bus.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.WilddogError;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.adapter.MyTicketsAdapter;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.models.Ticket;
import cn.bmkp.bus.app.util.AndyUtils;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.wilddog.WilddogApi;
import cn.bmkp.bus.app.wxapi.WXPayEntryActivity;

/**
 * Created by LW on 2016/3/19.
 */
public class MyTicketsActivity extends BaseActivity {

    @Bind(R.id.lv_my_tickets)
    ListView lvMyTickets;
    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_show_not_item)
    TextView tvShowNotItem;

    private List<Ticket> tickets = null;

//    private List<Ticket> unusedTickets;
//
//    private List<Ticket> usedTickets;

    private MyTicketsAdapter ticketsAdapter;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        goToMainActivity();
    }

    @OnClick(R.id.tv_history_ticket)
    public void historyTickets(View v) {
        startActivity(new Intent(MyTicketsActivity.this, MyHistoryTicketsActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_my_tickets);

        ButterKnife.bind(this);

        tvTitle.setText(R.string.text_my_ticket);


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (AndyUtils.isNetworkAvailable(this)) {
            showProgressDialog(getString(R.string.text_is_loading));
        } else {
            showNetworkDialog();
        }

        setData();

        lvMyTickets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                if (unusedTickets.get(position).getState() == Const.TICKET_STATE_CHECKED) {
//                    Toast.makeText(MyTicketsActivity.this, "该车票已使用！", Toast.LENGTH_SHORT).show();
//                } else {

                Intent intent = new Intent(MyTicketsActivity.this, TicketPaySuccessActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("ticket", tickets.get(position));
                intent.putExtra("ticketKey", tickets.get(position).getTicketKey());
                startActivityForResult(intent, 1);

//                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setData() {
        if (getPreferenceHelper().getLoginStatus() && getPreferenceHelper().getUserName()!=null) {

            WilddogApi.getInstance().getUnusedTicket(getPreferenceHelper().getUserName(), new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    tickets = ParseUtil.parseUserTickets(dataSnapshot);

//                    unusedTickets = new ArrayList<>();
//
//                    usedTickets = new ArrayList<>();
//
//                    for (Ticket ticket : tickets) {
//
//                        if(ticket.getState() == Const.TICKET_STATE_PAID) {
//                            unusedTickets.add(ticket);
//                        } else if(ticket.getState() == Const.TICKET_STATE_CHECKED){
//                            usedTickets.add(ticket);
//                        }
//                    }

                    if (tickets.size() > 0) {
                        ticketsAdapter = new MyTicketsAdapter(MyTicketsActivity.this, R.layout.list_item_my_ticket, tickets);
                        lvMyTickets.setAdapter(ticketsAdapter);
                        tvShowNotItem.setVisibility(View.GONE);
                    } else {
                        tvShowNotItem.setVisibility(View.VISIBLE);
                    }

                    dismissDialog();
                }

                @Override
                public void onCancelled(WilddogError wilddogError) {
                    dismissDialog();
                }
            });
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            ticketsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            goToMainActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void goToMainActivity() {
        startActivity(new Intent(MyTicketsActivity.this, MainActivity.class));
    }

}
