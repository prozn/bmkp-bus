package cn.bmkp.bus.app.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddog.client.DataSnapshot;
import com.wilddog.client.ValueEventListener;
import com.wilddog.client.Wilddog;
import com.wilddog.client.WilddogError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmkp.bus.app.R;
import cn.bmkp.bus.app.base.BaseActivity;
import cn.bmkp.bus.app.common.Const;
import cn.bmkp.bus.app.models.Ticket;
import cn.bmkp.bus.app.util.ParseUtil;
import cn.bmkp.bus.app.wilddog.WilddogApi;

/**
 * Created by LW on 2016/3/19.
 */
public class TicketInfoActivity extends BaseActivity {

    private String phone = "4000833359";

    @Bind(R.id.tv_ticket_info_from)
    TextView tvTicketInfoFrom;
    @Bind(R.id.tv_ticket_info_to)
    TextView tvTicketInfoTo;
    @Bind(R.id.rl_ticket_address_more)
    RelativeLayout rlTicketAddressMore;
    @Bind(R.id.tv_ticket_info_date)
    TextView tvTicketInfoDate;
    @Bind(R.id.tv_ticket_info_people)
    TextView tvTicketInfoPeople;
    @Bind(R.id.tv_ticket_info_tip_time)
    TextView tvTicketInfoTipTime;
    @Bind(R.id.tv_ticket_info_tip_from)
    TextView tvTicketInfoTipFrom;
    @Bind(R.id.tv_ticket_info_driver)
    TextView tvTicketInfoDriver;
    @Bind(R.id.tv_ticket_info_car_num_)
    TextView tvTicketInfoCarNum;
    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.btn_ticket_info_car_check)
    Button btnTicketInfoCarCheck;
    @Bind(R.id.btn_ticket_info_car_cancle)
    TextView btnTicketInfoCarCancel;

    private Ticket ticket;
    private String ticketKey;

    @OnClick(R.id.ib_function_left)
    public void back(View v) {
        this.finish();
    }

    //司机验票
    @OnClick(R.id.btn_ticket_info_car_check)
    public void checkTicket(View view) {

        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);
        String dateString;
        //班次时间
        try {
            Date date = df.parse(ticket.getDay());
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);
            dateString = String.valueOf(ca.get(Calendar.MONTH) + 1) + getString(R.string.text_month)
                    + String.valueOf(ca.get(Calendar.DATE)) + getString(R.string.text_day);
        } catch (ParseException e) {
            dateString = ticket.getDay();
        }

        String msg = getString(R.string.text_line_number) + dateString + " " + ticket.getStations().get(0).getTime()
                + "\n" + getString(R.string.text_line_name) + ticket.getStations().get(0).getName() + "---" + ticket.getStations().get(ticket.getStations().size() - 1).getName()
                + "\n" + getString(R.string.text_person_number) + ticket.getTicketNum() + getString(R.string.text_person);

        showAlertDialog(getString(R.string.text_check_passenger_msg), msg, getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WilddogApi.getInstance().checkTicket(getPreferenceHelper().getUserName(), ticket.getTicketKey(),
                        new Wilddog.CompletionListener() {
                            @Override
                            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
                                Toast.makeText(TicketInfoActivity.this, R.string.text_check_ticket_success, Toast.LENGTH_SHORT).show();
                                btnTicketInfoCarCheck.setEnabled(false);
                                btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        }, getString(R.string.text_cancel), null, TicketInfoActivity.this);
    }

    //退票
    @OnClick(R.id.btn_ticket_info_car_cancle)
    public void refundTicket(View view) {

        String sBegin = ticket.getStations().get(0).getTime();
        int hour = Integer.parseInt(sBegin.substring(0, sBegin.indexOf(":")));
        int minute = Integer.parseInt(sBegin.substring(sBegin.indexOf(":") + 1, sBegin.length()));

        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);

        Calendar beginTime = Calendar.getInstance();
        try {
            Date date = df.parse(ticket.getDay());
            beginTime.setTime(date);
        } catch (ParseException e) {
            Toast.makeText(this, R.string.text_wrong_date, Toast.LENGTH_SHORT).show();
        }
        beginTime.set(Calendar.HOUR_OF_DAY, hour);
        beginTime.set(Calendar.MINUTE, minute);
        Calendar nowTime = Calendar.getInstance();

        long  time = beginTime.getTimeInMillis() - nowTime.getTimeInMillis();
        if (time < 0) {
            showAlertDialog("", getString(R.string.text_car_left), getString(R.string.text_confirm), null, getString(R.string.text_cancel), null, TicketInfoActivity.this);

            btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);
            btnTicketInfoCarCheck.setText(getString(R.string.text_car_left));
            btnTicketInfoCarCheck.setEnabled(false);

            return;

        } else if (time < cn.bmkp.bus.app.common.Const.TIME_STOP_REFUND_TICKET) {

            showAlertDialog("", getString(R.string.text_stop_refund_ticket), getString(R.string.text_confirm), null, getString(R.string.text_cancel), null, TicketInfoActivity.this);

            btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);

            return;
        }

        showAlertDialog(getString(R.string.text_confirm_refund_ticket), getString(R.string.text_refund_ticket_phone) + phone, getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phone));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
//                        startActivity(callIntent);
                        Toast.makeText(TicketInfoActivity.this, R.string.text_no_call, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                startActivity(callIntent);

                // 改为由客服完成
//                WilddogApi.getInstance().getRefundForTicket(getPreferenceHelper().getUserName(), ticket.getTicketKey(),
//                        new Wilddog.CompletionListener() {
//                            @Override
//                            public void onComplete(WilddogError wilddogError, Wilddog wilddog) {
//                                Toast.makeText(TicketInfoActivity.this, "请耐心等待", Toast.LENGTH_SHORT).show();
//                                btnTicketInfoCarCheck.setEnabled(false);
//                                btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);
//                            }
//                        });

            }
        }, getString(R.string.text_cancel), null, TicketInfoActivity.this);
    }

    //查看车辆位置
    @OnClick(R.id.btn_ticket_info_car_location)
    public void markerDriver(View view) {
        Intent intent = new Intent(TicketInfoActivity.this, CarLocationActivity.class);
        intent.putExtra("busNumber", ticket.getBusNumber());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ticket_info);
        ButterKnife.bind(this);

        tvTitle.setText(R.string.text_ticket_details);

        Bundle bundle = getIntent().getExtras();
        ticket = (Ticket) bundle.getSerializable("ticket");

        if (ticket != null) {
            setData(ticket);

            WilddogApi.getInstance().setTicketListener(getPreferenceHelper().getUserName(), ticket.getTicketKey(), new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    ticket = ParseUtil.parseUserTicket(dataSnapshot);
                    if (ticket.getState() == Const.TICKET_STATE_WAIT_REFUND) {

                        btnTicketInfoCarCheck.setText(R.string.text_wait_for_refund);
                        btnTicketInfoCarCheck.setEnabled(false);
                        btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);

                    } else if (ticket.getState() == Const.TICKET_STATE_REFUNDED) {

                        btnTicketInfoCarCheck.setText(R.string.text_refund_success);

                    } else if(ticket.getState() == Const.TICKET_STATE_PAID) {

                        btnTicketInfoCarCheck.setText(R.string.text_check);
                        btnTicketInfoCarCheck.setEnabled(true);
                        btnTicketInfoCarCancel.setVisibility(View.VISIBLE);

                    } else if(ticket.getState() == Const.TICKET_STATE_CHECKED) {

                        btnTicketInfoCarCheck.setText(R.string.text_checked);
                        btnTicketInfoCarCheck.setEnabled(false);
                        btnTicketInfoCarCancel.setVisibility(View.INVISIBLE);

                    }
                }

                @Override
                public void onCancelled(WilddogError wilddogError) {

                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setData(Ticket ticket) {
//        Bundle bundle = getIntent().getExtras();
//        ticket = (Ticket) bundle.getSerializable("ticket");
//        if (ticket != null) {
        tvTicketInfoFrom.setText(ticket.getStations().get(0).getName());//起点

        tvTicketInfoTo.setText(ticket.getStations().get(ticket.getStations().size() - 1).getName());//终点

        SimpleDateFormat df = new SimpleDateFormat(Const.BUS_RUN_DATE_KEY_FORMAT);
        //班次时间
        try {
            Date date = df.parse(ticket.getDay());
            Calendar ca = Calendar.getInstance();
            ca.setTime(date);
            tvTicketInfoDate.setText(getString(R.string.text_bus_time) + String.valueOf(ca.get(Calendar.MONTH) + 1) + getString(R.string.text_month)
                    + String.valueOf(ca.get(Calendar.DATE)) + getString(R.string.text_day));
        } catch (ParseException e) {
            tvTicketInfoDate.setText(getString(R.string.text_bus_time) + ticket.getDay());
        }

        tvTicketInfoTipTime.setText(ticket.getStations().get(0).getTime());//提醒时间
        tvTicketInfoTipFrom.setText(ticket.getStations().get(0).getDetail());//提醒等候地点
        tvTicketInfoDriver.setText(ticket.getDriverName() + getString(R.string.text_driver));//司机
        tvTicketInfoCarNum.setText(ticket.getLicenseNumber());//车牌号
        tvTicketInfoPeople.setText("(" + ticket.getTicketNum() + getString(R.string.text_ticket_number));//乘车人数
//        }
    }

    public void showDialog(String title, String msg, String positive, String negative) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(TicketInfoActivity.this);
        dialog.setTitle(title);
        dialog.setMessage(msg);
        dialog.setCancelable(false);
        dialog.setPositiveButton(positive, new DialogInterface.
                OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(TicketInfoActivity.this, MyTicketsActivity.class);
                setResult(1, intent);
                TicketInfoActivity.this.finish();
            }
        });
        dialog.setNegativeButton(negative, new DialogInterface.
                OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }
}
