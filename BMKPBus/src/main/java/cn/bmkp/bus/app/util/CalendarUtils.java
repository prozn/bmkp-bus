package cn.bmkp.bus.app.util;

import java.util.ArrayList;
import java.util.List;

import cn.bmkp.bus.app.widget.SpecialCalendar;

/**
 * Created by LW on 2016/3/29.
 */
public class CalendarUtils {

    private static int weeksOfMonth = 0;

    /**
     * 判断某年某月所有的星期数
     *
     * @param year
     * @param month
     */
    public static int getWeeksOfMonth(int year, int month,SpecialCalendar sc) {
        // 先判断某月的第一天为星期几
        int preMonthRelax = 0;
        int dayFirst = getWhichDayOfWeek(year, month,sc);
        int days = sc.getDaysOfMonth(sc.isLeapYear(year), month);
        if (dayFirst != 7) {
            preMonthRelax = dayFirst;
        }
        if ((days + preMonthRelax) % 7 == 0) {
            weeksOfMonth = (days + preMonthRelax) / 7;
        } else {
            weeksOfMonth = (days + preMonthRelax) / 7 + 1;
        }
        return weeksOfMonth;

    }

    /**
     * 判断某年某月的第一天为星期几
     *
     * @param year
     * @param month
     * @return
     */
    public static int getWhichDayOfWeek(int year, int month,SpecialCalendar sc) {
        return sc.getWeekdayOfMonth(year, month);

    }

    public static int getWeeksOfMonth(int dayOfWeek,int daysOfMonth) {
        // getCalendar(year, month);
        int preMonthRelax = 0;
        if (dayOfWeek != 7) {
            preMonthRelax = dayOfWeek;
        }
        if ((daysOfMonth + preMonthRelax) % 7 == 0) {
            weeksOfMonth = (daysOfMonth + preMonthRelax) / 7;
        } else {
            weeksOfMonth = (daysOfMonth + preMonthRelax) / 7 + 1;
        }
        return weeksOfMonth;
    }

    /**
     * @param year
     * @param month
     */
    public static int getLastDayOfWeek(int year, int month,SpecialCalendar sc) {
        boolean isLeapyear = sc.isLeapYear(year); // 是否为闰年
        return sc.getWeekDayOfLastMonth(year, month,
                sc.getDaysOfMonth(isLeapyear, month));
    }

    public static List<String> getCalendar(int year, int month,SpecialCalendar sc) {
        List<String> strings = new ArrayList<>();
        boolean isLeapyear = sc.isLeapYear(year);
        strings.add(sc.isLeapYear(year)+"");// 是否为闰年
        strings.add(sc.getDaysOfMonth(isLeapyear, month)+"");// 某月的总天数
        strings.add(sc.getWeekdayOfMonth(year, month)+"");// 某月第一天为星期几
        return strings;
    }


}
