package cn.bmkp.bus.app.models;

import java.io.Serializable;

/**
 * Created by chenliang on 2016/3/23.
 */
public class StationPrice implements Serializable {
    //起点站名
    private String startName;
    //终点站名
    private String endName;
    private String price;
    private String time;

    public String getStartName() {
        return startName;
    }

    public void setStartName(String startName) {
        this.startName = startName;
    }

    public String getEndName() {
        return endName;
    }

    public void setEndName(String endName) {
        this.endName = endName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
